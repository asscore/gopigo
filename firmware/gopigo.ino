///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Oprogramowanie (firmware) nakładki GoPiGo/GoPiGo2.
//
// Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wymagane wartości fuse bitów:
//   extended - 0x05
//   high     - 0xDA
//   low      - 0x7F
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ustawienia zegara
// W celu obniżenia zakłóceń od silników zaprogramowany (wartość 0) jest bit dzielnika zegara CKDIV8. Sprawia on,
// że sygnał zegara wewnętrznego jest dzielony przez 8. Czyli nawet jak ustawione jest 16 MHz, to w rzeczywistości
// układ będzie działał z 2 MHz pobierając mniej prądu.
// W linii 75 w pliku
//   macOS: /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
//   Windows: C:\Program Files (x86)\Arduino\hardware\arduino\avr\boards.txt
// należy zmienić (Arduino/Genuino Uno):
//   uno.build.f_cpu=16000000L
// na:
//   uno.build.f_cpu=2000000L
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <Wire.h>
#include <SoftServo.h>

SoftServo servo;

#define I2C_ADDR 0x08  // Adres I2C nakładki

#define BOARD_NAME "GoPiGo"
#define FIRMWARE_VERSION "100"  // Wersja x.x.x

// Lista komend I2C
enum I2C_MESSAGE_TYPE {
  I2C_MESSAGE_TYPE_NONE,
  I2C_MESSAGE_TYPE_GET_NAME,
  I2C_MESSAGE_TYPE_GET_FIRMWARE_VERSION,
  I2C_MESSAGE_TYPE_GET_VOLTAGE_VCC,
  I2C_MESSAGE_TYPE_GET_CPU_SPEED,
  I2C_MESSAGE_TYPE_SET_LED,
  I2C_MESSAGE_TYPE_SET_SERVO,
  I2C_MESSAGE_TYPE_SET_GROVE_TYPE,
  I2C_MESSAGE_TYPE_SET_GROVE_MODE,
  I2C_MESSAGE_TYPE_SET_GROVE_STATE,
  I2C_MESSAGE_TYPE_SET_GROVE_PWM_DUTY,
  I2C_MESSAGE_TYPE_GET_GROVE_STATE,
  I2C_MESSAGE_TYPE_GET_GROVE_VALUE,
  I2C_MESSAGE_TYPE_SET_MOTOR_COMMAND,
  I2C_MESSAGE_TYPE_SET_MOTOR_SPEED,
  I2C_MESSAGE_TYPE_GET_MOTOR_SPEED,
  I2C_MESSAGE_TYPE_SET_MOTOR_CONTROL,
  I2C_MESSAGE_TYPE_SET_MOTOR_ENCODER,
  I2C_MESSAGE_TYPE_GET_MOTOR_ENCODER,
  I2C_MESSAGE_TYPE_RESET
};

enum GROVE_TYPE {  // Sposób obsługi portu Grove (tylko A1)
  GROVE_TYPE_CUSTOM = 1,
  GROVE_TYPE_US  // Ultradźwiękowy czujnik odległości (Grove - Ultrasonic Ranger)
};

int grove_type[] = { GROVE_TYPE_CUSTOM };  // Domyślna wartość 1 (CUSTOM)

enum GROVE_MODE {
  GROVE_MODE_INPUT = 0,
  GROVE_MODE_OUTPUT,
  GROVE_MODE_INPUT_PULLUP
};

enum MOTOR_COMMAND {
  MOTOR_COMMAND_FORWARD = 1,
  MOTOR_COMMAND_BACKWARD,
  MOTOR_COMMAND_RIGHT,
  MOTOR_COMMAND_LEFT,
  MOTOR_COMMAND_RIGHT_ROTATE,
  MOTOR_COMMAND_LEFT_ROTATE,
  MOTOR_COMMAND_STOP
};

// Zasada działania dla poszczególnych silników:
// .----------.------.---------------------------.
// | Kierunek | PWM  | Reakcja                   |
// .----------'------'---------------------------'
// |   0  1   |  XX  | do przodu z prędkością XX |
// |   1  0   |  XX  | do tyłu z prędkością XX   |
// |   1  1   |  --  | silnik wyłączony          |
// '----------'------'---------------------------'

// Konfiguracja pinów w GoPiGo2
int motor1_control_pin1 = 7;  // Kierunek obrotów silnika prawego
int motor1_control_pin2 = 8;
int motor2_control_pin1 = 4;  // Kierunek obrotów silnika lewego
int motor2_control_pin2 = 14;
int motor1_speed_pin = 9;  // Prędkość silnika prawego (PWM)
int motor2_speed_pin = 6;  // Prędkość silnika lewego (PWM)
int voltage_pin = 6;
int right_led_pin = 16;
int left_led_pin = 17;
int servo_pin = 5;
int grove_a1_pin = 15;
int grove_d11_pin = 10;
int hw_version_pin = 7;
int encoder1_pin = 3;
int encoder2_pin = 2;
// GoPiGo zawiera następujące różnice:
// motor2_control_pin2 = 13
// voltage_pin = 0
// right_led_pin = 5
// left_led_pin = 10

volatile int cmd[5], receive_index = 0, send_index = 0, bytes_to_send = 0;
byte payload[21];
int temp;

int speed1, speed2;

volatile int e1 = 0, e2 = 0;
int tgt_flag = 0, m1_en = 0, m2_en = 0, tgt = 0;

int servo_flag = 0;  // Serwomechanizm aktywny (1), czy też nie (0)
unsigned long time_since_servo_cmd = 0;  // Czas wywołania komendy zmiany pozycji
unsigned int servo_delay = 0;  // Przybliżony czas trwania zmiany pozycji

int hw_version;  // Wersja nakładki

#define DEFAULT_SPEED 200  // Domyślna prędkość

#define LED_RIGHT 0x01
#define LED_LEFT  0x02

#define BACKWARD 0
#define FORWARD  1

#define MOTOR_1 0x01
#define MOTOR_2 0x02
#define MOTOR_RIGHT MOTOR_1
#define MOTOR_LEFT  MOTOR_2

void motor_stop() {
  digitalWrite(motor1_control_pin1, HIGH);
  digitalWrite(motor1_control_pin2, HIGH);
  digitalWrite(motor2_control_pin1, HIGH);
  digitalWrite(motor2_control_pin2, HIGH);
}

void setup() {
  for (int i = 0; i < 10; i++) {
    hw_version = analogRead(hw_version_pin);
  }

  if (hw_version < 790) {  // Korekta pinów dla GoPiGo
    motor2_control_pin2 = 13;
    right_led_pin = 5;
    left_led_pin = 10;
    voltage_pin = 0;
  }

  // Zliczanie impulsów enkodera (18 na obrót) - wywołanie przy zmianie stanu z niski na wysoki
  attachInterrupt(digitalPinToInterrupt(encoder1_pin), encoder1_int, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder2_pin), encoder2_int, RISING);

  // Konfiguracja pinów mostka H (SN754410/TB6612FNG)
  pinMode(motor1_control_pin1, OUTPUT);
  pinMode(motor1_control_pin2, OUTPUT);
  pinMode(motor2_control_pin1, OUTPUT);
  pinMode(motor2_control_pin2, OUTPUT);

  pinMode(left_led_pin, OUTPUT);
  pinMode(right_led_pin, OUTPUT);

  servo.begin(servo_pin);

  Wire.begin(I2C_ADDR);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  speed1 = speed2 = DEFAULT_SPEED;
}

void loop() {
  analogWrite(motor1_speed_pin, speed1);  // Prędkość silnika prawego
  analogWrite(motor2_speed_pin, speed2);  // Prędkość silnika lewego

  if (cmd[0] == I2C_MESSAGE_TYPE_GET_NAME) {
    // [cmd, 0, 0, 0] - nazwa nakładki

    int c = 0;
    int len = strlen(BOARD_NAME);
    while (c < len) {
      payload[c] = BOARD_NAME[c];
      c++;
    }
    if (hw_version >= 790) {
      payload[c++] = '2';  // Dla nowej wersji nakładki zwracana nazwa 'GoPiGo2'
    }
    payload[c] = '\0';
    bytes_to_send = 10;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_FIRMWARE_VERSION) {
    // [cmd, 0, 0, 0] - wersja oprogramowania

    payload[0] = FIRMWARE_VERSION[0] - '0';
    payload[1] = FIRMWARE_VERSION[1] - '0';
    payload[2] = FIRMWARE_VERSION[2] - '0';
    bytes_to_send = 3;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_VOLTAGE_VCC) {
    // [cmd, 0, 0, 0] - poziom naładowania baterii

    int voltage = analogRead(voltage_pin);
    payload[0] = voltage / 256;
    payload[1] = voltage % 256;
    bytes_to_send = 2;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_CPU_SPEED) {
    // [cmd, 0, 0, 0] - częstotliwość zegara procesora nakładki w MHz

    payload[0] = F_CPU / 1000000;
    bytes_to_send = 1;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_LED) {
    // [cmd, led, state, 0] - stan diód LED

    if (cmd[1] & LED_RIGHT) {
      digitalWrite(right_led_pin, cmd[2]);
    }
    if (cmd[1] & LED_LEFT) {
      digitalWrite(left_led_pin, cmd[2]);
    }
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_SERVO) {
    // [cmd, position, 0, 0] - pozycja serwomechanizmu w stopniach (0 - 180)

    servo.setAngle(cmd[1]);
    servo_delay = map(cmd[1], 0, 180, 1000, 2000);
    time_since_servo_cmd = millis();
    servo_flag = 1;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_GROVE_TYPE) {
    // [cmd, pin, type, 0] - sposób obsługi portu Grove (tylko A1)

    if (cmd[1] == grove_a1_pin) {
      grove_type[0] = cmd[2];
    }
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_GROVE_MODE) {
    // [cmd, pin, mode, 0] - tryb pracy portu Grove

    if (cmd[2] == GROVE_MODE_INPUT) {
      pinMode(cmd[1], INPUT);
    } else if (cmd[2] == GROVE_MODE_OUTPUT) {
      pinMode(cmd[1], OUTPUT);
    } else if (cmd[2] == GROVE_MODE_INPUT_PULLUP) {
      pinMode(cmd[1], INPUT_PULLUP);
    }
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_GROVE_STATE) {
    // [cmd, pin, state, 0] - stan portu Grove

    digitalWrite(cmd[1], cmd[2]);
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_GROVE_PWM_DUTY) {
    // [cmd, pin, duty, 0] - wypełnienia sygnału PWM (0 - 255)

    analogWrite(cmd[1], cmd[2]);
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_GROVE_STATE) {
    // [cmd, pin, 0, 0] - stan portu Grove

    payload[0] = digitalRead(cmd[1]);
    bytes_to_send = 1;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_GROVE_VALUE) {
    // [cmd, pin, 0, 0] - wartość na porcie Grove (odczyt w zależności od ustawionego sposobu obsługi)

    if (grove_type[0] == GROVE_TYPE_US) {  // Ultradźwiękowy czujnik odległości (Grove - Ultrasonic Ranger)
      pinMode(cmd[1], OUTPUT);
      digitalWrite(cmd[1], LOW);
      delayMicroseconds(2);
      digitalWrite(cmd[1], HIGH);
      delayMicroseconds(5);
      digitalWrite(cmd[1], LOW);
      pinMode(cmd[1], INPUT);
      long duration = pulseIn(cmd[1], HIGH);
      temp = (duration / 29 / 2) * 10;
    } else {
      temp = analogRead(cmd[1]);
    }
    payload[0] = temp / 256;
    payload[1] = temp % 256;
    bytes_to_send = 2;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_MOTOR_COMMAND) {
    // [cmd, command, 0, 0] - wykonanie komendy

    switch (cmd[1]) {
      case MOTOR_COMMAND_FORWARD:  // Do przodu
        digitalWrite(motor1_control_pin1, LOW);
        digitalWrite(motor1_control_pin2, HIGH);
        digitalWrite(motor2_control_pin1, LOW);
        digitalWrite(motor2_control_pin2, HIGH);
        break;
      case MOTOR_COMMAND_BACKWARD:  // Do tyłu
        digitalWrite(motor1_control_pin1, HIGH);
        digitalWrite(motor1_control_pin2, LOW);
        digitalWrite(motor2_control_pin1, HIGH);
        digitalWrite(motor2_control_pin2, LOW);
        break;
      case MOTOR_COMMAND_RIGHT:  // Obrót względem prawego koła
        digitalWrite(motor1_control_pin1, HIGH);
        digitalWrite(motor1_control_pin2, HIGH);
        digitalWrite(motor2_control_pin1, LOW);
        digitalWrite(motor2_control_pin2, HIGH);
        break;
      case MOTOR_COMMAND_LEFT:  // Obrót względem lewego koła
        digitalWrite(motor1_control_pin1, LOW);
        digitalWrite(motor1_control_pin2, HIGH);
        digitalWrite(motor2_control_pin1, HIGH);
        digitalWrite(motor2_control_pin2, HIGH);
        break;
      case MOTOR_COMMAND_RIGHT_ROTATE:  // Obrót w miejscu w prawo
        digitalWrite(motor1_control_pin1, HIGH);
        digitalWrite(motor1_control_pin2, LOW);
        digitalWrite(motor2_control_pin1, LOW);
        digitalWrite(motor2_control_pin2, HIGH);
        break;
      case MOTOR_COMMAND_LEFT_ROTATE:  // Obrót w miejscu w lewo
        digitalWrite(motor1_control_pin1, LOW);
        digitalWrite(motor1_control_pin2, HIGH);
        digitalWrite(motor2_control_pin1, HIGH);
        digitalWrite(motor2_control_pin2, LOW);
        break;
      case MOTOR_COMMAND_STOP:  // Zatrzymanie
        motor_stop();
        break;
      default:
        break;
    }

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_MOTOR_SPEED) {
    // [cmd, motor, speed, 0] - prędkość silnika (0 - 255)

    if (cmd[1] & MOTOR_1) {  // Prędkość silnika prawego
      speed1 = cmd[2];
    }
    if (cmd[1] & MOTOR_2) {  // Prędkość silnika lewego
      speed2 = cmd[2];
    }
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_GET_MOTOR_SPEED)  {
    // [cmd, motor, 0, 0] - bieżąca prędkość silnika

    int c = 0;
    if (cmd[1] & MOTOR_1) {  // Prędkość silnika prawego
      payload[c++] = speed1;
    }
    if (cmd[1] & MOTOR_2) {  // Prędkość silnika lewego
      payload[c++] = speed2;
    }
    bytes_to_send = c;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_SET_MOTOR_CONTROL) {
    // [cmd, motor, direction, speed] - kierunek i prędkość silnika

    if (cmd[1] & MOTOR_1) {  // Silnik prawy
      speed1 = cmd[3];  // Prędkość (0 - 255)
      if (cmd[2] == FORWARD) {  // Kierunek (1 - do przodu, 0 - do tyłu)
        digitalWrite(motor1_control_pin1, LOW);
        digitalWrite(motor1_control_pin2, HIGH);
      } else if (cmd[2] == BACKWARD) {
        digitalWrite(motor1_control_pin1, HIGH);
        digitalWrite(motor1_control_pin2, LOW);
      }
    }
    if (cmd[1] & MOTOR_2) {  // Silnik lewy
      speed2 = cmd[3];  // Prędkość (0 - 255)
      if (cmd[2] == FORWARD) {  // Kierunek (1 - do przodu, 0 - do tyłu)
        digitalWrite(motor2_control_pin1, LOW);
        digitalWrite(motor2_control_pin2, HIGH);
      } else if (cmd[2] == BACKWARD) {
        digitalWrite(motor2_control_pin1, HIGH);
        digitalWrite(motor2_control_pin2, LOW);
      }
    }

  }

  if (cmd[0] == I2C_MESSAGE_TYPE_SET_MOTOR_ENCODER || tgt_flag == 1) {
    if (cmd[0] == I2C_MESSAGE_TYPE_SET_MOTOR_ENCODER) {
      // [cmd, motor, target // 2, target % 2] - liczba impulsów do osiągnięcia przez enkoder

      cmd[0] = 0;
      tgt_flag = 1;
      // Sprawdzenie dla którego silnika zdefiniowano cel
      m1_en = (cmd[1] & MOTOR_1) ? 1 : 0;
      m2_en = (cmd[1] & MOTOR_2) ? 1 : 0;
      tgt = cmd[2] * 256 + cmd[3];  // Odczytanie celu
      e1 = e2 = 0;

    }

    if (m1_en && !m2_en) {  // Tylko silnik prawy
      if (e1 >= tgt) {  // Po osiągnięcie celu, zatrzymanie
        cmd[0] = 0;
        motor_stop();
        tgt_flag = 0;
      }
    } else if (!m1_en && m2_en) {  // Tylko silnik lewy
      if (e2 >= tgt) {  // Po osiągnięcie celu, zatrzymanie
        cmd[0] = 0;
        motor_stop();
        tgt_flag = 0;
      }
    } else if (m1_en && m2_en) {  // Oba silniki
      if (e1 >= tgt && e2 >= tgt) {  // Po osiągnięcie celu przez oba, zatrzymanie
        cmd[0] = 0;
        motor_stop();
        tgt_flag = 0;
      }
    }
  }

  if (cmd[0] == I2C_MESSAGE_TYPE_GET_MOTOR_ENCODER) {
    // [cmd, motor, 0, 0] - bieżąca liczba impulsów enkodera

    int c = 0;
    if (cmd[1] & MOTOR_1) {
      temp = e1;  // Liczba impulsów enkodera prawego
      payload[c++] = temp / 256;
      payload[c++] = temp % 256;
    }
    if (cmd[1] & MOTOR_2) {
      temp = e2;  // Liczba impulsów enkodera lewego
      payload[c++] = temp / 256;
      payload[c++] = temp % 256;
    }
    bytes_to_send = c;
    cmd[0] = 0;

  } else if (cmd[0] == I2C_MESSAGE_TYPE_RESET) {
    // [cmd, 0, 0, 0] - przywrócenie stanu początkowego

    motor_stop();
    speed1 = speed2 = DEFAULT_SPEED;
    servo_flag = 0;
    time_since_servo_cmd = 0;
    servo_delay = 0;
    tgt_flag = 0;
    e1 = e2 = 0;
    grove_type[0] = GROVE_TYPE_CUSTOM;
    pinMode(grove_a1_pin, INPUT);
    pinMode(grove_d11_pin, INPUT);
    digitalWrite(right_led_pin, LOW);
    digitalWrite(left_led_pin, LOW);
    cmd[0] = 0;

  }

  // Odświeżenie pozycji, jeśli serwomechanizm aktywny
  if (servo_flag) {
    servo.refresh();
  }

  // Wyłączenie odświeżania, jeśli minął określony czas od ostatniej komendy zmiany pozycji
  if (millis() - time_since_servo_cmd > servo_delay) {
    servo_flag = 0;
  }
}

void receiveData(int byte_count) {
  while (Wire.available()) {
    if (Wire.available() == 4) {  // Liczba bajtów do odczytania
      receive_index = 0;
    }
    cmd[receive_index++] = Wire.read();
  }
}

void sendData() {
  if (bytes_to_send >= 2) {
    Wire.write(payload[send_index++]);
    if (send_index == bytes_to_send) {
      send_index = 0;
      bytes_to_send = 0;
    }
  } else if (bytes_to_send == 1) {
    Wire.write(payload[0]);
    bytes_to_send = 0;
  }
}

void encoder1_int() {
  e1++;
}

void encoder2_int() {
  e2++;
}
