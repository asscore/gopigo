//
// Wersja biblioteki SoftwareServo do obsługi tylko jednego serwomechanizmu.
//
// Copyright (c) 2017 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//

#ifndef SOFT_SERVO_H
#define SOFT_SERVO_H

#include <Arduino.h>

class SoftServo {
  public:
    SoftServo();
    void begin(int pin);
    void setMinimumPulse(uint16_t t);  // Czas trwania impulsu dla 0 stopni w mikrosekundach
    void setMaximumPulse(uint16_t t);  // Czas trwania impulsu dla 180 stopni w mikrosekundach
    void setAngle(int angle);  // Pozycja serwomechanizmu w stopniach (0 - 180)
    void refresh();  // Aktualizacja pozycji serwomechanizmu

  private:
    uint8_t _pin;
    uint8_t _angle;
    uint16_t _pulse0;
    uint8_t _min16;
    uint8_t _max16;
};

#endif  // SOFT_SERVO_H
