//
// Wersja biblioteki SoftwareServo do obsługi tylko jednego serwomechanizmu.
//
// Copyright (c) 2017 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//

#include <SoftServo.h>

#define NO_ANGLE 0xFF

SoftServo::SoftServo() : _pin(0), _angle(NO_ANGLE), _pulse0(0), _min16(34), _max16(150) {
}

void SoftServo::begin(int pin) {
  _pin = pin;
  _angle = NO_ANGLE;
  _pulse0 = 0;
  digitalWrite(pin, 0);
  pinMode(pin, OUTPUT);
  return 1;
}

void SoftServo::setMinimumPulse(uint16_t t) {
  _min16 = t / 16;
}

void SoftServo::setMaximumPulse(uint16_t t) {
  _max16 = t / 16;
}

void SoftServo::setAngle(int angle) {
  if (angle < 0) {
    angle = 0;
  }
  if (angle > 180) {
    angle = 180;
  }
  _angle = angle;
  _pulse0 = (_min16 * 16L * clockCyclesPerMicrosecond() + (_max16 - _min16)
             * (16L * clockCyclesPerMicrosecond()) * _angle / 180L) / 64L;
}

void SoftServo::refresh() {
  uint8_t count = 0, i = 0;
  uint16_t base = 0;
  static unsigned long last_refresh = 0;
  unsigned long m = millis();

  if (m >= last_refresh && m < last_refresh + 20) {
    return;
  }

  last_refresh = m;

  if (_pulse0 == 0) {
    return;
  }

  digitalWrite(_pin, 1);

  uint8_t start = TCNT0;
  uint8_t now = start;
  uint8_t last = now;

  uint16_t go = start + _pulse0;

  for (;;) {
    now = TCNT0;
    if (now < last) {
      base += 256;
    }
    last = now;

    if (base + now > go) {
      digitalWrite(_pin, 0);
      break;
    }
  }
}
