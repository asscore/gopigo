#!/bin/bash

FW_UPDATE_FILE=$(basename $(find ./ -maxdepth 1 -name *.hex))

sudo avrdude -c gpio -p m328p -U lfuse:w:0x7F:m
sudo avrdude -c gpio -p m328p -U hfuse:w:0xDA:m
sudo avrdude -c gpio -p m328p -U efuse:w:0x05:m
sudo avrdude -c gpio -p m328p -U flash:w:$FW_UPDATE_FILE
