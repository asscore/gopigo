//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//

#include "gopigo.h"
#include <errno.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define I2C_ADDR 0x08  // Adres I2C nakładki

#define FIRMWARE_VERSION_REQUIRED "1.0."

enum I2C_MESSAGE_TYPE {
    I2C_MESSAGE_TYPE_NONE,
    I2C_MESSAGE_TYPE_GET_NAME,
    I2C_MESSAGE_TYPE_GET_FIRMWARE_VERSION,
    I2C_MESSAGE_TYPE_GET_VOLTAGE_VCC,
    I2C_MESSAGE_TYPE_GET_CPU_SPEED,
    I2C_MESSAGE_TYPE_SET_LED,
    I2C_MESSAGE_TYPE_SET_SERVO,
    I2C_MESSAGE_TYPE_SET_GROVE_TYPE,
    I2C_MESSAGE_TYPE_SET_GROVE_MODE,
    I2C_MESSAGE_TYPE_SET_GROVE_STATE,
    I2C_MESSAGE_TYPE_SET_GROVE_PWM_DUTY,
    I2C_MESSAGE_TYPE_GET_GROVE_STATE,
    I2C_MESSAGE_TYPE_GET_GROVE_VALUE,
    I2C_MESSAGE_TYPE_SET_MOTOR_COMMAND,
    I2C_MESSAGE_TYPE_SET_MOTOR_SPEED,
    I2C_MESSAGE_TYPE_GET_MOTOR_SPEED,
    I2C_MESSAGE_TYPE_SET_MOTOR_CONTROL,
    I2C_MESSAGE_TYPE_SET_MOTOR_ENCODER,
    I2C_MESSAGE_TYPE_GET_MOTOR_ENCODER,
    I2C_MESSAGE_TYPE_RESET
};

GoPiGo::GoPiGo() {
    parse_cpuinfo();

    if ((descriptor_ = open(i2c_device_, O_RDWR)) < 0) {
        fprintf(stderr, "Unable to open I2C device: %s.\n", strerror(errno));
        return;
    }

    if (ioctl(descriptor_, I2C_SLAVE, I2C_ADDR) < 0) {
        fprintf(stderr, "Unable to select I2C device: %s.\n", strerror(errno));
        return;
    }
}

GoPiGo::~GoPiGo() {
    if (descriptor_ != -1) {
        close(descriptor_);
    }
}

void GoPiGo::parse_cpuinfo() {
    char line[32];
    char s[32];
    int i;

    FILE *fp = fopen("/proc/cpuinfo", "r");
    if (fp == NULL) {
        return;
    }

    while (fgets(line, 32, fp) != NULL) {
        sscanf(line, "%s : %x", (char *) &s, &i);
        if (strcmp(s, "Revision") == 0) {
            if (i < 4) {
                i2c_device_ = (char *)"/dev/i2c-0";
            } else {
                i2c_device_ = (char *)"/dev/i2c-1";
            }
            break;
        }
    }

    fclose(fp);
}

void GoPiGo::sleep(unsigned int milliseconds) {
    usleep(milliseconds * 1000);
}

void GoPiGo::i2c_write_block(uint8_t command, uint8_t opt1, uint8_t opt2, uint8_t opt3) {
    uint8_t data_block[4] = {command, opt1, opt2, opt3};

    i2c_smbus_write_i2c_block_data(descriptor_, 1, sizeof(data_block), &data_block[0]);
    sleep(5);
}

uint8_t GoPiGo::i2c_read_byte() {
    return i2c_smbus_read_byte(descriptor_);
}

int GoPiGo::detect(bool critical) {
    char board_str[10];
    char vfw_str[10];

    get_board(board_str);
    if (strstr(board_str, "GoPiGo") != board_str && strstr(board_str, "GoPiGo2") != board_str) {
        fprintf(stderr, "GoPiGo/GoPiGo2 with address 0x%02x not connected.\n", I2C_ADDR);
        if (critical) {
            exit(EXIT_FAILURE);
        }
        return ERROR_WRONG_DEVICE;
    }

    get_version_firmware(vfw_str);
    if (strstr(vfw_str, FIRMWARE_VERSION_REQUIRED) != vfw_str) {
        fprintf(stderr, "%s firmware needs to be version %sx but is currently version %s.\n", board_str,
                FIRMWARE_VERSION_REQUIRED, vfw_str);
        if (critical) {
            exit(EXIT_FAILURE);
        }
        return ERROR_FIRMWARE_MISMATCH;
    }

    return ERROR_NONE;
}

void GoPiGo::get_board(char *str) {
    i2c_write_block(I2C_MESSAGE_TYPE_GET_NAME, 0, 0, 0);
    sleep(80);
    for (int i = 0; i < 10; i++) {
        str[i] = i2c_read_byte();
    }
}

void GoPiGo::get_version_firmware(char *str) {
    uint8_t number[3];

    i2c_write_block(I2C_MESSAGE_TYPE_GET_FIRMWARE_VERSION, 0, 0, 0);
    sleep(80);
    for (int i = 0; i < 3; i++) {
        number[i] = i2c_read_byte();
    }

    sprintf(str, "%d.%d.%d", number[0], number[1], number[2]);
}

float GoPiGo::get_voltage_battery() {
    int b[2];
    float voltage = -1;

    i2c_write_block(I2C_MESSAGE_TYPE_GET_VOLTAGE_VCC, 0, 0, 0);
    sleep(80);
    for (int i = 0; i < 2; i++) {
        b[i] = i2c_read_byte();
    }
    if (b[0] != -1 && b[1] != -1) {
        voltage = b[0] * 256 + b[1];
        voltage = (5.0 * voltage / 1024 ) / .4;
    }

    return voltage;
}

uint8_t GoPiGo::get_cpu_speed() {
    i2c_write_block(I2C_MESSAGE_TYPE_GET_CPU_SPEED, 0, 0, 0);
    sleep(80);

    return i2c_read_byte();
}

void GoPiGo::set_led(uint8_t led, uint8_t state) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_LED, led, state, 0);
}

void GoPiGo::set_servo(uint8_t position) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_SERVO, position, 0, 0);
}

void GoPiGo::set_grove_type(uint8_t pin, uint8_t type) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_GROVE_TYPE, pin, type, 0);
}

void GoPiGo::set_grove_mode(uint8_t pin, uint8_t mode) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_GROVE_MODE, pin, mode, 0);
}

void GoPiGo::set_grove_state(uint8_t pin, uint8_t state) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_GROVE_STATE, pin, state, 0);
}

void GoPiGo::set_grove_pwm_duty(uint8_t pin, uint8_t duty) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_GROVE_PWM_DUTY, pin, duty, 0);
}

uint8_t GoPiGo::get_grove_state(uint8_t pin) {
    i2c_write_block(I2C_MESSAGE_TYPE_GET_GROVE_STATE, pin, 0, 0);
    sleep(80);

    return i2c_read_byte();
}

int GoPiGo::get_grove_value(uint8_t pin) {
    int b[2];
    int value = -1;

    i2c_write_block(I2C_MESSAGE_TYPE_GET_GROVE_VALUE, pin, 0, 0);
    sleep(80);
    for (int i = 0; i < 2; i++) {
        b[i] = i2c_read_byte();
    }
    if (b[0] != -1 && b[1] != -1) {
        value = b[0] * 256 + b[1];
    }

    return value;
}

void GoPiGo::set_motor_command(uint8_t command) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_MOTOR_COMMAND, command, 0, 0);
}

void GoPiGo::set_motor_speed(uint8_t motor, uint8_t speed) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_MOTOR_SPEED, motor, speed, 0);
}

void GoPiGo::get_motor_speed(uint8_t motor, uint8_t *speed) {
    i2c_write_block(I2C_MESSAGE_TYPE_GET_MOTOR_SPEED, motor, 0, 0);
    sleep(80);
    if (motor & MOTOR_1) {
        speed[0] = i2c_read_byte();
    }
    if (motor & MOTOR_2) {
        speed[1] = i2c_read_byte();
    }
}

void GoPiGo::set_motor_control(uint8_t motor, uint8_t direction, uint8_t speed) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_MOTOR_CONTROL, motor, direction, speed);
}

void GoPiGo::set_motor_encoder(uint8_t motor, int32_t target) {
    i2c_write_block(I2C_MESSAGE_TYPE_SET_MOTOR_ENCODER, motor, target / 256, target % 256);
}

void GoPiGo::get_motor_encoder(uint8_t motor, int32_t *value) {
    int b[2];

    i2c_write_block(I2C_MESSAGE_TYPE_GET_MOTOR_ENCODER, motor, 0, 0);
    sleep(80);
    if (motor & MOTOR_1) {
        for (int i = 0; i < 2; i++) {
            b[i] = i2c_read_byte();
        }
        if (b[0] != -1 && b[1] != -1) {
            value[0] = b[0] * 256 + b[1];
        } else {
            value[0] = -1; 
        }
    }
    if (motor & MOTOR_2) {
        for (int i = 0; i < 2; i++) {
            b[i] = i2c_read_byte();
        }
        if (b[0] != -1 && b[1] != -1) {
            value[1] = b[0] * 256 + b[1];
        } else {
            value[1] = -1; 
        }
    }
}

void GoPiGo::reset() {
    i2c_write_block(I2C_MESSAGE_TYPE_RESET, 0, 0, 0);
}
