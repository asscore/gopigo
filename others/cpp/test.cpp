//
// Test biblioteki GoPiGo.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//

#include "gopigo.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#define ENABLE_US

GoPiGo gpg = GoPiGo();

void exit_signal_handler(int signo) {
    if (signo == SIGINT) {
        gpg.reset();
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv) {
    char str[33];
    int32_t value[2];

    signal(SIGINT, exit_signal_handler);

    gpg.detect();

    gpg.get_board(str);
    printf("Board            : %s\n", str);
    gpg.get_version_firmware(str);
    printf("Firmware version : %s\n", str);
    printf("Battery voltage  : %.2f V\n", gpg.get_voltage_battery());
    printf("CPU speed        : %d MHz\n", gpg.get_cpu_speed());

    gpg.set_led(LED_RIGHT + LED_LEFT, ON);  // Diody LED zapalone
    usleep(1000 * 1000);
    gpg.set_led(LED_RIGHT + LED_LEFT, OFF);  // Diody LED zgaszone

    // Zmiana pozycji serwomechanizmu
    gpg.set_servo(20);
    usleep(1000 * 1000);
    gpg.set_servo(160);
    usleep(1000 * 1000);
    gpg.set_servo(90);

#ifdef ENABLE_US
    // Odczyt za pomocą ultradźwiękowego czujnika odległości (Grove - Ultrasonic Ranger)
    printf("\n");
    gpg.set_grove_type(GROVE_A1, GROVE_TYPE_US);
    for (int i = 0; i < 20; i++) {
        usleep(500 * 1000);
        int value = gpg.get_grove_value(GROVE_A1);
        printf("\rDistance (%02d/20): %4d mm", i + 1, value);
        fflush(stdout);
    }
    printf("\n");
#endif

    // Do przodu 10 cm
    int32_t pulse_distance_10 = ENCODER_PULSE_PER_ROTATION * (10 / float(WHEEL_CIRCUMFERENCE));

    gpg.set_motor_encoder(MOTOR_RIGHT + MOTOR_LEFT, pulse_distance_10);
    gpg.set_motor_command(MOTOR_COMMAND_FORWARD);
    while (1) {
        gpg.get_motor_encoder(MOTOR_RIGHT + MOTOR_LEFT, value);
        if (value[0] >= pulse_distance_10 || value[1] >= pulse_distance_10) {
            break;
        }
    }

    // Obrót względem prawego koła o 90 stopni
    int32_t pulse_degrees_90 = 90 / float(DEGREES_PER_ENCODER_PULSE);
    gpg.set_motor_encoder(MOTOR_LEFT, pulse_degrees_90);
    gpg.set_motor_command(MOTOR_COMMAND_RIGHT);
    while (1) {
        gpg.get_motor_encoder(MOTOR_LEFT, value);
        if (value[1] >= pulse_degrees_90) {
            break;
        }
    }

    gpg.reset();

    return EXIT_SUCCESS;
}
