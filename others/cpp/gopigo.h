//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//

#ifndef GOPIGO_H
#define GOPIGO_H

#include <math.h>
#include <stdint.h>

#define ERROR_NONE               0
#define ERROR_WRONG_DEVICE      -1
#define ERROR_FIRMWARE_MISMATCH -2

enum GROVE_TYPE {
    GROVE_TYPE_CUSTOM = 1,
    GROVE_TYPE_US
};

enum GROVE_MODE {
    GROVE_MODE_INPUT,
    GROVE_MODE_OUTPUT,
    GROVE_MODE_INPUT_PULLUP
};

enum MOTOR_COMMAND {
    MOTOR_COMMAND_FORWARD = 1,
    MOTOR_COMMAND_BACKWARD,
    MOTOR_COMMAND_RIGHT,
    MOTOR_COMMAND_LEFT,
    MOTOR_COMMAND_RIGHT_ROTATE,
    MOTOR_COMMAND_LEFT_ROTATE,
    MOTOR_COMMAND_STOP
};

#define LED_RIGHT 0x01
#define LED_LEFT  0x02

#define ON  1
#define OFF 0

#define GROVE_A1  15
#define GROVE_D11 10

#define LOW  0
#define HIGH 1

#define DEFAULT_SPEED 200

#define BACKWARD 0
#define FORWARD  1

#define MOTOR_1 0x01
#define MOTOR_2 0x02
#define MOTOR_RIGHT MOTOR_1
#define MOTOR_LEFT  MOTOR_2

#define WHEEL_DIAMETER      6.5  // Średnica koła w cm
#define WHEEL_CIRCUMFERENCE M_PI * WHEEL_DIAMETER  // Obwód koła w cm

#define ENCODER_PULSE_PER_ROTATION 18  // Liczba impulsów na obrót
#define DEGREES_PER_ENCODER_PULSE  360.0 / 64  // Liczba stopni na impuls (obrót o 360 stopni to około 64 impulsy)

class GoPiGo {

public:    
    GoPiGo();
    ~GoPiGo();

    // Sprawdzenie zgodności z oprogramowaniem nakładki
    int detect(bool critical = true);

    // Nazwa nakładki
    void get_board(char *str);
    // Wersja oprogramowania
    void get_version_firmware(char *str);
    // Poziom naładowania baterii w V
    float get_voltage_battery();
    // Częstotliwość zegara procesora nakładki w MHz
    uint8_t get_cpu_speed();

    // Stan diód LED
    void set_led(uint8_t led, uint8_t state);
    // Pozycja serwomechanizmu w stopniach
    void set_servo(uint8_t position);

    // Sposób obsługi portu Grove (tylko A1)
    void set_grove_type(uint8_t pin, uint8_t type);
    // Tryb pracy portu Grove
    void set_grove_mode(uint8_t pin, uint8_t mode);
    // Stan portu Grove
    void set_grove_state(uint8_t pin, uint8_t state);
    // Wypełnienia sygnału PWM
    void set_grove_pwm_duty(uint8_t pin, uint8_t duty);
    // Stan portu Grove
    uint8_t get_grove_state(uint8_t pin);
    // Wartość na porcie Grove (odczyt w zależności od ustawionego sposobu obsługi)
    int get_grove_value(uint8_t pin);

    // Wykonanie komendy
    void set_motor_command(uint8_t command);
    // Prędkość silnika
    void set_motor_speed(uint8_t motor, uint8_t speed);
    // Bieżąca prędkość silnika
    void get_motor_speed(uint8_t motor, uint8_t *speed);
    // Kierunek i prędkość silnika
    void set_motor_control(uint8_t motor, uint8_t direction, uint8_t speed);

    // Liczba impulsów do osiągnięcia przez enkoder
    void set_motor_encoder(uint8_t motor, int32_t target);
    // Bieżąca liczba impulsów enkodera
    void get_motor_encoder(uint8_t motor, int32_t *value);

    // Przywrócenie stanu początkowego
    void reset();

private:
    void parse_cpuinfo();
    void sleep(unsigned int milliseconds);
    void i2c_write_block(uint8_t command, uint8_t opt1, uint8_t opt2, uint8_t opt3);
    uint8_t i2c_read_byte();

    char *i2c_device_;
    int descriptor_;
};

#endif // GOPIGO_H
