//
// Test biblioteki GoPiGo.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

import java.io.IOException;

public class Test {

    public static final boolean ENABLE_US = false;

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
        }
    }

    public static void main(String[] args) throws IOException, FirmwareVersionException {
        GoPiGo gpg = GoPiGo.getInstance();
        System.out.println("Board            : " + gpg.getBoard());
        System.out.println("Firmware version : " + gpg.getVersionFirmware());
        System.out.println(String.format("Battery voltage  : %.2f V", gpg.getVoltageBattery()));
        System.out.println(String.format("CPU speed        : %d MHz\n", gpg.getCpuSpeed()));

        gpg.setLed(GoPiGo.LED_RIGHT + GoPiGo.LED_LEFT, GoPiGo.ON);  // Diody LED zapalone
        sleep(1000);
        gpg.setLed(GoPiGo.LED_RIGHT + GoPiGo.LED_LEFT, GoPiGo.OFF);  // Diody LED zgaszone

        // Zmiana pozycji serwomechanizmu
        gpg.setServo(20);
        sleep(1000);
        gpg.setServo(160);
        sleep(1000);
        gpg.setServo(90);

        if (ENABLE_US) {
            // Odczyt za pomocą ultradźwiękowego czujnika odległości (Grove - Ultrasonic Ranger)
            gpg.setGroveType(GoPiGo.GROVE_A1, GroveType.US);
            for (int i = 0; i < 20; i++) {
                sleep(500);
                int value = gpg.getGroveValue(GoPiGo.GROVE_A1);
                System.out.println(String.format("Distance (%02d/20): %4d mm", i + 1, value));
            }
        }

        // Do przodu 10 cm
        int pulse_distance_10 = (int) (GoPiGo.ENCODER_PULSE_PER_ROTATION * (10 / GoPiGo.WHEEL_CIRCUMFERENCE));
        gpg.setMotorEncoder(GoPiGo.MOTOR_RIGHT + GoPiGo.MOTOR_LEFT, pulse_distance_10);
        gpg.setMotorCommand(MotorCommand.FORWARD);
        while (true) {
            int[] value = gpg.getMotorEncoder(GoPiGo.MOTOR_RIGHT + GoPiGo.MOTOR_LEFT);
            if (value[0] >= pulse_distance_10 || value[1] >= pulse_distance_10) {
                break;
            }
        }

        // Obrót względem prawego koła o 90 stopni
        int pulse_degrees_90 = (int) (90 / GoPiGo.DEGREES_PER_ENCODER_PULSE);
        gpg.setMotorEncoder(GoPiGo.MOTOR_LEFT, pulse_degrees_90);
        gpg.setMotorCommand(MotorCommand.RIGHT);
        while (true) {
            int[] value = gpg.getMotorEncoder(GoPiGo.MOTOR_LEFT);
            if (value[1] >= pulse_degrees_90) {
                break;
            }
        }

        gpg.reset();
    }
}
