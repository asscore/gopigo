//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

public enum MotorCommand {
    FORWARD,
    BACKWARD,
    RIGHT,
    LEFT,
    RIGHT_ROTATE,
    LEFT_ROTATE,
    STOP;

    static int number = 0;

    private int mId;

    MotorCommand() {
        setId();
    }

    private void setId() {
        mId = ++number;
    }

    public int getId() {
        return mId;
    }
}
