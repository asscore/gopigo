//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.system.SystemInfo;
import java.io.IOException;

public final class GoPiGo {

    public static final byte LED_RIGHT = 0x01;
    public static final byte LED_LEFT = 0x02;

    public static final byte ON = 1;
    public static final byte OFF = 0;

    public static final byte GROVE_A1 = 15;
    public static final byte GROVE_D11 = 10;

    public static final byte LOW = 0;
    public static final byte HIGH = 1;

    public static final int DEFAULT_SPEED = 200;

    public static final byte BACKWARD = 0;
    public static final byte FORWARD = 1;

    public static final byte MOTOR_1 = 0x01;
    public static final byte MOTOR_2 = 0x02;
    public static final byte MOTOR_RIGHT = MOTOR_1;
    public static final byte MOTOR_LEFT = MOTOR_2;

    public static final float WHEEL_DIAMETER = 6.5f;  // Średnica koła w cm
    public static final float WHEEL_CIRCUMFERENCE = (float) (Math.PI * WHEEL_DIAMETER);  // Obwód koła w cm

    public static final int ENCODER_PULSE_PER_ROTATION = 18;  // Liczba impulsów na obrót
    public static final float DEGREES_PER_ENCODER_PULSE = 360 / 64;  // Liczba stopni na impuls
    // (obrót o 360 stopni to około 64 impulsy)

    private final String FIRMWARE_VERSION_REQUIRED = "1.0.x";

    private final byte I2C_ADDR = 0x08;  // Adres I2C nakładki

    private final I2CDevice mDevice;

    private static GoPiGo sInstance;

    public static GoPiGo getInstance() throws IOException, FirmwareVersionException {
        if (sInstance == null) {
            sInstance = new GoPiGo(true);
        }
        return sInstance;
    }

    private GoPiGo(boolean detect) throws IOException, FirmwareVersionException {
        int busId = I2CBus.BUS_1;

        try {
            String type = SystemInfo.getBoardType().name();
            if (type.indexOf("ModelA") > 0) {
                busId = I2CBus.BUS_0;
            }
        } catch (InterruptedException | UnsupportedOperationException ex) {
        }

        try {
            final I2CBus bus = I2CFactory.getInstance(busId);
            mDevice = bus.getDevice(I2C_ADDR);
        } catch (I2CFactory.UnsupportedBusNumberException ex) {
            throw new IOException(ex.getMessage());
        }

        if (detect) {  // Sprawdzenie zgodności z oprogramowaniem nakładki
            try {
                String board = getBoard();
                String vfw = getVersionFirmware();
                if (!board.equals("GoPiGo") && !board.equals("GoPiGo2")) {
                    throw new IOException();
                }
                if (!vfw.split("[.]")[0].equals(FIRMWARE_VERSION_REQUIRED.split("[.]")[0])
                        || !vfw.split("[.]")[1].equals(FIRMWARE_VERSION_REQUIRED.split("[.]")[1])) {
                    throw new FirmwareVersionException(
                            String.format("%s firmware needs to be version %s but is currently version %s.",
                                    board, FIRMWARE_VERSION_REQUIRED, vfw));
                }
            } catch (IOException ex) {
                throw new IOException(String.format("GoPiGo/GoPiGo2 with address 0x%02x not connected.", I2C_ADDR));
            }
        }
    }

    private void sleep(int secs) {
        try {
            Thread.sleep(secs);
        } catch (InterruptedException ex) {
        }
    }

    private void i2cWrite(int... command) throws IOException {
        byte[] buffer = new byte[command.length];
        for (int i = 0; i < command.length; i++) {
            buffer[i] = (byte) command[i];
        }
        mDevice.write(buffer, 0, command.length);
        sleep(5);
    }

    private byte i2cRead() throws IOException {
        byte[] buffer = new byte[1];
        mDevice.read(buffer, 0, buffer.length);
        return buffer[0];
    }

    // Nazwa nakładki
    public String getBoard() throws IOException {
        i2cWrite(I2CMessageType.GET_NAME.getId(), 0, 0, 0);
        sleep(80);

        String name = "";
        for (int i = 0; i < 10; i++) {
            byte b = i2cRead();
            if (b != 0) {
                name += (char) b;
            }
        }

        return name;
    }

    // Wersja oprogramowania
    public String getVersionFirmware() throws IOException {
        i2cWrite(I2CMessageType.GET_FIRMWARE_VERSION.getId(), 0, 0, 0);
        sleep(80);

        byte[] number = new byte[3];
        for (int i = 0; i < 3; i++) {
            number[i] = i2cRead();
        }

        return String.format("%s.%s.%s", number[0], number[1], number[2]);
    }

    // Poziom naładowania baterii w V
    public float getVoltageBattery() throws IOException {
        i2cWrite(I2CMessageType.GET_VOLTAGE_VCC.getId(), 0, 0, 0);
        sleep(80);

        byte[] b = new byte[2];
        for (int i = 0; i < 2; i++) {
            b[i] = i2cRead();
        }
        float voltage = -1;
        if (b[0] != -1 && b[1] != -1) {
            voltage = b[0] * 256 + b[1];
            voltage = (float) ((5.0 * voltage / 1024) / .4);
        }

        return voltage;
    }

    // Częstotliwość zegara procesora nakładki w MHz
    public byte getCpuSpeed() throws IOException {
        i2cWrite(I2CMessageType.GET_CPU_SPEED.getId(), 0, 0, 0);
        sleep(80);

        return i2cRead();
    }

    // Stan diód LED
    void setLed(int led, byte state) throws IOException {
        i2cWrite(I2CMessageType.SET_LED.getId(), led, state, 0);
    }

    // Pozycja serwomechanizmu w stopniach
    void setServo(int position) throws IOException {
        i2cWrite(I2CMessageType.SET_SERVO.getId(), position, 0, 0);
    }

    // Sposób obsługi portu Grove (tylko A1)
    void setGroveType(byte pin, GroveType type) throws IOException {
        i2cWrite(I2CMessageType.SET_GROVE_TYPE.getId(), pin, type.getId(), 0);
    }

    // Tryb pracy portu Grove
    void setGroveMode(byte pin, GroveMode mode) throws IOException {
        i2cWrite(I2CMessageType.SET_GROVE_MODE.getId(), pin, mode.getId(), 0);
    }

    // Stan portu Grove
    void setGroveState(byte pin, byte state) throws IOException {
        i2cWrite(I2CMessageType.SET_GROVE_STATE.getId(), pin, state, 0);
    }

    // Wypełnienia sygnału PWM
    void setGrovePwmDuty(byte pin, byte duty) throws IOException {
        i2cWrite(I2CMessageType.SET_GROVE_PWM_DUTY.getId(), pin, duty, 0);
    }

    // Stan portu Grove
    byte getGroveState(byte pin) throws IOException {
        i2cWrite(I2CMessageType.GET_GROVE_STATE.getId(), pin, 0, 0);
        sleep(80);

        return i2cRead();
    }

    // Wartość na porcie Grove (odczyt w zależności od ustawionego sposobu obsługi)
    int getGroveValue(byte pin) throws IOException {
        i2cWrite(I2CMessageType.GET_GROVE_VALUE.getId(), pin, 0, 0);
        sleep(80);

        byte[] b = new byte[2];
        for (int i = 0; i < 2; i++) {
            b[i] = i2cRead();
        }
        int value = -1;
        if (b[0] != -1 && b[1] != -1) {
            value = b[0] * 256 + b[1];
        }

        return value;
    }

    // Wykonanie komendy
    void setMotorCommand(MotorCommand command) throws IOException {
        i2cWrite(I2CMessageType.SET_MOTOR_COMMAND.getId(), command.getId(), 0, 0);
    }

    // Prędkość silnika
    void setMotorSpeed(int motor, int speed) throws IOException {
        i2cWrite(I2CMessageType.SET_MOTOR_SPEED.getId(), motor, speed, 0);
    }

    // Bieżąca prędkość silnika
    int[] getMotorSpeed(int motor) throws IOException {
        i2cWrite(I2CMessageType.GET_MOTOR_SPEED.getId(), motor, 0, 0);
        sleep(80);

        int[] speed = new int[2];
        if ((motor & MOTOR_1) != 0) {
            speed[0] = i2cRead() & 0xFF;
        }
        if ((motor & MOTOR_2) != 0) {
            speed[1] = i2cRead() & 0xFF;
        }

        return speed;
    }

    // Kierunek i prędkość silnika
    void setMotorControl(int motor, byte direction, int speed) throws IOException {
        i2cWrite(I2CMessageType.SET_MOTOR_CONTROL.getId(), motor, direction, speed);
    }

    // Liczba impulsów do osiągnięcia przez enkoder
    void setMotorEncoder(int motor, int target) throws IOException {
        i2cWrite(I2CMessageType.SET_MOTOR_ENCODER.getId(), motor, target / 256, target % 256);
    }

    // Bieżąca liczba impulsów enkodera
    int[] getMotorEncoder(int motor) throws IOException {
        i2cWrite(I2CMessageType.GET_MOTOR_ENCODER.getId(), motor, 0, 0);
        sleep(80);

        byte[] b = new byte[2];
        int[] value = new int[2];
        if ((motor & MOTOR_1) != 0) {
            for (int i = 0; i < 2; i++) {
                b[i] = i2cRead();
            }
            if (b[0] != -1 && b[1] != -1) {
                value[0] = b[0] * 256 + b[1];
            } else {
                value[0] = -1;
            }
        }
        if ((motor & MOTOR_2) != 0) {
            for (int i = 0; i < 2; i++) {
                b[i] = i2cRead();
            }
            if (b[0] != -1 && b[1] != -1) {
                value[1] = b[0] * 256 + b[1];
            } else {
                value[1] = -1;
            }
        }

        return value;
    }

    // Przywrócenie stanu początkowego
    void reset() throws IOException {
        i2cWrite(I2CMessageType.RESET.getId(), 0, 0, 0);
    }
}
