//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

public class FirmwareVersionException extends Exception {

    public FirmwareVersionException(String s) {
        super(s);
    }

    public FirmwareVersionException() {
        super();
    }
}
