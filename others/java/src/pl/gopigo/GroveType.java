//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

public enum GroveType {
    CUSTOM,
    US;

    static int number = 0;

    private int mId;

    GroveType() {
        setId();
    }

    private void setId() {
        mId = ++number;
    }

    public int getId() {
        return mId;
    }
}
