//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

public enum I2CMessageType {
    NONE,
    GET_NAME,
    GET_FIRMWARE_VERSION,
    GET_VOLTAGE_VCC,
    GET_CPU_SPEED,
    SET_LED,
    SET_SERVO,
    SET_GROVE_TYPE,
    SET_GROVE_MODE,
    SET_GROVE_STATE,
    SET_GROVE_PWM_DUTY,
    GET_GROVE_STATE,
    GET_GROVE_VALUE,
    SET_MOTOR_COMMAND,
    SET_MOTOR_SPEED,
    GET_MOTOR_SPEED,
    SET_MOTOR_CONTROL,
    SET_MOTOR_ENCODER,
    GET_MOTOR_ENCODER,
    RESET;

    static int number = 0;

    private int mId;

    I2CMessageType() {
        setId();
    }

    private void setId() {
        mId = number++;
    }

    public int getId() {
        return mId;
    }
}
