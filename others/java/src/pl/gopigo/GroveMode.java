//
// Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
//
// Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
//
package pl.gopigo;

public enum GroveMode {
    INPUT,
    OUTPUT,
    INPUT_PULLUP;

    static int number = 0;

    private int mId;

    GroveMode() {
        setId();
    }

    private void setId() {
        mId = number++;
    }

    public int getId() {
        return mId;
    }
}
