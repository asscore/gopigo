# -*- coding: utf-8 -*-
#
# Interfejs do Scratch.
#
# Copyright (c) 2018, 2019 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import atexit
import os
import re
import sys
import time
import ConfigParser as configparser

import scratch

import gopigo
import ir_receiver
import line_follower
from modules import dexter

gpg = None
lf = None
s = None
ds = None

regex_us_sensor = '^GET(\s|_)DIST(ANCE)?'
comp_regex_us_sensor = re.compile(regex_us_sensor, re.IGNORECASE)


@atexit.register
def cleanup():
    try:
        gpg.reset()
    except:
        pass


def distance_to_pulse(distance):
    return int(gpg.ENCODER_PULSE_PER_ROTATION * (distance / gpg.WHEEL_CIRCUMFERENCE))


def degrees_to_pulse(degrees):
    return int(degrees / gpg.DEGREES_PER_ENCODER_PULSE)


if __name__ == '__main__':
    try:
        gpg = gopigo.GoPiGo()
    except gpg.FirmwareVersionError:
        sys.exit()
    except IOError:
        sys.exit()

    try:
        lf = line_follower.LineFollower()
    except IOError:
        pass

    try:
        ds = dexter.Distance()
    except IOError:
        gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.US)

    config = configparser.ConfigParser()
    config.read('scratch.cfg')

    connected = 0
    show_error = True
    while connected == 0:
        try:
            s = scratch.Scratch(os.getenv('SSH_CLIENT', config.get('scratch', 'hostname')).split(' ')[0],
                                config.getint('scratch', 'port'))
            if s.connected:
                print('GoPiGo Scratch: Connected to Scratch successfully.')
            connected = 1
        except KeyboardInterrupt:
            sys.exit()
        except scratch.ScratchError:
            if show_error:
                print('GoPiGo Scratch: Scratch is either not opened or remote sensor connections aren''t enabled.')
                show_error = False

    try:
        s.broadcast('READY')
    except NameError:
        print('GoPiGo Scratch: Unable to Broadcast.')

    while True:
        try:
            m = s.receive()

            while m is None or m[0] == 'sensor-update':
                m = s.receive()

            msg = m[1]
            if msg.upper() == 'STOP':  # Zatrzymanie
                gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)

            elif msg[:7].upper() == 'FORWARD':  # Do przodu
                if len(msg) > 7:
                    distance = int(msg[7:])
                    pulse = distance_to_pulse(distance)
                    gpg.set_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, pulse)
                gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)

            elif msg[:8].upper() == 'BACKWARD':  # Do tyłu
                if len(msg) > 8:
                    distance = int(msg[8:])
                    pulse = distance_to_pulse(distance)
                    gpg.set_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, pulse)
                gpg.set_motor_command(gpg.MOTOR_COMMAND.BACKWARD)

            elif msg[:5].upper() == 'RIGHT':  # Obrót względem prawego koła
                if len(msg) > 5:
                    degrees = int(msg[5:])
                    pulse = degrees_to_pulse(degrees)
                    gpg.set_motor_encoder(gpg.MOTOR_LEFT, pulse)
                gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT)

            elif msg[:4].upper() == 'LEFT':  # Obrót względem lewego koła
                if len(msg) > 4:
                    degrees = int(msg[4:])
                    pulse = degrees_to_pulse(degrees)
                    gpg.set_motor_encoder(gpg.MOTOR_RIGHT, pulse)
                gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT)

            elif msg[:5].upper() == 'SPEED':  # Prędkość silnika (0 - 255)
                speed = int(msg[5:])
                gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, speed)

            elif msg[:9].upper() == 'LED RIGHT':  # Stan prawej diody LED
                state = None
                if msg[9:].upper() == 'ON':
                    state = gpg.ON
                elif msg[9:].upper() == 'OFF':
                    state = gpg.OFF
                if state is not None:
                    gpg.set_led(gpg.LED_RIGHT, state)

            elif msg[:8].upper() == 'LED LEFT':  # Stan lewej diody LED
                state = None
                if msg[8:].upper() == 'ON':
                    state = gpg.ON
                elif msg[8:].upper() == 'OFF':
                    state = gpg.OFF
                if state is not None:
                    gpg.set_led(gpg.LED_LEFT, state)

            elif msg[:5].upper() == 'SERVO':  # Pozycja serwomechanizmu w stopniach (0 - 180)
                angle = max(0, min(180, int(msg[5:])))
                gpg.set_servo(angle)

            elif comp_regex_us_sensor.match(msg):  # Odległość w cm
                if ds is not None:
                    distance = ds.read_range_single() // 10
                    s.sensorupdate({'distance': distance})
                else:
                    raw_data = float(gpg.get_grove_value(gpg.GROVE_A1))
                    if raw_data > 0:
                        corrected_data = (raw_data + 4.41) / 1.423
                    else:
                        corrected_data = raw_data
                    s.sensorupdate({'distance': int(corrected_data // 10)})

            elif msg.upper() == 'IR':  # Kod wciśniętego klawisza
                code = ir_receiver.nextcode()
                s.sensorupdate({'ir': code})

            elif msg.upper() == 'LINE':  # Położenie czarnej linii
                if lf is not None:
                    position = lf.read_position()
                    s.sensorupdate({'line': position})

            elif msg.upper() == 'RESET':  # Przywrócenie stanu początkowego
                gpg.reset()

        except (scratch.ScratchError, KeyboardInterrupt):
            print('GoPiGo Scratch: Disconnected from Scratch.')
            break
        except (scratch.scratch.ScratchConnectionError, NameError):
            show_error = True
            while True:
                if show_error:
                    print('GoPiGo Scratch: Scratch connection error, retrying...')
                    show_error = False
                try:
                    time.sleep(5)
                    s = scratch.Scratch(os.getenv('SSH_CLIENT', config.get('scratch', 'hostname')).split(' ')[0],
                                        config.getint('scratch', 'port'))
                    s.broadcast('READY')
                    print('GoPiGo Scratch: Connected to Scratch successfully.')
                    break
                except KeyboardInterrupt:
                    sys.exit()
                except scratch.ScratchError:
                    pass
        except ValueError:
            pass
