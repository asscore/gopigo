# -*- coding: utf-8 -*-
#
# Test modułu PivotPi.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import pivotpi

pp = pivotpi.PivotPi()

pp.set_led_brightness(pp.LED_2, 80)
pp.set_angle(pp.SERVO_2, 0)
pp.set_led_brightness(pp.LED_4, 100)
pp.set_angle(pp.SERVO_4, 0)

time.sleep(2.)

pp.set_led_brightness(pp.LED_2, 0)
pp.set_angle(pp.SERVO_2, 90)
pp.set_led_brightness(pp.LED_4, 0)
pp.set_angle(pp.SERVO_4, 90)
