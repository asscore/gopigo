# -*- coding: utf-8 -*-
#
# Test modułu z odbiornikiem podczerwieni (Grove - Infrared Receiver).
#  http://wiki.seeedstudio.com/Grove-Infrared_Receiver/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import ir_receiver

while True:
    try:
        key = ir_receiver.nextcode()
        if len(key) != 0:
            print(key)
        time.sleep(.1)
    except KeyboardInterrupt:
        break
