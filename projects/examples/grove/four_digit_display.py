# -*- coding: utf-8 -*-
#
# Test modułu z 4 cyfrowym wyświetlaczem 7-segmentowym (Grove - 4 Digit Display).
#  http://wiki.seeedstudio.com/Grove-4-Digit_Display/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO

from modules import grove

CLK_PIN = 23  # GPIO 23
DIO_PIN = 24  # GPIO 24

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

four_digit = grove.FourDigit(CLK_PIN, DIO_PIN)

while True:
    try:
        four_digit.set_number(1, False)  # Bez zer wiodących
        time.sleep(.5)
        four_digit.set_number(12, False)
        time.sleep(.5)
        four_digit.set_number(123, False)
        time.sleep(.5)
        four_digit.set_number(1234, False)
        time.sleep(.5)

        four_digit.set_number(1, True)  # Z zerami wiodącymi
        time.sleep(.5)
        four_digit.set_number(12, True)
        time.sleep(.5)
        four_digit.set_number(123, True)
        time.sleep(.5)
        four_digit.set_number(1234, True)
        time.sleep(.5)

        four_digit.set_doublepoint(True)
        four_digit.set_digit(0, 2)
        four_digit.set_digit(1, 6)
        four_digit.set_digit(2, 9)
        four_digit.set_digit(3, 15)  # F
        four_digit.set_doublepoint(False)
        time.sleep(1.)

        four_digit.set_brightness(0)
        four_digit.set_segment(0, 57)  # C
        four_digit.set_segment(1, 63)  # O
        four_digit.set_segment(2, 63)  # O
        four_digit.set_segment(3, 56)  # L

        for i in range(1, 8):
            four_digit.set_brightness(i)
            time.sleep(.2)
        time.sleep(.3)
    except KeyboardInterrupt:
        four_digit.clear()
        GPIO.cleanup(CLK_PIN)
        GPIO.cleanup(DIO_PIN)
        break
