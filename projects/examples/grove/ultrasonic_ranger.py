# -*- coding: utf-8 -*-
#
# Test modułu z ultradźwiękowym czujnikiem odległości (Grove - Ultrasonic Ranger).
#  http://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.US)

while True:
    try:
        time.sleep(.05)
        raw_data = float(gpg.get_grove_value(gpg.GROVE_A1))
        if raw_data > 0:
            corrected_data = (raw_data + 4.41)  / 1.423
        else:
            corrected_data = raw_data
        print('Distance: %d mm' % int(corrected_data))
    except KeyboardInterrupt:
        gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
        break
