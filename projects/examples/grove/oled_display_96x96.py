# -*- coding: utf-8 -*-
#
# Test modułu wyświetlacza OLED 1.12 cala o rozdzielczości 96x96 (Grove - OLED Display 1.12").
#  http://wiki.seeedstudio.com/Grove-OLED_Display_1.12inch/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import grove

oled = grove.OLED(grove.OLED.DISPLAY_96X96)
oled.set_contrast(127)
oled.clear()

for i in range(12):
    oled.set_position(i, 0)
    oled.set_text('Hello world')
