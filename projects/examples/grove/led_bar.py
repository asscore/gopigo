# -*- coding: utf-8 -*-
#
# Test modułu z wyświetlaczem LED linijka (Grove - LED Bar).
#  http://wiki.seeedstudio.com/Grove-LED_Bar/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO

from modules import grove

DI_PIN = 23  # GPIO 23
DCKI_PIN = 24  # GPIO 24

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

led_bar = grove.LEDBar(DI_PIN, DCKI_PIN)

try:
    led_bar.set_level(6)  # Diody od 1 - 6 zapalone, pozostałe zgaszone
    time.sleep(1.)
    led_bar.set_reverse(True)  # Zmiana kierunku wyświetlania
    time.sleep(1.)
    led_bar.toggle_led(1)  # Dioda 1 zgaszona
    time.sleep(1.)
    led_bar.set_led(1, 1)  # Dioda 1 zapalona
    time.sleep(1.)
    led_bar.set_bits(0x155)  # Diody 1, 3, 5, 7, 9 zapalone, pozostałe zgaszone
    time.sleep(1.)
    led_bar.set_level(0)
except KeyboardInterrupt:
    led_bar.set_level(0)

GPIO.cleanup(DI_PIN)
GPIO.cleanup(DCKI_PIN)
