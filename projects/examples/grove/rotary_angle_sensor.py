# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem obrotowym (Grove - Rotary Angle Sensor).
#  http://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

ADC_REF = 5
GROVE_VCC = 5
FULL_ANGLE = 300

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

while True:
    try:
        sensor_value = gpg.get_grove_value(gpg.GROVE_A1)
        voltage = sensor_value * ADC_REF / 1021  # Powinno być 1023 !?
        degrees = (voltage * FULL_ANGLE) / GROVE_VCC
        print('Sensor value: %d Voltage: %.2f Degrees: %.1f' % (sensor_value, voltage, degrees))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
