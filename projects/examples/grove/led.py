# -*- coding: utf-8 -*-
#
# Test modułu z diodą LED (Grove - LED).
#  http://wiki.seeedstudio.com/Grove-LED_Socket_Kit/
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_D11, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_D11, gpg.GROVE_MODE.OUTPUT)

try:
    gpg.set_grove_state(gpg.GROVE_D11, gpg.HIGH)
    time.sleep(2.)
    gpg.set_grove_state(gpg.GROVE_D11, gpg.LOW)
    time.sleep(1.)
    for i in range(0, 256, 5):
        gpg.set_grove_pwm_duty(gpg.GROVE_D11, i)
        time.sleep(.05)
except KeyboardInterrupt:
    pass

gpg.set_grove_state(gpg.GROVE_D11, gpg.LOW)
gpg.set_grove_mode(gpg.GROVE_D11, gpg.GROVE_MODE.INPUT)
