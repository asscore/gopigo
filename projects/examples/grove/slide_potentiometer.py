# -*- coding: utf-8 -*-
#
# Test modułu z potencjometrem przesuwnym (Grove - Slide Potentiometer).
#  http://wiki.seeedstudio.com/Grove-Slide_Potentiometer/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

while True:
    try:
        print(gpg.get_grove_value(gpg.GROVE_A1))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
