# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem światła (Grove - Light Sensor).
#  http://wiki.seeedstudio.com/Grove-Light_Sensor/
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

while True:
    try:
        sensor_value = gpg.get_grove_value(gpg.GROVE_A1)
        percent = sensor_value * 100 / 1023
        print('Sensor value: %d (%.2f %%)' % (sensor_value, percent))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
