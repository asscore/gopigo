# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem czystości powietrza (Grove - Air Quality Sensor).
#  http://wiki.seeedstudio.com/Grove-Air_Quality_Sensor_v1.3/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

while True:
    try:
        sensor_value = gpg.get_grove_value(gpg.GROVE_A1)
        if sensor_value > 700:
            print('High pollution')
        elif sensor_value > 300:
            print('Low pollution')
        else:
            print('Air fresh')
        print('Sensor value: %d' % sensor_value)
        time.sleep(.5)
    except KeyboardInterrupt:
        break
