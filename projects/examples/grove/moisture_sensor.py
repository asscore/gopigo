# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem wilgoci (Grove - Moisture Sensor).
#  http://wiki.seeedstudio.com/Grove-Moisture_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

# Czujnik na powietrzu: 0
# Czujnik w suchej glebie: 0 - 300
# Czujnik w wilgotnej glebie: 300 - 700
# Czujnik w wodzie: 700 - 950

while True:
    try:
        # Zaobserwowane wartości:
        #   0 - czujnik na powietrzu
        #  18 - czujnik w suchej glebie
        # 425 - czujnik w wilgotnej glebie
        # 690 - czujnik w wodzie
        print(gpg.get_grove_value(gpg.GROVE_A1))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
