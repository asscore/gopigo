# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem koloru (Grove - I2C Color Sensor).
#  http://wiki.seeedstudio.com/Grove-I2C_Color_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

lc = grove.LightColor(grove.LightColor.INTEGRATIONTIME_700MS, grove.LightColor.GAIN_1X)

while True:
    try:
        red, green, blue, clear = lc.read_raw_color()
        print('Color: red = %d green = %d blue = %d clear = %d' % (red, green, blue, clear))

        color_temperature = grove.LightColor.calculate_color_temperature(red, green, blue)
        if color_temperature is None:
            print('Too dark to determine color temperature!')
        else:
            print('Color temperature: %d K' % color_temperature)

        lux = grove.LightColor.calculate_lux(red, green, blue)
        print('Luminosity: %d lx' % lux)
        time.sleep(.02)
    except KeyboardInterrupt:
        break
