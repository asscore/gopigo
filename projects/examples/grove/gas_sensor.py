# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem gazów (Grove - Gas Sensor).
#  http://wiki.seeedstudio.com/Grove-Gas_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

# Czujnik dymu i łatwopalnych gazów (Grove - Gas Sensor(MQ2))
#  http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ2/
# Czujnik alkoholu (Grove - Gas Sensor(MQ3))
#  http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ3/
# Czujnik LPG, gazu ziemnego i gazu węglowego (Grove - Gas Sensor(MQ5))
#  http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ5/
# Czujnik tlenku węgla i łatwopalnych gazów (Grove - Gas Sensor(MQ9))
#  http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ9/

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

while True:
    try:
        sensor_value = gpg.get_grove_value(gpg.GROVE_A1)
        density = sensor_value / 1024
        print('Sensor value: %d Density: %.2f' % (sensor_value, density))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
