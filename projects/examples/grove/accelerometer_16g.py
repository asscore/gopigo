# -*- coding: utf-8 -*-
#
# Test modułu z 3-osiowym akcelerometrem cyfrowym (Grove - 3 Axis Digital Accelerometer(+/-16g)).
#  http://wiki.seeedstudio.com/Grove-3-Axis_Digital_Accelerometer-16g/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

acc = grove.Accelerometer()

while True:
    try:
        x, y, z = acc.read(True)
        print('X: %.03f g Y: %.03f g Z: %.03f g' % (x, y, z))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
