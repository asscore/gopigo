# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem wilgotności, ciśnienia i temperatury (Grove - Barometer Sensor (BME280)).
#  http://wiki.seeedstudio.com/Grove-Barometer_Sensor-BME280/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

bs = grove.Barometer()

while True:
    try:
        temperature, pressure, humidity = bs.read()
        print('Temperature: %.02f C Pressure: %.02f hPa Humidity: %.02f %%' % (temperature, pressure, humidity))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
