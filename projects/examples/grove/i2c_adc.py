# -*- coding: utf-8 -*-
#
# Test modułu z przetwornikiem analogowo-cyfrowym o 12-bitowej precyzji (Grove - I2C ADC).
#  http://wiki.seeedstudio.com/Grove-I2C_ADC/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

V_REF = 3.167336838849366  # Wartość dobrana przy wykorzystaniu Grove - Rotary Angle Sensor

adc = grove.ADC()

while True:
    try:
        sensor_value = adc.read()
        voltage = sensor_value * V_REF * 2 / 4096
        print('Sensor value: %d (%.2f V)' % (sensor_value, voltage))
        time.sleep(.5)
    except KeyboardInterrupt:
        break
