# -*- coding: utf-8 -*-
#
# Test modułu z przekaźnikiem (Grove - Relay).
#  http://wiki.seeedstudio.com/Grove-Relay/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_D11, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_D11, gpg.GROVE_MODE.OUTPUT)

try:
    gpg.set_grove_state(gpg.GROVE_D11, gpg.HIGH)
    time.sleep(1.)
    gpg.set_grove_state(gpg.GROVE_D11, gpg.LOW)
except KeyboardInterrupt:
    gpg.set_grove_state(gpg.GROVE_D11, gpg.LOW)

gpg.set_grove_mode(gpg.GROVE_D11, gpg.GROVE_MODE.INPUT)
