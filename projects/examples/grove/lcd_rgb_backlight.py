# -*- coding: utf-8 -*-
#
# Test modułu wyświetlacza LCD RGB z podświetleniem (Grove - LCD RGB Backlight).
#  http://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import grove

heart = [0b00000,
         0b01010,
         0b11111,
         0b11111,
         0b11111,
         0b01110,
         0b00100,
         0b00000]

lcd = grove.LCD()
lcd.set_backlight(0, 255, 0)
lcd.create_char(0, heart)
lcd.set_text('I %c GoPiGo\nLCD test' % chr(0))
