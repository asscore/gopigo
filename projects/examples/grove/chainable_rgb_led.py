# -*- coding: utf-8 -*-
#
# Test modułu z diodą LED RGB (Grove - Chainable RGB LED).
#  http://wiki.seeedstudio.com/Grove-Chainable_RGB_LED/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO

from modules import grove

CI_PIN = 23  # GPIO 23
DI_PIN = 24  # GPIO 24
NUMBER_OF_LEDS = 1

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

rgb_led = grove.ChainableLED(CI_PIN, DI_PIN, NUMBER_OF_LEDS)

while True:
    try:
        rgb_led.set_color(0, 255, 0, 0)
        time.sleep(2.)
        rgb_led.set_color(0, 0, 255, 0)
        time.sleep(2.)
        rgb_led.set_color(0, 0, 0, 255)
        time.sleep(2.)
        rgb_led.set_color(0, 0, 255, 255)
        time.sleep(2.)
        rgb_led.set_color(0, 255, 0, 255)
        time.sleep(2.)
        rgb_led.set_color(0, 255, 255, 0)
        time.sleep(2.)
        rgb_led.set_color(0, 255, 255, 255)
        time.sleep(2.)
    except KeyboardInterrupt:
        rgb_led.set_color(0, 0, 0, 0)
        GPIO.cleanup(CI_PIN)
        GPIO.cleanup(DI_PIN)
        break
