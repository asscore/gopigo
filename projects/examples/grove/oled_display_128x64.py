# -*- coding: utf-8 -*-
#
# Test modułu wyświetlacza OLED 0.96 cala o rozdzielczości 128x64 (Grove - OLED Display 0.96").
#  http://wiki.seeedstudio.com/Grove-OLED_Display_0.96inch/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import grove

oled = grove.OLED(grove.OLED.DISPLAY_128X64)
oled.set_contrast(127)
oled.clear()

for i in range(8):
    oled.set_position(i, 0)
    oled.set_text('Hello world')
