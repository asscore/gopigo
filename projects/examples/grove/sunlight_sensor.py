# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem światła UV (Grove - Sunlight Sensor).
#  http://wiki.seeedstudio.com/Grove-Sunlight_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

s = grove.Sunlight()

while True:
    try:
        uv = s.read_uv()
        vi = s.read_visible()
        ir = s.read_ir()
        print('UV Index: %.02f, Visible: %d lm, IR: %d lm' % (uv, vi, ir))
        time.sleep(1.)
    except KeyboardInterrupt:
        break
