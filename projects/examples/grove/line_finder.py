# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem linii (Grove - Line Finder).
#  http://wiki.seeedstudio.com/Grove-Line_Finder/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_D11, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_D11, gpg.GROVE_MODE.INPUT)

while True:
    try:
        if gpg.get_grove_state(gpg.GROVE_D11) == gpg.HIGH:
            print('Black line detected')
        else:
            print('White line detected')
        time.sleep(.5)
    except KeyboardInterrupt:
        break
