# -*- coding: utf-8 -*-
#
# Test modułu z 3-osiowym akcelerometrem, żyroskopem i magnetometrem (Grove - IMU 9DOF).
#  http://wiki.seeedstudio.com/Grove-IMU_9DOF_v2.0/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

imu = grove.IMU()

while True:
    try:
        ax, ay, az = imu.read_accelerometer()
        print('Accelerometer: x = %.03f y = %.03f z = %.03f' % (ax, ay, az))
        gx, gy, gz = imu.read_gyroscope()
        print('Gyroscope: x = %.03f y = %.03f z = %.03f' % (gx, gy, gz))
        mx, my, mz = imu.read_magnetometer()
        print('Magnetometer: x = %.03f y = %.03f z = %.03f' % (mx, my, mz))
        temperature = imu.read_temperature()
        print('Temperature: %.03f C' % temperature)
        time.sleep(.5)
    except KeyboardInterrupt:
        break
