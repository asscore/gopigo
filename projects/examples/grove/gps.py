# -*- coding: utf-8 -*-
#
# Test modułu GPS UART z anteną (Grove - GPS).
#  http://wiki.seeedstudio.com/Grove-GPS/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import grove

gps = grove.GPS()

while True:
    try:
        timestamp, latitude, longitude, quality, satellites, altitude = gps.read()
        output = 'Timestamp: %s ' \
                 'Quality: %d ' \
                 'Satellites: %d ' \
                 'Altitude: %.2f ' \
                 'Coordinates: %.6f, %.6f' % (timestamp, quality, satellites, altitude, latitude, longitude)
        print(output)
    except KeyboardInterrupt:
        break
