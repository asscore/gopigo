# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem temperatury (Grove - Temperature Sensor).
#  http://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math
import time

import gopigo

MODEL = '1.2'

gpg = gopigo.GoPiGo()
gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.CUSTOM)
gpg.set_grove_mode(gpg.GROVE_A1, gpg.GROVE_MODE.INPUT)

if MODEL == '1.2':
    b = 4250  # Termistor NCP18WF104F03RC
elif MODEL == '1.1':
    b = 4250  # Termistor NCP18WF104F03RC
else:
    b = 3975  # Termistor TTC3A103*39H

while True:
    try:
        sensor_value = gpg.get_grove_value(gpg.GROVE_A1)
        resistance = (1023 - sensor_value) * 10000 / sensor_value
        temperature = 1 / (math.log(resistance / 10000) / b + 1 / 298.15) - 273.15
        print('Temperature: %.02f C' % temperature)
        time.sleep(.5)
    except KeyboardInterrupt:
        break
