# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem temperatury i wilgotności (Grove - Temperature and Humidity Sensor).
#  http://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math
import time

import RPi.GPIO as GPIO

from modules import grove

PIN = 23  # GPIO 23

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

dht = grove.DHT(PIN)

while True:
    try:
        temperature, humidity = dht.read()
        if math.isnan(temperature) == False and math.isnan(humidity) == False:
            print('Temperature: %.02f C Humidity: %.02f %%' % (temperature, humidity))
        time.sleep(.5)
    except KeyboardInterrupt:
        GPIO.cleanup(PIN)
        break
