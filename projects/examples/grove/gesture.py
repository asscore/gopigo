# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem gestów (Grove - Gesture).
#  http://wiki.seeedstudio.com/Grove-Gesture_v1.0/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove

g = grove.Gesture()

while True:
    try:
        gesture = g.read()
        if gesture == g.FORWARD:
            print('FORWARD')
        elif gesture == g.BACKWARD:
            print('BACKWARD')
        elif gesture == g.RIGHT:
            print('RIGHT')
        elif gesture == g.LEFT:
            print('LEFT')
        elif gesture == g.UP:
            print('UP')
        elif gesture == g.DOWN:
            print('DOWN')
        elif gesture == g.CLOCKWISE:
            print('CLOCKWISE')
        elif gesture == g.ANTI_CLOCKWISE:
            print('ANTI_CLOCKWISE')
        elif gesture == g.WAVE:
            print('WAVE')
        elif gesture == g.NOTHING:
            print('-')
        time.sleep(.1)
    except KeyboardInterrupt:
        break
