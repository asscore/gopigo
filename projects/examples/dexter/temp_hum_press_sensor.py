# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem temperatury, wilgotności i ciśnienia.
#  Dexter Industries Temperature Humidity & Pressure Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import dexter

thp = dexter.TempHumPress()

while True:
    try:
        temperature, pressure, humidity = thp.read()
        print('Temperature: %.02f C Pressure: %.02f hPa Humidity: %.02f %%' % (temperature, pressure, humidity))
        time.sleep(.02)
    except KeyboardInterrupt:
        break
