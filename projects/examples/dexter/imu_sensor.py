# -*- coding: utf-8 -*-
#
# Test modułu z 3-osiowym akcelerometrem, żyroskopem i magnetometrem.
#  Dexter Industries Inertial Measurement Unit Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import dexter

imu = dexter.IMU()

while True:
    try:
        m = imu.read_magnetometer()
        g = imu.read_gyroscope()
        a = imu.read_accelerometer()
        e = imu.read_euler()
        t = imu.read_temperature()
        output = 'Magnetometer:  x = %.1f y = %.1f z = %.1f\n' \
                 'Gyroscope: x = %.1f y = %.1f z = %.1f\n' \
                 'Accelerometer: x = %.1f y = %.1f z = %.1f\n' \
                 'Euler: heading = %.1f roll = %.1f pitch = %.1f\n' \
                 'Temperature: %.1f C' % (m[0], m[1], m[2], g[0], g[1], g[2], a[0], a[1], a[2], e[0], e[1], e[2], t)
        print(output)
        time.sleep(.1)
    except KeyboardInterrupt:
        break
