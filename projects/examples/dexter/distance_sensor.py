# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem odległości.
#  Dexter Industries Distance Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import dexter

ds = dexter.Distance()
ds.start_continuous()

while True:
    try:
        distance = ds.read_range_continuous()
        print('Distance: %d mm' % distance)
        time.sleep(.1)
    except KeyboardInterrupt:
        break
