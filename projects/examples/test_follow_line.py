# -*- coding: utf-8 -*-
#
# Test modułu Line Follower.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo
import line_follower

fwd_speed = 80
slight_turn_speed = int(.7 * fwd_speed)

gpg = gopigo.GoPiGo()
lf = line_follower.LineFollower()

try:
    while True:
        position = lf.read_position()
        if position == 'center':
            gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, fwd_speed)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
        elif position == 'right':
            gpg.set_motor_speed(gpg.MOTOR_RIGHT, 0)
            gpg.set_motor_speed(gpg.MOTOR_LEFT, slight_turn_speed)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
        elif position == 'left':
            gpg.set_motor_speed(gpg.MOTOR_RIGHT, slight_turn_speed)
            gpg.set_motor_speed(gpg.MOTOR_LEFT, 0)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
        elif position == 'black':
            gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)
        elif position == 'white':
            gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)
        time.sleep(.01)
except KeyboardInterrupt:
    gpg.reset()
