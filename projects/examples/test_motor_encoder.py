# -*- coding: utf-8 -*-
#
# Test enkoderów.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import gopigo

gpg = gopigo.GoPiGo()
gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, 150)

pulse_distance_10 = int(gpg.ENCODER_PULSE_PER_ROTATION * (10 / gpg.WHEEL_CIRCUMFERENCE))
pulse_degrees_45 = int(45 / gpg.DEGREES_PER_ENCODER_PULSE)

try:
    # Do przodu 10 cm
    gpg.set_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, pulse_distance_10)
    gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
    while True:
       e1, e2 = gpg.get_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT)
       if e1 >= pulse_distance_10 or e2 >= pulse_distance_10:
          break

    # Obrót względem prawego koła o 45 stopni
    gpg.set_motor_encoder(gpg.MOTOR_LEFT, pulse_degrees_45)
    gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT)
    while True:
       e2 = gpg.get_motor_encoder(gpg.MOTOR_LEFT)
       if e2 >= pulse_degrees_45:
          break

    # Obrót względem lewego koła o 45 stopni
    gpg.set_motor_encoder(gpg.MOTOR_RIGHT, pulse_degrees_45)
    gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT)
    while True:
       e1 = gpg.get_motor_encoder(gpg.MOTOR_RIGHT)
       if e1 >= pulse_degrees_45:
          break

    # Do tyłu 10 cm
    gpg.set_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, pulse_distance_10)
    gpg.set_motor_command(gpg.MOTOR_COMMAND.BACKWARD)
    while True:
       e1, e2 = gpg.get_motor_encoder(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT)
       if e1 >= pulse_distance_10 or e2 >= pulse_distance_10:
          break
except KeyboardInterrupt:
    pass

gpg.reset()
