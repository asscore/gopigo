# -*- coding: utf-8 -*-
#
# Test biblioteki GoPiGo - wykrywanie przeszkód.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import random
import time

import gopigo
from modules import dexter

MIN_DISTANCE = 350  # mm
FORWARD_SPEED = 150
BACKWARD_SPEED = 150


def autonomy():
    no_problem = True
    while no_problem:
        gpg.set_servo(90)
        time.sleep(1.)
        if ds is not None:
            distance = ds.read_range_single()
        else:
            distance = gpg.get_grove_value(gpg.GROVE_A1)
        if distance > MIN_DISTANCE:
            print('Forward is clear', distance)
            gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, FORWARD_SPEED)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
            time.sleep(.5)
        else:
            print('Obstacle detected', distance)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)
            gpg.set_servo(45)
            time.sleep(1.)
            if ds is not None:
                right_dir = ds.read_range_single()
            else:
                right_dir = gpg.get_grove_value(gpg.GROVE_A1)
            time.sleep(1.)
            gpg.set_servo(145)
            time.sleep(1.)
            if ds is not None:
                left_dir = ds.read_range_single()
            else:
                left_dir = gpg.get_grove_value(gpg.GROVE_A1)
            time.sleep(1.)
            if left_dir > right_dir and left_dir > MIN_DISTANCE:
                print('Choose left!')
                gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT)
                time.sleep(1.)
            elif left_dir < right_dir and right_dir > MIN_DISTANCE:
                print('Choose right!')
                gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT)
                time.sleep(1.)
            else:
                print('No clear path, REVERSE!')
                gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, BACKWARD_SPEED)
                gpg.set_motor_command(gpg.MOTOR_COMMAND.BACKWARD)
                time.sleep(2.)
                rotation = random.randrange(0, 2)
                if rotation == 0:
                    gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT_ROTATE)
                else:
                    gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT_ROTATE)
                time.sleep(1.)
            gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)


if __name__ == '__main__':
    gpg = gopigo.GoPiGo()

    try:
        ds = dexter.Distance()
    except IOError:
        gpg.set_grove_type(gpg.GROVE_A1, gpg.GROVE_TYPE.US)

    gpg.set_servo(90)
    time.sleep(1.)
    try:
        autonomy()
    except KeyboardInterrupt:
        gpg.reset()
