# -*- coding: utf-8 -*-
#
# Test biblioteki odbiornika podczerwieni.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo
import ir_receiver

gpg = gopigo.GoPiGo()
gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, 150)

while True:
    try:
        key = ir_receiver.nextcode()
        if len(key) != 0:
            print(key)
            if key == 'KEY_UP':
                gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
            elif key == 'KEY_LEFT':
                gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT)
            elif key == 'KEY_RIGHT':
                gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT)
            elif key == 'KEY_DOWN':
                gpg.set_motor_command(gpg.MOTOR_COMMAND.BACKWARD)
            elif key == 'KEY_OK':
                gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)
        time.sleep(.1)
    except KeyboardInterrupt:
        gpg.reset()
        break
