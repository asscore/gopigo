# -*- coding: utf-8 -*-
#
# Implementacja modelu mózgu robaka 'C. elegans'. 
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import copy
import json
import signal
import sys
import time

import gopigo
from modules import dexter

"""
 Algorytm:
 - Jeśli nie wykryto obiektu, stymuluj neurony wyczuwające żywność
 - Jeśli wykryto obiekt, stymuluj neurony czuciowe dotyku nosa

 Stymulacja polega na dodaniu do każdego neuronu z grupy odpowiedniej wagi (metoda dendrite_accumulate).
 Następnie uruchamiana jest analiza połączeń (metoda run_connectome), w celu wykrycia wagi przekraczającej 
 zdefiniowany próg (stała THRESHOLD). W takim przypadku następuje aktywacja neuronu (metoda fire_neuron)
 i dodawane są dodatkowe wagi w całej grupie (neurony, mięśnie). Podczas analizy pobierana jest również
 suma wag dla mięśni w celu określenie prędkości i kierunku jazdy (metoda motor_control).
 Za każdym razem, gdy aktywowany jest neuron lub mięsień, wagi są ustawiane na zero, aby akumulacja mogła
 się rozpocząć ponownie.
"""

THRESHOLD = 30

post_synaptic = {}

this_state = 0
next_state = 1

accum_left = 0
accum_right = 0

muscles = ['MVU', 'MVL', 'MDL', 'MVR', 'MDR']

muscle_left = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17',
               'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11',
               'MVL12', 'MVL13', 'MVL14', 'MVL15', 'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22',
               'MVL23']

muscle_right = ['MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16', 'MDR17',
                'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09', 'MVR10', 'MVR11',
                'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21', 'MVR22',
                'MVR23']

muscle_list = muscle_left + muscle_right

connectome = None

gpg = None
ds = None


def create_post_synaptic():
    global connectome

    with open('connectome.json') as connectome_file:
        connectome = json.load(connectome_file)

        for neuron in connectome:
            post_synaptic[neuron] = [0, 0]


def dendrite_accumulate(dneuron):
    for neuron in connectome[dneuron]:
        post_synaptic[neuron][next_state] += connectome[dneuron][neuron]


def fire_neuron(fneuron):
    if fneuron != 'MVULVA':
        for neuron in connectome[fneuron]:
            post_synaptic[neuron][next_state] += connectome[fneuron][neuron]
        post_synaptic[fneuron][next_state] = 0


def motor_control():
    global accum_right
    global accum_left

    for muscle in muscle_list:
        if muscle in muscle_left:
            accum_left += post_synaptic[muscle][next_state]
            post_synaptic[muscle][next_state] = 0
        elif muscle in muscle_right:
            accum_right += post_synaptic[muscle][next_state]
            post_synaptic[muscle][next_state] = 0

    new_speed = abs(accum_left) + abs(accum_right)
    if new_speed > 150:
        new_speed = 150
    elif new_speed < 75:
        new_speed = 75

    print('Left: %d Right: %d Speed: %d' % (accum_left, accum_right, new_speed))

    gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, new_speed)

    if accum_left == 0 and accum_right == 0:
        gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)
    elif accum_right <= 0 and accum_left < 0:
        gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, 150)
        turn_ratio = accum_right / accum_left
        if turn_ratio <= .6:
            gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT_ROTATE)
            time.sleep(.8)
        elif turn_ratio >= 2:
            gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT_ROTATE)
            time.sleep(.8)
        gpg.set_motor_command(gpg.MOTOR_COMMAND.BACKWARD)
        time.sleep(.5)
    elif accum_right <= 0 <= accum_left:
        gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT_ROTATE)
        time.sleep(.8)
    elif accum_right >= 0 >= accum_left:
        gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT_ROTATE)
        time.sleep(.8)
    elif accum_right >= 0 and accum_left > 0:
        turn_ratio = accum_right / accum_left
        if turn_ratio <= .6:
            gpg.set_motor_command(gpg.MOTOR_COMMAND.LEFT_ROTATE)
            time.sleep(.8)
        elif turn_ratio >= 2:
            gpg.set_motor_command(gpg.MOTOR_COMMAND.RIGHT_ROTATE)
            time.sleep(.8)
        gpg.set_motor_command(gpg.MOTOR_COMMAND.FORWARD)
        time.sleep(.5)
    else:
        gpg.set_motor_command(gpg.MOTOR_COMMAND.STOP)

    accum_left = 0
    accum_right = 0
    time.sleep(.5)


def run_connectome():
    global this_state
    global next_state

    for ps in post_synaptic:
        if ps[:3] not in muscles and abs(post_synaptic[ps][this_state]) > THRESHOLD:
            fire_neuron(ps)

    motor_control()

    for ps in post_synaptic:
        post_synaptic[ps][this_state] = copy.deepcopy(post_synaptic[ps][next_state])

    this_state, next_state = next_state, this_state


def main():
    food = 0

    while True:
        try:
            distance = ds.read_range_continuous() // 10
        except IOError:
            distance = 27

        if 0 < distance < 30:  # Neurony czuciowe dotyku nosa
            print('OBSTACLE (Nose Touch) %d' % distance)
            dendrite_accumulate('FLPR')
            dendrite_accumulate('FLPL')
            dendrite_accumulate('ASHL')
            dendrite_accumulate('ASHR')
            dendrite_accumulate('IL1VL')
            dendrite_accumulate('IL1VR')
            dendrite_accumulate('OLQDL')
            dendrite_accumulate('OLQDR')
            dendrite_accumulate('OLQVR')
            dendrite_accumulate('OLQVL')
            run_connectome()
        else:
            if food < 2:  # Neurony wyczuwające żywność
                print('FOOD')
                dendrite_accumulate('ADFL')
                dendrite_accumulate('ADFR')
                dendrite_accumulate('ASGR')
                dendrite_accumulate('ASGL')
                dendrite_accumulate('ASIL')
                dendrite_accumulate('ASIR')
                dendrite_accumulate('ASJR')
                dendrite_accumulate('ASJL')
                run_connectome()
                time.sleep(.5)
            food += .5
            if food > 20:
                food = 0


def keyboard_interrupt_handler(signum, frame):
    print('Ctrl+C detected. Program stopped!')

    gpg.reset()

    for ps in post_synaptic:
        print('%s\t%d\t%d' % (ps, post_synaptic[ps][0], post_synaptic[ps][1]))

    sys.exit(0)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, keyboard_interrupt_handler)

    create_post_synaptic()

    gpg = gopigo.GoPiGo()
    gpg.set_motor_speed(gpg.MOTOR_RIGHT + gpg.MOTOR_LEFT, 120)
    gpg.set_servo(90)
    time.sleep(1.)

    ds = dexter.Distance()
    ds.start_continuous()

    main()
