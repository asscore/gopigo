# -*- coding: utf-8 -*-
#
# Biblioteka upraszczająca i zapewniająca wątkowo bezpieczną komunikację z nakładką GoPiGo/GoPiGo2
# i z podłączonymi do niej czujnikami.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo
import i2c
import line_follower
from modules import dexter

mutex = i2c.Mutex()


def mutex_acquire(mutex_enabled=False):
    if mutex_enabled:
        mutex.acquire()


def mutex_release(mutex_enabled=False):
    if mutex_enabled:
        mutex.release()


class EasyGoPiGo(gopigo.GoPiGo):
    speed = None

    def __init__(self, use_mutex=False):
        self.use_mutex = use_mutex

        mutex_acquire(self.use_mutex)
        try:
            super(self.__class__, self).__init__()
        except IOError as e:
            raise e
        except gopigo.FirmwareVersionError as e:
            raise e
        except Exception as e:
            raise e
        finally:
            mutex_release(self.use_mutex)

        self.reset_speed()

    def voltage(self):
        """ Poziom naładowania baterii w V """
        mutex_acquire(self.use_mutex)
        voltage = self.get_voltage_battery()
        mutex_release(self.use_mutex)

        return voltage

    def set_speed(self, speed):
        """ Prędkość (0 - 255) """
        self.speed = speed

        mutex_acquire(self.use_mutex)
        self.set_motor_speed(self.MOTOR_RIGHT + self.MOTOR_LEFT, speed)
        mutex_release(self.use_mutex)

    def get_speed(self):
        """ Bieżąca prędkość """
        return self.speed

    def reset_speed(self):
        """ Prędkość domyślna (200) """
        self.set_speed(self.DEFAULT_SPEED)

    def stop(self):
        """ Zatrzymanie """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.STOP)
        mutex_release(self.use_mutex)

    def forward(self):
        """ Jazda do przodu """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.FORWARD)
        mutex_release(self.use_mutex)

    def backward(self):
        """ Jazda do tyłu """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.BACKWARD)
        mutex_release(self.use_mutex)

    def right(self):
        """ Obrót względem prawego koła """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.RIGHT)
        mutex_release(self.use_mutex)

    def left(self):
        """ Obrót względem lewego koła """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.LEFT)
        mutex_release(self.use_mutex)

    def right_rotate(self):
        """ Obrót w miejscu w prawo """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.RIGHT_ROTATE)
        mutex_release(self.use_mutex)

    def left_rotate(self):
        """ Obrót w miejscu w lewo """
        mutex_acquire(self.use_mutex)
        self.set_motor_command(self.MOTOR_COMMAND.LEFT_ROTATE)
        mutex_release(self.use_mutex)

    def drive_cm(self, distance, wait=False):
        """ Jazda do przodu/tyłu

        distance: dystans w cm (wartość dodatnia - do przodu, ujemna - do tyłu)
        wait: czekać na osiągnięcie celu
        """
        pulse = int(self.ENCODER_PULSE_PER_ROTATION * (abs(distance) / self.WHEEL_CIRCUMFERENCE))
        if pulse == 0:  # Do 1 cm, powyżej ok
            return

        mutex_acquire(self.use_mutex)
        self.set_motor_encoder(self.MOTOR_RIGHT + self.MOTOR_LEFT, pulse)
        if distance > 0:
            self.set_motor_command(self.MOTOR_COMMAND.FORWARD)
        elif distance < 0:
            self.set_motor_command(self.MOTOR_COMMAND.BACKWARD)
        mutex_release(self.use_mutex)

        if wait and distance != 0:
            right_motor_target = pulse
            left_motor_target = pulse
            while self.target_reached(right_motor_target, left_motor_target) is False:
                time.sleep(.1)

    def turn_degrees(self, degrees, wait=False):
        """ Obrót względem prawego/lewego koła

        degrees: liczba stopni (wartość dodatnia - względem prawego koła, ujemna - lewego)
        wait: czekać na osiągnięcie celu
        """
        pulse = int(abs(degrees) / self.DEGREES_PER_ENCODER_PULSE)
        if pulse == 0:  # Do 5 stopni, powyżej ok
            return

        mutex_acquire(self.use_mutex)
        if degrees > 0:
            self.set_motor_encoder(self.MOTOR_LEFT, pulse)
            self.set_motor_command(self.MOTOR_COMMAND.RIGHT)
        elif degrees < 0:
            self.set_motor_encoder(self.MOTOR_RIGHT, pulse)
            self.set_motor_command(self.MOTOR_COMMAND.LEFT)
        mutex_release(self.use_mutex)

        if wait and degrees != 0:
            right_motor_target = pulse if degrees < 0 else None
            left_motor_target = pulse if degrees > 0 else None
            while self.target_reached(right_motor_target, left_motor_target) is False:
                time.sleep(.1)

    def target_reached(self, right_motor_target, left_motor_target):
        """ Sprawdzenie, czy cel osiągnięty """
        result = False

        mutex_acquire(self.use_mutex)
        if right_motor_target is not None and left_motor_target is not None:
            e1, e2 = self.get_motor_encoder(self.MOTOR_RIGHT + self.MOTOR_LEFT)
            result = (e1 >= right_motor_target or e2 >= left_motor_target)
        elif right_motor_target is not None and left_motor_target is None:
            e1 = self.get_motor_encoder(self.MOTOR_RIGHT)
            result = (e1 >= right_motor_target)
        elif right_motor_target is None and left_motor_target is not None:
            e2 = self.get_motor_encoder(self.MOTOR_LEFT)
            result = (e2 >= left_motor_target)
        mutex_release(self.use_mutex)

        return result

    def led_on(self, led):
        """ Zapalenie diody LED

        led: LED_RIGHT i/lub LED_LEFT
        """
        mutex_acquire(self.use_mutex)
        self.set_led(led, self.ON)
        mutex_release(self.use_mutex)

    def led_off(self, led):
        """ Zgaszenie diody LED

        led: LED_RIGHT i/lub LED_LEFT
        """
        mutex_acquire(self.use_mutex)
        self.set_led(led, self.OFF)
        mutex_release(self.use_mutex)

    def init_servo(self):
        """ Serwomechanizm """
        return Servo(self, self.use_mutex)

    def init_ultrasonic_sensor(self, port=gopigo.GoPiGo.GROVE_A1):
        """ Ultradźwiękowy czujnik odległości """
        return UltrasonicSensor(self, port, self.use_mutex)

    def init_distance_sensor(self):
        """ Czujnik odległości """
        return DistanceSensor(self.use_mutex)

    def init_line_follower(self):
        """ Czujnik linii """
        return LineFollower(self.use_mutex)


class Servo(object):
    """ Serwomechanizm """

    def __init__(self, gpg, use_mutex=False):
        self.gpg = gpg
        self.use_mutex = use_mutex

    def rotate(self, position):
        """ Pozycja w stopniach (0 - 180) """
        if position > 180:
            position = 180
        if position < 0:
            position = 0

        mutex_acquire(self.use_mutex)
        self.gpg.set_servo(position)
        mutex_release(self.use_mutex)

    def reset(self):
        """ Środkowa pozycja (90 stopni) """
        self.rotate(90)


class UltrasonicSensor(object):
    """ Ultradźwiękowy czujnik odległości """

    def __init__(self, gpg, port, use_mutex=False):
        self.gpg = gpg
        self.port = port
        self.use_mutex = use_mutex

        mutex_acquire(self.use_mutex)
        self.gpg.set_grove_type(self.port, self.gpg.GROVE_TYPE.US)
        mutex_release(self.use_mutex)

    def read_mm(self):
        """ Odległość w mm """
        return_reading = 0
        readings = []
        value = 0
        skip = 0
        # 3 próby, aby uzyskać odczyt do 3 m
        while len(readings) < 3:
            mutex_acquire(self.use_mutex)
            try:
                value = self.gpg.get_grove_value(self.port)
            except Exception:
                pass
            finally:
                mutex_release(self.use_mutex)

            if 3000 >= value >= 0:
                readings.append(value)
            else:
                skip += 1
                if skip > 5:
                    break

        if skip > 5:
            return 5010  # Brak przeszkody

        for reading in readings:
            return_reading += reading

        return return_reading // len(readings)

    def read_cm(self):
        """ Odległość w cm """
        return self.read_mm() // 10


class DistanceSensor(dexter.Distance):
    """ Czujnik odległości """

    def __init__(self, use_mutex=False):
        self.use_mutex = use_mutex

        mutex_acquire(self.use_mutex)
        try:
            super(self.__class__, self).__init__()
        except Exception as e:
            raise e
        finally:
            mutex_release(self.use_mutex)

    def read_mm(self):
        """ Odległość w mm """
        mm = 8190
        attempt = 0
        # 3 próby, aby uzyskać odczyt pomiędzy 5 mm a 8 m
        while (mm > 8000 or mm < 5) and attempt < 3:
            mutex_acquire(self.use_mutex)
            try:
                mm = self.read_range_single()
            except Exception:
                mm = 0
            finally:
                mutex_release(self.use_mutex)
            attempt = attempt + 1
            time.sleep(.001)

        # Zakres odległości od 5 do 3000 mm
        if mm > 3000:
            mm = 3000  # Wartość poza zakresem 

        return mm

    def read_cm(self):
        """ Odległość w cm """
        return self.read_mm() // 10


class LineFollower(line_follower.LineFollower):
    """ Czujnik linii """

    def __init__(self, use_mutex=False):
        self.use_mutex = use_mutex

        mutex_acquire(self.use_mutex)
        try:
            super(self.__class__, self).__init__()
        except Exception as e:
            raise e
        finally:
            mutex_release(self.use_mutex)

    def read_state(self):
        """ Stan czujników ('b' - czarna linia, 'w' - biała linia) """
        mutex_acquire(self.use_mutex)
        five_vals = self.read()
        mutex_release(self.use_mutex)

        out_str = ''.join(['b' if sensor_val == 1 else 'w' for sensor_val in five_vals])
        return out_str

    def read_position(self):
        """ Położenie czarnej linii """
        mutex_acquire(self.use_mutex)
        result = super(self.__class__, self).read_position()
        mutex_release(self.use_mutex)

        return result
