# -*- coding: utf-8 -*-
#
# Biblioteka do komunikacji z nakładką GoPiGo/GoPiGo2.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math
import time

import i2c

I2C_ADDR = 0x08  # Adres I2C nakładki

REGISTER = 0x01

FIRMWARE_VERSION_REQUIRED = '1.0.x'


class Enumeration(object):
    def __init__(self, names):
        number = 0
        for line, name in enumerate(names.split('\n')):
            if name.find(',') >= 0:
                while name.find(' ') != -1:
                    name = name[:name.find(' ')] + name[(name.find(' ') + 1):]

                while name.find(',') != -1:
                    name = name[:name.find(',')] + name[(name.find(',') + 1):]

                if name.find('=') != -1:
                    number = int(float(name[(name.find('=') + 1):]))
                    name = name[:name.find('=')]

                setattr(self, name, number)
                number = number + 1


class FirmwareVersionError(Exception):
    """ Niezgodność wersji oprogramowania """


class GoPiGo(object):
    WHEEL_DIAMETER = 6.5  # Średnica koła w cm
    WHEEL_CIRCUMFERENCE = math.pi * WHEEL_DIAMETER  # Obwód koła w cm
    ENCODER_PULSE_PER_ROTATION = 18  # Liczba impulsów na obrót
    DEGREES_PER_ENCODER_PULSE = 360. / 64  # Liczba stopni na impuls (obrót o 360 stopni to około 64 impulsy)

    I2C_MESSAGE_TYPE = Enumeration('''
        NONE,
        GET_NAME,
        GET_FIRMWARE_VERSION,
        GET_VOLTAGE_VCC,
        GET_CPU_SPEED,
        SET_LED,
        SET_SERVO,
        SET_GROVE_TYPE,
        SET_GROVE_MODE,
        SET_GROVE_STATE,
        SET_GROVE_PWM_DUTY,
        GET_GROVE_STATE,
        GET_GROVE_VALUE,
        SET_MOTOR_COMMAND,
        SET_MOTOR_SPEED,
        GET_MOTOR_SPEED,
        SET_MOTOR_CONTROL,
        SET_MOTOR_ENCODER,
        GET_MOTOR_ENCODER,
        RESET,
    ''')

    GROVE_TYPE = Enumeration('''
        CUSTOM = 1,
        US,
    ''')

    GROVE_MODE = Enumeration('''
        INPUT,
        OUTPUT,
        INPUT_PULLUP,
    ''')

    MOTOR_COMMAND = Enumeration('''
        FORWARD = 1,
        BACKWARD,
        RIGHT,
        LEFT,
        RIGHT_ROTATE,
        LEFT_ROTATE,
        STOP,
    ''')

    LED_RIGHT = 0x01
    LED_LEFT = 0x02

    ON = 1
    OFF = 0

    GROVE_A1 = 15
    GROVE_D11 = 10

    LOW = 0
    HIGH = 1

    DEFAULT_SPEED = 200

    BACKWARD = 0
    FORWARD = 1

    MOTOR_1 = 0x01
    MOTOR_2 = 0x02
    MOTOR_RIGHT = MOTOR_1
    MOTOR_LEFT = MOTOR_2

    def __init__(self, detect=True):
        self.i2c_bus = i2c.Bus(I2C_ADDR)
        if detect:
            board = self.get_board()
            vfw = self.get_version_firmware()
            if board not in ['GoPiGo', 'GoPiGo2']:
                raise IOError('GoPiGo/GoPiGo2 with address 0x%02x not connected.' % I2C_ADDR)
            if vfw.split('.')[0] != FIRMWARE_VERSION_REQUIRED.split('.')[0] or \
                    vfw.split('.')[1] != FIRMWARE_VERSION_REQUIRED.split('.')[1]:
                raise FirmwareVersionError('%s firmware needs to be version %s but is currently version %s.' % (
                    board, FIRMWARE_VERSION_REQUIRED, vfw))

    def i2c_write_block(self, block):
        try:
            op = self.i2c_bus.write_reg_list(REGISTER, block)
            time.sleep(.005)
            return op
        except IOError:
            return -1

    def i2c_read_byte(self):
        try:
            return self.i2c_bus.read_8u()
        except IOError:
            return -1

    def get_board(self):
        """ Nazwa nakładki """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_NAME, 0, 0, 0])
        time.sleep(.08)
        board = ''
        ignore = False
        for _ in range(10):
            result = self.i2c_read_byte()
            if result != 0 and not ignore:
                board += chr(result)
            else:
                ignore = True
        return board

    def get_version_firmware(self):
        """ Wersja oprogramowania """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_FIRMWARE_VERSION, 0, 0, 0])
        time.sleep(.08)
        number = []
        for _ in range(3):
            result = self.i2c_read_byte()
            number.append(result)
        return '%s.%s.%s' % (number[0], number[1], number[2])

    def get_voltage_battery(self):
        """ Poziom naładowania baterii w V """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_VOLTAGE_VCC, 0, 0, 0])
        time.sleep(.08)
        b1 = self.i2c_read_byte()
        b2 = self.i2c_read_byte()
        if b1 != -1 and b2 != -1:
            v = b1 * 256 + b2
            v = (5 * v / 1024) / .4
            return round(v, 2)
        else:
            return -1

    def get_cpu_speed(self):
        """ Częstotliwość zegara procesora nakładki w MHz """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_CPU_SPEED, 0, 0, 0])
        time.sleep(.08)
        return self.i2c_read_byte()

    def set_led(self, led, state):
        """ Stan diód LED

        led: LED_RIGHT i/lub LED_LEFT
        state: OFF/ON lub LOW/HIGH
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_LED, led, state, 0])

    def set_servo(self, position):
        """ Pozycja serwomechanizmu w stopniach (0 - 180) """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_SERVO, position, 0, 0])

    def set_grove_type(self, pin, type):
        """ Sposób obsługi portu Grove (tylko A1)

        pin: GROVE_A1
        type: GROVE_TYPE.xxx
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_GROVE_TYPE, pin, type, 0])

    def set_grove_mode(self, pin, mode):
        """ Tryb pracy portu Grove

        pin: GROVE_A1/GROVE_D11
        mode: GROVE_MODE.xxx
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_GROVE_MODE, pin, mode, 0])

    def set_grove_state(self, pin, state):
        """ Stan portu Grove

        pin: GROVE_A1/GROVE_D11
        state: LOW/HIGH lub OFF/ON
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_GROVE_STATE, pin, state, 0])

    def set_grove_pwm_duty(self, pin, duty):
        """ Wypełnienia sygnału PWM (0 - 255)

        pin: GROVE_A1/GROVE_D11
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_GROVE_PWM_DUTY, pin, duty, 0])

    def get_grove_state(self, pin):
        """ Stan portu Grove

        pin: GROVE_A1/GROVE_D11
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_GROVE_STATE, pin, 0, 0])
        time.sleep(.08)
        b1 = self.i2c_read_byte()
        return b1

    def get_grove_value(self, pin):
        """ Wartość na porcie Grove (odczyt w zależności od ustawionego sposobu obsługi)

        pin: GROVE_A1
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_GROVE_VALUE, pin, 0, 0])
        time.sleep(.08)
        b1 = self.i2c_read_byte()
        b2 = self.i2c_read_byte()
        if b1 != -1 and b2 != -1:
            return b1 * 256 + b2
        else:
            return -1

    def set_motor_command(self, command):
        """ Wykonanie komendy

        command: MOTOR_COMMAND.xxx
        """
        return self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_MOTOR_COMMAND, command, 0, 0])

    def set_motor_speed(self, motor, speed):
        """ Prędkość silnika

        motor: MOTOR_RIGHT i/lub MOTOR_LEFT
        speed: 0 - 255
        """
        speed = max(0, min(255, speed))
        return self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_MOTOR_SPEED, motor, speed, 0])

    def get_motor_speed(self, motor):
        """ Bieżąca prędkość silnika

        motor: MOTOR_RIGHT i/lub MOTOR_LEFT
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_MOTOR_SPEED, motor, 0, 0])
        time.sleep(.08)
        result = []
        if motor & self.MOTOR_1:
            s1 = self.i2c_read_byte()
            result.append(s1)
        if motor & self.MOTOR_2:
            s2 = self.i2c_read_byte()
            result.append(s2)
        if len(result) == 1:
            return result[0]
        return tuple(result)

    def set_motor_control(self, motor, direction, speed):
        """ Kierunek i prędkość silnika

        motor: MOTOR_RIGHT i/lub MOTOR_LEFT
        direction: BACKWARD/FORWARD
        speed: 0 - 255
        """
        speed = max(0, min(255, speed))
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_MOTOR_CONTROL, motor, direction, speed])

    def set_motor_encoder(self, motor, target):
        """ Liczba impulsów do osiągnięcia przez enkoder

        motor: MOTOR_RIGHT i/lub MOTOR_LEFT
        target: liczba impulsów (18 na obrót)

        Liczba impulsów dla dystansu w cm:
          ENCODER_PULSE_PER_ROTATION * (dystans / WHEEL_CIRCUMFERENCE)
        Liczba impulsów dla obrotu w stopniach:
          obrót / DEGREES_PER_ENCODER_PULSE
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.SET_MOTOR_ENCODER, motor, target // 256, target % 256])

    def get_motor_encoder(self, motor):
        """ Bieżąca liczba impulsów enkodera

        motor: MOTOR_RIGHT i/lub MOTOR_LEFT
        """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.GET_MOTOR_ENCODER, motor, 0, 0])
        time.sleep(.08)
        result = []
        if motor & self.MOTOR_1:
            b1 = self.i2c_read_byte()
            b2 = self.i2c_read_byte()
            if b1 != -1 and b2 != -1:
                v = b1 * 256 + b2
            else:
                v = -1
            result.append(v)
        if motor & self.MOTOR_2:
            b1 = self.i2c_read_byte()
            b2 = self.i2c_read_byte()
            if b1 != -1 and b2 != -1:
                v = b1 * 256 + b2
            else:
                v = -1
            result.append(v)
        if len(result) == 1:
            return result[0]
        return tuple(result)

    def reset(self):
        """ Przywrócenie stanu początkowego """
        self.i2c_write_block([self.I2C_MESSAGE_TYPE.RESET, 0, 0, 0])
