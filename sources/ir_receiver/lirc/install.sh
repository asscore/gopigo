#!/bin/bash

echo " "
echo "Installing Dependencies"
echo "======================="
sudo apt-get update -y
sudo apt-get install lirc python-lirc -y

echo " "
echo "Copying Config Files"
echo "===================="
sudo cp hardware.conf /etc/lirc/hardware.conf
sudo cp lircd_keyes.conf /etc/lirc/lircd.conf
sudo cp lircrc_keyes /etc/lirc/lircrc
echo "Files copied"

echo " "
echo "Enabling LIRC"
echo "======================="

if grep -q "lirc_dev" /etc/modules; then
    echo "lirc_dev already present"
else
    sudo echo "lirc_dev" >> /etc/modules
    echo "lirc_dev added"
fi

if grep -q "lirc_rpi gpio_in_pin=15" /etc/modules; then
    echo "lirc_rpi already present"
else
    sudo echo "lirc_rpi gpio_in_pin=15" >> /etc/modules
    echo "lirc_rpi added"
fi

if grep -q "dtoverlay=lirc-rpi,gpio_in_pin=15" /boot/config.txt; then
    echo "lirc_rpi for kernel already present"
else
    sudo echo "dtoverlay=lirc-rpi,gpio_in_pin=15" >> /boot/config.txt
    echo "lirc_rpi for kernel added"
fi

echo " "
echo "Please restart the Raspberry Pi for the changes to take effect"
