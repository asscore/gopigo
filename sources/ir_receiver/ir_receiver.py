# -*- coding: utf-8 -*-
#
# Biblioteka odbierająca z portu 21852 informacje o wciśnięciu klawisza.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import socket
import sys
import threading

NO_PRESS = 'NO_KEYPRESS'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
s.bind(('localhost', 21852))
s.listen(1)
s.settimeout(.5)

last_recv_or_code = NO_PRESS
previous_keypress = NO_PRESS


def run_server():
    global last_recv_or_code

    while True:
        try:
            connection, client_address = s.accept()
            connection.setblocking(True)
        except socket.timeout:
            last_recv_or_code = NO_PRESS
            continue
        last_recv_or_code = connection.recv(16)
        connection.close()
    sys.exit(0)


th = threading.Thread(target=run_server)
th.daemon = True
th.start()


def nextcode(consume=True):
    """ Kod wciśniętego klawisza """
    global last_recv_or_code
    global previous_keypress

    send_back = last_recv_or_code

    if consume:
        if previous_keypress == send_back:
            return ''
        if send_back == NO_PRESS:
            previous_keypress = send_back
            return ''

    previous_keypress = send_back

    return send_back
