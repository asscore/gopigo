# -*- coding: utf-8 -*-
#
# Biblioteka do sterowania ramieniem MeArm.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time
from math import pi

import kinematics
import pivotpi


def setup_servo(n_min, n_max, a_min, a_max):
    rec = {}
    n_range = n_max - n_min
    a_range = a_max - a_min
    if a_range == 0:
        return
    gain = n_range / a_range
    zero = n_min - gain * a_min
    rec['gain'] = gain
    rec['zero'] = zero
    rec['min'] = n_min
    rec['max'] = n_max
    return rec


class MeArm(object):
    def __init__(self, address=0x40, sweep_min_base=135, sweep_max_base=45, angle_min_base=-pi / 4,
                 angle_max_base=pi / 4, sweep_min_shoulder=110, sweep_max_shoulder=28, angle_min_shoulder=pi / 4,
                 angle_max_shoulder=3 * pi / 4, sweep_min_elbow=85, sweep_max_elbow=36, angle_min_elbow=0,
                 angle_max_elbow=-pi / 4, sweep_min_gripper=60, sweep_max_gripper=120, angle_min_gripper=pi / 2,
                 angle_max_gripper=0):
        self.pivotpi = pivotpi.PivotPi(address)
        self.servo_info = {}
        self.servo_info['base'] = setup_servo(sweep_min_base, sweep_max_base, angle_min_base, angle_max_base)
        self.servo_info['shoulder'] = setup_servo(sweep_min_shoulder, sweep_max_shoulder, angle_min_shoulder,
                                                       angle_max_shoulder)
        self.servo_info['elbow'] = setup_servo(sweep_min_elbow, sweep_max_elbow, angle_min_elbow, angle_max_elbow)
        self.servo_info['gripper'] = setup_servo(sweep_min_gripper, sweep_max_gripper, angle_min_gripper,
                                                      angle_max_gripper)
        self.open_gripper()
        self.go_directly_to(0, 100, 50)

    def angle_to_pwm(self, servo, angle):
        """ Wartość PWM """
        ret = 150 + int(.5 + (self.servo_info[servo]['zero'] + self.servo_info[servo]['gain'] * angle) * 450 / 180)
        return 4095 - ret

    def go_directly_to(self, x, y, z):
        """ Pozycja chwytaka (ruch bezpośredni do pozycji docelowej) """
        angles = [0, 0, 0]
        if kinematics.solve(x, y, z, angles):
            self.pivotpi.pca9685.set_pwm(self.pivotpi.SERVO_1, 0, self.angle_to_pwm('base', angles[0]))
            self.pivotpi.pca9685.set_pwm(self.pivotpi.SERVO_2, 0, self.angle_to_pwm('shoulder', angles[1]))
            self.pivotpi.pca9685.set_pwm(self.pivotpi.SERVO_3, 0, self.angle_to_pwm('elbow', angles[2]))
            self.x = x
            self.y = y
            self.z = z

    def go_to(self, x, y, z):
        """ Pozycja chwytaka (ruch po linii prostej od aktualnej pozycji do docelowej) """
        x0 = self.x
        y0 = self.y
        z0 = self.z
        dist = kinematics.distance(x0, y0, z0, x, y, z)
        step = 10
        i = 0
        while i < dist:
            self.go_directly_to(x0 + (x - x0) * i / dist, y0 + (y - y0) * i / dist, z0 + (z - z0) * i / dist)
            time.sleep(.05)
            i += step
        self.go_directly_to(x, y, z)
        time.sleep(.05)

    def open_gripper(self):
        """ Chwytak otwarty """
        self.pivotpi.pca9685.set_pwm(self.pivotpi.SERVO_4, 0, self.angle_to_pwm('gripper', pi / 2.))
        time.sleep(.3)

    def close_gripper(self):
        """ Chwytak zamknięty """
        self.pivotpi.pca9685.set_pwm(self.pivotpi.SERVO_4, 0, self.angle_to_pwm('gripper', 0))
        time.sleep(.3)

    def is_reachable(self, x, y, z):
        """ Sprawdzenie, czy punkt jest osiągalny przez chwytak """
        angles = [0, 0, 0]
        return kinematics.solve(x, y, z, angles)

    def get_position(self):
        """ Aktualna pozycja chwytaka """
        return [self.x, self.y, self.z]
