# -*- coding: utf-8 -*-
#
# Wykorzystanie wzorów kinematyki prostej/odwrotnej do sterowania ramieniem MeArm.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math

L1 = 80
L2 = 80
L3 = 68


def cart_to_polar(x, y):
    r = math.hypot(x, y)
    if r == 0:
        return

    c = x / r
    s = y / r

    if s > 1:
        s = 1
    if c > 1:
        c = 1
    if s < -1:
        s = -1
    if c < -1:
        c = -1

    theta = math.acos(c)

    if s < 0:
        theta = -theta

    return r, theta


def cos_angle(opp, adj1, adj2, theta):
    den = 2 * adj1 * adj2
    if den == 0:
        return False

    c = (adj1 * adj1 + adj2 * adj2 - opp * opp) / den
    if c > 1 or c < -1:
        return False

    theta[0] = math.acos(c)
    return True


def solve(x, y, z, angles):
    r0, th0 = cart_to_polar(y, x)
    r0 -= L3

    r, ang_p = cart_to_polar(r0, z)

    parm_b = [0]
    parm_c = [0]

    if not cos_angle(L2, L1, r, parm_b):
        return False
    if not cos_angle(r, L1, L2, parm_c):
        return False

    b = parm_b[0]
    c = parm_c[0]

    a0 = th0
    a1 = ang_p + b
    a2 = c + a1 - math.pi

    angles[0] = a0
    angles[1] = a1
    angles[2] = a2

    return True


def polar_to_cart(r, theta):
    a = r * math.cos(theta)
    b = r * math.sin(theta)
    return a, b


def distance(x1, y1, z1, x2, y2, z2):
    dx = x2 - x1
    dy = y2 - y1
    dz = z2 - z1
    return math.sqrt(dx * dx + dy * dy + dz * dz)


def unsolve(a0, a1, a2):
    u01, v01 = polar_to_cart(L1, a1)
    u12, v12 = polar_to_cart(L2, a2)

    u = u01 + u12 + L3
    v = v01 + v12

    y, x = polar_to_cart(u, a0)
    z = v
    return x, y, z
