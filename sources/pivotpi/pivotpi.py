# -*- coding: utf-8 -*-
#
# Biblioteka do komunikacji z modułem PivotPi.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import pca9685


# Translacja wartości z zakresu min1 - max1 na wartość z min2 - max2
def translate(value, min1, max1, min2, max2):
    left_span = max1 - min1
    right_span = max2 - min2
    value_scaled = float(value - min1) / float(left_span)
    return int(min2 + (value_scaled * right_span))


class PivotPi(object):
    SERVO_MIN = 150  # Minimalna długość impulsu (z 4096)
    SERVO_MAX = 600  # Maksymalna długość impulsu (z 4096)

    SERVO_1 = 0
    SERVO_2 = 1
    SERVO_3 = 2
    SERVO_4 = 3
    SERVO_5 = 4
    SERVO_6 = 5
    SERVO_7 = 6
    SERVO_8 = 7

    LED_1 = 8
    LED_2 = 9
    LED_3 = 10
    LED_4 = 11
    LED_5 = 12
    LED_6 = 13
    LED_7 = 14
    LED_8 = 15

    def __init__(self, address=0x40, frequency=60):
        self.pca9685 = pca9685.PCA9685(address)
        self.frequency = frequency
        self.pca9685.set_pwm_freq(frequency)

    def set_angle(self, channel, angle):
        """ Pozycja serwomechanizmu w stopniach (0 - 180)

        channel: SERVO_x
        """
        pwm_to_send = 4095 - translate(angle, 0, 180, self.SERVO_MIN, self.SERVO_MAX)
        self.pca9685.set_pwm(channel, 0, int(pwm_to_send))

    def set_angle_microseconds(self, channel, time):
        """ Pozycja serwomechanizmu w us

        channel: SERVO_x
        """
        if time <= 0:
            self.pca9685.set_pwm(channel, 4096, 4095)
        else:
            pwm_to_send = 4095 - ((4096. / (1000000. / self.frequency)) * time)
            if pwm_to_send < 0:
                pwm_to_send = 0
            if pwm_to_send > 4095:
                pwm_to_send = 4095
            self.pca9685.set_pwm(channel, 0, int(pwm_to_send))

    def set_led_brightness(self, channel, percent):
        """ Jasność diody LED w %

        channel: LED_x
        """
        if percent >= 100:
            self.pca9685.set_pwm(channel, 4096, 4095)
        else:
            if percent < 0:
                percent = 0
            pwm_to_send = percent * 40.95
            self.pca9685.set_pwm(channel, 0, int(pwm_to_send))
