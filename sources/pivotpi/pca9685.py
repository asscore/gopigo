# -*- coding: utf-8 -*-
#
# PCA9685 - 16 kanałowy, 12 bitowy sterownik PWM.
#
# Copyright (c) 2017, 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math
import time

import i2c

# Rejestry
MODE1 = 0x00
MODE2 = 0x01
SUBADR1 = 0x02
SUBADR2 = 0x03
SUBADR3 = 0x04
PRESCALE = 0xFE
LED0_ON_L = 0x06
LED0_ON_H = 0x07
LED0_OFF_L = 0x08
LED0_OFF_H = 0x09
ALL_LED_ON_L = 0xFA
ALL_LED_ON_H = 0xFB
ALL_LED_OFF_L = 0xFC
ALL_LED_OFF_H = 0xFD

# Bity (MODE1)
RESTART = 0x80
SLEEP = 0x10
ALLCALL = 0x01
# Bity (MODE2)
INVRT = 0x10
OUTDRV = 0x04


class PCA9685(object):
    general_call_i2c = i2c.Bus(0x00)

    @classmethod
    def software_reset(cls):
        """ Wysłanie komendy SWRST (software reset) """
        cls.general_call_i2c.write_8(0x06)  # SWRST

    def __init__(self, address=0x40):
        self.i2c_bus = i2c.Bus(address)
        self.set_all_pwm(0, 0)
        self.i2c_bus.write_reg_8(MODE2, (OUTDRV | INVRT))
        self.i2c_bus.write_reg_8(MODE1, ALLCALL)
        time.sleep(.005)
        mode1 = self.i2c_bus.read_reg_8u(MODE1)
        mode1 = mode1 & ~SLEEP
        self.i2c_bus.write_reg_8(MODE1, mode1)
        time.sleep(.005)

    def set_pwm_freq(self, freq):
        """ Częstotliwość pracy modułu w Hz

        freq: 40 - 1000
        """
        prescale_val = 25000000.  # 25MHz
        prescale_val /= 4096.  # 12-bit
        prescale_val /= float(freq)
        prescale_val -= 1.
        prescale = int(math.floor(prescale_val + .5))
        old_mode = self.i2c_bus.read_reg_8u(MODE1)
        new_mode = (old_mode & 0x7F) | 0x10
        self.i2c_bus.write_reg_8(MODE1, new_mode)
        self.i2c_bus.write_reg_8(PRESCALE, prescale)
        self.i2c_bus.write_reg_8(MODE1, old_mode)
        time.sleep(.005)
        self.i2c_bus.write_reg_8(MODE1, old_mode | 0x80)

    def set_pwm(self, channel, on, off):
        """ Wartość PWM dla kanału (0 - 4095)

        on: początek pojawienia się impulsu (zmianę stanu z niskiego na wysoki)
        off: jego koniec (zmiana stanu z wysokiego na niski)
        """
        self.i2c_bus.write_reg_8(LED0_ON_L + 4 * channel, on & 0xFF)
        self.i2c_bus.write_reg_8(LED0_ON_H + 4 * channel, on >> 8)
        self.i2c_bus.write_reg_8(LED0_OFF_L + 4 * channel, off & 0xFF)
        self.i2c_bus.write_reg_8(LED0_OFF_H + 4 * channel, off >> 8)

    def set_all_pwm(self, on, off):
        """ Wartość PWM dla wszystkich kanałów """
        self.i2c_bus.write_reg_8(ALL_LED_ON_L, on & 0xFF)
        self.i2c_bus.write_reg_8(ALL_LED_ON_H, on >> 8)
        self.i2c_bus.write_reg_8(ALL_LED_OFF_L, off & 0xFF)
        self.i2c_bus.write_reg_8(ALL_LED_OFF_H, off >> 8)
