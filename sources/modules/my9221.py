# -*- coding: utf-8 -*-
#
# MY9221 - 12-kanałowy sterownik LED z APDM.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO

CMD_MODE = 0x0000
ON = 0x00FF
OFF = 0x0000


class MY9221(object):
    def __init__(self, di_pin, dcki_pin, reverse=False):
        self.di_pin = di_pin
        self.dcki_pin = dcki_pin
        self.reverse = reverse
        self.led_state = 0x00

        GPIO.setup(self.dcki_pin, GPIO.OUT)
        GPIO.setup(self.di_pin, GPIO.OUT)

    def _send_data(self, data):
        for i in range(16):
            state = GPIO.HIGH if data & 0x8000 else GPIO.LOW
            GPIO.output(self.di_pin, state)
            state = GPIO.LOW if GPIO.input(self.dcki_pin) else GPIO.HIGH
            GPIO.output(self.dcki_pin, state)
            data <<= 1

    def _latch_data(self):
        GPIO.output(self.di_pin, GPIO.LOW)
        time.sleep(.00001)
        for i in range(4):
            GPIO.output(self.di_pin, GPIO.HIGH)
            GPIO.output(self.di_pin, GPIO.LOW)

    def set_reverse(self, reverse):
        self.reverse = reverse
        self.set_bits(self.led_state)

    def set_level(self, level):
        self.led_state = ~(~0 << level);
        self.set_bits(self.led_state);

    def set_led(self, led, state):
        led -= 1
        self.led_state = (self.led_state | (0x01 << led)) if state else (self.led_state & ~(0x01 << led))
        self.set_bits(self.led_state)

    def toggle_led(self, led):
        led -= 1
        self.led_state ^= (0x01 << led);
        self.set_bits(self.led_state)

    def set_bits(self, bits):
        self.led_state = bits
        self._send_data(CMD_MODE)
        for i in range(12):
            if self.reverse:
                state = ON if (bits << i) & 0x200 else OFF
            else:
                state = ON if (bits >> i) & 0x01 else OFF
            self._send_data(state)
        self._latch_data()

    def get_bits(self):
        return self.led_state
