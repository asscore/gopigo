# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem koloru.
#  Dexter Industries Light & Color Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import grove
from modules import tcs34725


class LightColor(grove.LightColor):
    def __init__(self, integration_time=tcs34725.INTEGRATIONTIME_2_4MS, gain=tcs34725.GAIN_16X, led_state=False):
        super(self.__class__, self).__init__(integration_time, gain)
        self.set_led(led_state)

    def set_led(self, state):
        """ Stan diody (zapalona/zgaszona)

        state: True/False
        """
        self.tcs34725.set_interrupt(state)
        time.sleep(tcs34725.integration_time_delay[self.tcs34725.integration_time])
