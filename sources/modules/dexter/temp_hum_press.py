# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem temperatury, wilgotności i ciśnienia.
#  Dexter Industries Temperature Humidity & Pressure Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import grove


class TempHumPress(grove.Barometer):
    def __init__(self, address=0x76):
        super(self.__class__, self).__init__(address)
