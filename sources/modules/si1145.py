# -*- coding: utf-8 -*-
#
# SI1145 - czujnik światła UV.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

# Komendy
CMD_PARAM_QUERY = 0x80
CMD_PARAM_SET = 0xA0
CMD_NOP = 0x0
CMD_RESET = 0x01
CMD_BUSADDR = 0x02
CMD_PS_FORCE = 0x05
CMD_ALS_FORCE = 0x06
CMD_PSALS_FORCE = 0x07
CMD_PS_PAUSE = 0x09
CMD_ALS_PAUSE = 0x0A
CMD_PSALS_PAUSE = 0xB
CMD_PS_AUTO = 0x0D
CMD_ALS_AUTO = 0x0E
CMD_PSALS_AUTO = 0x0F
CMD_GET_CAL = 0x12

# Parametry
PARAM_I2CADDR = 0x00
PARAM_CHLIST = 0x01
PARAM_CHLIST_ENUV = 0x80
PARAM_CHLIST_ENAUX = 0x40
PARAM_CHLIST_ENALSIR = 0x20
PARAM_CHLIST_ENALSVIS = 0x10
PARAM_CHLIST_ENPS1 = 0x01
PARAM_CHLIST_ENPS2 = 0x02
PARAM_CHLIST_ENPS3 = 0x04

PARAM_PSLED12SEL = 0x02
PARAM_PSLED12SEL_PS2NONE = 0x00
PARAM_PSLED12SEL_PS2LED1 = 0x10
PARAM_PSLED12SEL_PS2LED2 = 0x20
PARAM_PSLED12SEL_PS2LED3 = 0x40
PARAM_PSLED12SEL_PS1NONE = 0x00
PARAM_PSLED12SEL_PS1LED1 = 0x01
PARAM_PSLED12SEL_PS1LED2 = 0x02
PARAM_PSLED12SEL_PS1LED3 = 0x04

PARAM_PSLED3SEL = 0x03
PARAM_PSENCODE = 0x05
PARAM_ALSENCODE = 0x06

PARAM_PS1ADCMUX = 0x07
PARAM_PS2ADCMUX = 0x08
PARAM_PS3ADCMUX = 0x09
PARAM_PSADCOUNTER = 0x0A
PARAM_PSADCGAIN = 0x0B
PARAM_PSADCMISC = 0x0C
PARAM_PSADCMISC_RANGE = 0x20
PARAM_PSADCMISC_PSMODE = 0x04

PARAM_ALSIRADCMUX = 0x0E
PARAM_AUXADCMUX = 0x0F

PARAM_ALSVISADCOUNTER = 0x10
PARAM_ALSVISADCGAIN = 0x11
PARAM_ALSVISADCMISC = 0x12
PARAM_ALSVISADCMISC_VISRANGE = 0x20

PARAM_ALSIRADCOUNTER = 0x1D
PARAM_ALSIRADCGAIN = 0x1E
PARAM_ALSIRADCMISC = 0x1F
PARAM_ALSIRADCMISC_RANGE = 0x20

PARAM_ADCCOUNTER_511CLK = 0x70

PARAM_ADCMUX_SMALLIR = 0x00
PARAM_ADCMUX_LARGEIR = 0x03

# Rejestry
REG_PARTID = 0x00
REG_REVID = 0x01
REG_SEQID = 0x02

REG_INTCFG = 0x03
REG_INTCFG_INTOE = 0x01
REG_INTCFG_INTMODE = 0x02

REG_IRQEN = 0x04
REG_IRQEN_ALSEVERYSAMPLE = 0x01
REG_IRQEN_PS1EVERYSAMPLE = 0x04
REG_IRQEN_PS2EVERYSAMPLE = 0x08
REG_IRQEN_PS3EVERYSAMPLE = 0x10

REG_IRQMODE1 = 0x05
REG_IRQMODE2 = 0x06

REG_HWKEY = 0x07
REG_MEASRATE0 = 0x08
REG_MEASRATE1 = 0x09
REG_PSRATE = 0x0A
REG_PSLED21 = 0x0F
REG_PSLED3 = 0x10
REG_UCOEFF0 = 0x13
REG_UCOEFF1 = 0x14
REG_UCOEFF2 = 0x15
REG_UCOEFF3 = 0x16
REG_PARAMWR = 0x17
REG_COMMAND = 0x18
REG_RESPONSE = 0x20
REG_IRQSTAT = 0x21
REG_IRQSTAT_ALS = 0x01

REG_ALSVISDATA0 = 0x22
REG_ALSVISDATA1 = 0x23
REG_ALSIRDATA0 = 0x24
REG_ALSIRDATA1 = 0x25
REG_PS1DATA0 = 0x26
REG_PS1DATA1 = 0x27
REG_PS2DATA0 = 0x28
REG_PS2DATA1 = 0x29
REG_PS3DATA0 = 0x2A
REG_PS3DATA1 = 0x2B
REG_UVINDEX0 = 0x2C
REG_UVINDEX1 = 0x2D
REG_PARAMRD = 0x2E
REG_CHIPSTAT = 0x30


class SI1145(object):
    def __init__(self, address=0x60):
        self.i2c_bus = i2c.Bus(address, big_endian=False)
        self._reset()
        self._load_calibration()

    def _reset(self):
        self.i2c_bus.write_reg_8(REG_MEASRATE0, 0)
        self.i2c_bus.write_reg_8(REG_MEASRATE1, 0)
        self.i2c_bus.write_reg_8(REG_IRQEN, 0)
        self.i2c_bus.write_reg_8(REG_IRQMODE1, 0)
        self.i2c_bus.write_reg_8(REG_IRQMODE2, 0)
        self.i2c_bus.write_reg_8(REG_INTCFG, 0)
        self.i2c_bus.write_reg_8(REG_IRQSTAT, 0xFF)

        self.i2c_bus.write_reg_8(REG_COMMAND, CMD_RESET)
        time.sleep(.01)
        self.i2c_bus.write_reg_8(REG_HWKEY, 0x17)
        time.sleep(.01)

    def _write_param(self, p, v):
        self.i2c_bus.write_reg_8(REG_PARAMWR, v)
        self.i2c_bus.write_reg_8(REG_COMMAND, p | CMD_PARAM_SET)
        param_val = self.i2c_bus.read_reg_8u(REG_PARAMRD)
        return param_val

    def _load_calibration(self):
        self.i2c_bus.write_reg_8(REG_UCOEFF0, 0x29)
        self.i2c_bus.write_reg_8(REG_UCOEFF1, 0x89)
        self.i2c_bus.write_reg_8(REG_UCOEFF2, 0x02)
        self.i2c_bus.write_reg_8(REG_UCOEFF3, 0x00)

        self._write_param(PARAM_CHLIST,
                          PARAM_CHLIST_ENUV | PARAM_CHLIST_ENALSIR | PARAM_CHLIST_ENALSVIS | PARAM_CHLIST_ENPS1)

        self.i2c_bus.write_reg_8(REG_INTCFG, REG_INTCFG_INTOE)
        self.i2c_bus.write_reg_8(REG_IRQEN, REG_IRQEN_ALSEVERYSAMPLE)

        self.i2c_bus.write_reg_8(REG_PSLED21, 0x03)
        self._write_param(PARAM_PS1ADCMUX, PARAM_ADCMUX_LARGEIR)

        self._write_param(PARAM_PSLED12SEL, PARAM_PSLED12SEL_PS1LED1)
        self._write_param(PARAM_PSADCGAIN, 0)
        self._write_param(PARAM_PSADCOUNTER, PARAM_ADCCOUNTER_511CLK)
        self._write_param(PARAM_PSADCMISC, PARAM_PSADCMISC_RANGE | PARAM_PSADCMISC_PSMODE)
        self._write_param(PARAM_ALSIRADCMUX, PARAM_ADCMUX_SMALLIR)
        self._write_param(PARAM_ALSIRADCGAIN, 0)
        self._write_param(PARAM_ALSIRADCOUNTER, PARAM_ADCCOUNTER_511CLK)
        self._write_param(PARAM_ALSIRADCMISC, PARAM_ALSIRADCMISC_RANGE)
        self._write_param(PARAM_ALSVISADCGAIN, 0)
        self._write_param(PARAM_ALSVISADCOUNTER, PARAM_ADCCOUNTER_511CLK)
        self._write_param(PARAM_ALSVISADCMISC, PARAM_ALSVISADCMISC_VISRANGE)

        self.i2c_bus.write_reg_8(REG_MEASRATE0, 0xFF)
        self.i2c_bus.write_reg_8(REG_COMMAND, CMD_PSALS_AUTO)

    def read_uv(self):
        return self.i2c_bus.read_reg_16u(0x2C)  # Indeks UV * 100

    def read_visible(self):
        return self.i2c_bus.read_reg_16u(0x22)

    def read_ir(self):
        return self.i2c_bus.read_reg_16u(0x24)

    def read_proximity(self):
        # Pomiar zbliżeniowy wymaga podłączenia IR LED do złącza LED
        return self.i2c_bus.read_reg_16u(0x26)
