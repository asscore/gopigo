# -*- coding: utf-8 -*-
#
# ADC121C021 - przetwornik ADC 12-bitowy I2C.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import i2c

REG_ADDR_RESULT = 0x00
REG_ADDR_ALERT  = 0x01
REG_ADDR_CONFIG = 0x02
REG_ADDR_LIMITL = 0x03
REG_ADDR_LIMITH = 0x04
REG_ADDR_HYST   = 0x05
REG_ADDR_CONVL  = 0x06
REG_ADDR_CONVH  = 0x07


class ADC121C021(object):
    def __init__(self, address=0x50):
        self.i2c_bus = i2c.Bus(address)
        self.i2c_bus.write_reg_8(REG_ADDR_CONFIG, 0x20)

    def read(self):
        return self.i2c_bus.read_reg_16u(REG_ADDR_RESULT)
