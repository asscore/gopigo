# -*- coding: utf-8 -*-
#
# ADXL345 - 3-osiowy akcelerometr cyfrowy.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import i2c

DATA_FORMAT = 0x31
BW_RATE = 0x2C
POWER_CTL = 0x2D
DATAX0 = 0x32

BW_RATE_1600HZ = 0x0F
BW_RATE_800HZ = 0x0E
BW_RATE_400HZ = 0x0D
BW_RATE_200HZ = 0x0C
BW_RATE_100HZ = 0x0B
BW_RATE_50HZ = 0x0A
BW_RATE_25HZ = 0x09

RANGE_2G = 0x00
RANGE_4G = 0x01
RANGE_8G = 0x02
RANGE_16G = 0x03

EARTH_GRAVITY_MS2 = 9.80665
SCALE_MULTIPLIER = .004


class ADXL345(object):
    def __init__(self, address=0x53):
        self.i2c_bus = i2c.Bus(address)
        self.set_bandwidth_rate(BW_RATE_100HZ)
        self.set_range(RANGE_2G)
        self.enable_measurement()

    def enable_measurement(self):
        self.i2c_bus.write_reg_8(POWER_CTL, 0x08)

    def set_bandwidth_rate(self, rate_flag):
        self.i2c_bus.write_reg_8(BW_RATE, rate_flag)

    def set_range(self, range_flag):
        value = self.i2c_bus.read_reg_8u(DATA_FORMAT)
        value &= ~0x0F
        value |= range_flag
        value |= 0x08
        self.i2c_bus.write_reg_8(DATA_FORMAT, value)

    def get_axes(self, gforce=False):
        bytes = self.i2c_bus.read_reg_list(DATAX0, 6)

        x = bytes[0] | (bytes[1] << 8)
        if x & (1 << 16 - 1):
            x = x - (1 << 16)

        y = bytes[2] | (bytes[3] << 8)
        if y & (1 << 16 - 1):
            y = y - (1 << 16)

        z = bytes[4] | (bytes[5] << 8)
        if z & (1 << 16 - 1):
            z = z - (1 << 16)

        x = x * SCALE_MULTIPLIER
        y = y * SCALE_MULTIPLIER
        z = z * SCALE_MULTIPLIER

        if gforce == False:  # Wyniki w m/s^2
            x = x * EARTH_GRAVITY_MS2
            y = y * EARTH_GRAVITY_MS2
            z = z * EARTH_GRAVITY_MS2

        x = round(x, 4)
        y = round(y, 4)
        z = round(z, 4)

        return {'x': x, 'y': y, 'z': z}
