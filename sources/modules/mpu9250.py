# -*- coding: utf-8 -*-
#
# MPU-9250 - 3-osiowy akcelerometr, żyroskop i magnetometr (kompas).
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

MPU9250_ADDR = 0x68
AK8963_ADDR = 0x0C

# MPU-9250
INT_PIN_CFG = 0x37
INT_ENABLE = 0x38
PWR_MGMT_1 = 0x6B
WHO_AM_I = 0x75

ACCEL_XOUT_H = 0x3B
ACCEL_XOUT_L = 0x3C
ACCEL_YOUT_H = 0x3D
ACCEL_YOUT_L = 0x3E
ACCEL_ZOUT_H = 0x3F
ACCEL_ZOUT_L = 0x40

TEMP_OUT_H = 0x41
TEMP_OUT_L = 0x42

GYRO_XOUT_H = 0x43
GYRO_XOUT_L = 0x44
GYRO_YOUT_H = 0x45
GYRO_YOUT_L = 0x46
GYRO_ZOUT_H = 0x47
GYRO_ZOUT_L = 0x48

ACCEL_CONFIG  = 0x1C
ACCEL_2G = 0x00
ACCEL_4G = (0x01 << 3)
ACCEL_8G = (0x02 << 3)
ACCEL_16G = (0x03 << 3)

GYRO_CONFIG = 0x1B
GYRO_250DPS = 0x00
GYRO_500DPS = (0x01 << 3)
GYRO_1000DPS = (0x02 << 3)
GYRO_2000DPS = (0x03 << 3)

# AK8963
WIA = 0x00
ST1 = 0x02
CNTL1 = 0x0A

ASAX = 0x10
ASAY = 0x11
ASAZ = 0x12

HXL = 0x03
HXH = 0x04
HYL = 0x05
HYH = 0x06
HZL = 0x07
HZH = 0x08

# CNTL1
MODE_DOWN = 0x00
MODE_ONE = 0x01
MODE_FUSE_ROM = 0x0F

BIT_16 = 0x01


class MPU9250(object):
    def __init__(self):
        self.i2c_bus = i2c.Bus(MPU9250_ADDR)

        chip_id = self.i2c_bus.read_reg_8u(WHO_AM_I)
        if chip_id != 0x71:
            raise RuntimeError('Incorrect chip ID.')

        self.i2c_bus.write_reg_8(PWR_MGMT_1, 0x00)
        time.sleep(.1)
        self.i2c_bus.write_reg_8(PWR_MGMT_1, 0x01)
        time.sleep(.1)
        self.i2c_bus.write_reg_8(ACCEL_CONFIG, ACCEL_2G)
        self.i2c_bus.write_reg_8(GYRO_CONFIG, GYRO_250DPS)
                
        self.i2c_bus.write_reg_8(INT_PIN_CFG, 0x02)
        self.i2c_bus.write_reg_8(INT_ENABLE, 0x01)
        time.sleep(.1)

        self.i2c_bus.set_address(AK8963_ADDR)
        chip_id = self.i2c_bus.read_reg_8u(WIA)
        if chip_id != 0x48:
            raise RuntimeError('Incorrect chip ID.')

        self.i2c_bus.write_reg_8(CNTL1, MODE_DOWN)
        time.sleep(.1)
        self.i2c_bus.write_reg_8(CNTL1, MODE_FUSE_ROM)
        time.sleep(.1)
        asax = self.i2c_bus.read_reg_8u(ASAX)
        asay = self.i2c_bus.read_reg_8u(ASAY)
        asaz = self.i2c_bus.read_reg_8u(ASAZ)
        self.adj_x = (.5 * (asax - 128)) / 128. + 1.
        self.adj_y = (.5 * (asay - 128)) / 128. + 1.
        self.adj_z = (.5 * (asaz - 128)) / 128. + 1.
        self.i2c_bus.write_reg_8(CNTL1, MODE_DOWN)
        time.sleep(.1)

    def _write(self, address, reg, val):
        self.i2c_bus.set_address(address)
        self.i2c_bus.write_reg_8(reg, val)

    def _read(self, address, high, low):
        self.i2c_bus.set_address(address)
        h = self.i2c_bus.read_reg_8u(high)
        l = self.i2c_bus.read_reg_8u(low)
        value = (h << 8) + l
        if value >= 0x8000:
            return -((65535 - value) + 1)
        else:
            return value

    def get_accelerometer_axes(self):
        x_out = self._read(MPU9250_ADDR, ACCEL_XOUT_H, ACCEL_XOUT_L)
        y_out = self._read(MPU9250_ADDR, ACCEL_YOUT_H, ACCEL_YOUT_L)
        z_out = self._read(MPU9250_ADDR, ACCEL_ZOUT_H, ACCEL_ZOUT_L)
        ax = 2. * x_out / 32768.  # 2g
        ay = 2. * y_out / 32768.
        az = 2. * z_out / 32768.
        return {'x': ax, 'y': ay, 'z': az}

    def get_gyroscopic_axes(self):
        x_out = self._read(MPU9250_ADDR, GYRO_XOUT_H, GYRO_XOUT_L)
        y_out = self._read(MPU9250_ADDR, GYRO_YOUT_H, GYRO_YOUT_L)
        z_out = self._read(MPU9250_ADDR, GYRO_ZOUT_H, GYRO_ZOUT_L)
        gx = 250. * x_out / 32768.  # +250dps
        gy = 250. * y_out / 32768.
        gz = 250. * z_out / 32768.
        return {'x': gx, 'y': gy, 'z': gz}

    def get_temperature(self):
        temp_out = self._read(MPU9250_ADDR, TEMP_OUT_H, TEMP_OUT_L)
        temp = temp_out / 333.87 + 21.
        return temp

    def get_magnetic_axes(self):
        self._write(AK8963_ADDR, CNTL1, (BIT_16 << 4) | MODE_ONE)  # Pojedynczy pomiar (16 bit)
        time.sleep(.1)
        x_out = self._read(AK8963_ADDR, HXH, HXL)
        y_out = self._read(AK8963_ADDR, HYH, HYL)
        z_out = self._read(AK8963_ADDR, HZH, HZL)
        mx = 4912. * x_out / 32760.
        my = 4912. * y_out / 32760.
        mz = 4912. * z_out / 32760.
        mx *= self.adj_x
        my *= self.adj_y
        mz *= self.adj_z
        return {'x': mx, 'y': my, 'z': mz}
