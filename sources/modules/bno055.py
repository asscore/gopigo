# -*- coding: utf-8 -*-
#
# BNO055 - 3-osiowy akcelerometr, żyroskop i magnetometr (kompas).
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

ADDRESS_A = 0x28
ADDRESS_B = 0x29

ID = 0xA0

REG_PAGE_ID = 0x07

REG_CHIP_ID = 0x00
REG_ACCEL_REV_ID = 0x01
REG_MAG_REV_ID = 0x02
REG_GYRO_REV_ID = 0x03
REG_SW_REV_ID_LSB = 0x04
REG_SW_REV_ID_MSB = 0x05
REG_BL_REV_ID = 0x06

REG_ACCEL_DATA_X_LSB = 0x08
REG_ACCEL_DATA_X_MSB = 0x09
REG_ACCEL_DATA_Y_LSB = 0x0A
REG_ACCEL_DATA_Y_MSB = 0x0B
REG_ACCEL_DATA_Z_LSB = 0x0C
REG_ACCEL_DATA_Z_MSB = 0x0D

REG_MAG_DATA_X_LSB = 0x0E
REG_MAG_DATA_X_MSB = 0x0F
REG_MAG_DATA_Y_LSB = 0x10
REG_MAG_DATA_Y_MSB = 0x11
REG_MAG_DATA_Z_LSB = 0x12
REG_MAG_DATA_Z_MSB = 0x13

REG_GYRO_DATA_X_LSB = 0x14
REG_GYRO_DATA_X_MSB = 0x15
REG_GYRO_DATA_Y_LSB = 0x16
REG_GYRO_DATA_Y_MSB = 0x17
REG_GYRO_DATA_Z_LSB = 0x18
REG_GYRO_DATA_Z_MSB = 0x19

REG_EULER_H_LSB = 0x1A
REG_EULER_H_MSB = 0x1B
REG_EULER_R_LSB = 0x1C
REG_EULER_R_MSB = 0x1D
REG_EULER_P_LSB = 0x1E
REG_EULER_P_MSB = 0x1F

REG_QUATERNION_DATA_W_LSB = 0x20
REG_QUATERNION_DATA_W_MSB = 0x21
REG_QUATERNION_DATA_X_LSB = 0x22
REG_QUATERNION_DATA_X_MSB = 0x23
REG_QUATERNION_DATA_Y_LSB = 0x24
REG_QUATERNION_DATA_Y_MSB = 0x25
REG_QUATERNION_DATA_Z_LSB = 0x26
REG_QUATERNION_DATA_Z_MSB = 0x27

REG_LINEAR_ACCEL_DATA_X_LSB = 0x28
REG_LINEAR_ACCEL_DATA_X_MSB = 0x29
REG_LINEAR_ACCEL_DATA_Y_LSB = 0x2A
REG_LINEAR_ACCEL_DATA_Y_MSB = 0x2B
REG_LINEAR_ACCEL_DATA_Z_LSB = 0x2C
REG_LINEAR_ACCEL_DATA_Z_MSB = 0x2D

REG_GRAVITY_DATA_X_LSB = 0x2E
REG_GRAVITY_DATA_X_MSB = 0x2F
REG_GRAVITY_DATA_Y_LSB = 0x30
REG_GRAVITY_DATA_Y_MSB = 0x31
REG_GRAVITY_DATA_Z_LSB = 0x32
REG_GRAVITY_DATA_Z_MSB = 0x33

REG_TEMP = 0x34

REG_CALIB_STAT = 0x35
REG_SELFTEST_RESULT = 0x36
REG_INTR_STAT = 0x37

REG_SYS_CLK_STAT = 0x38
REG_SYS_STAT = 0x39
REG_SYS_ERR = 0x3A

REG_UNIT_SEL = 0x3B
UNIT_SEL_ACC = 0x01
UNIT_SEL_GYR = 0x02
UNIT_SEL_EUL = 0x04
UNIT_SEL_TEMP = 0x10
UNIT_SEL_ORI = 0x80

REG_DATA_SELECT = 0x3C

REG_OPR_MODE = 0x3D
REG_PWR_MODE = 0x3E

REG_SYS_TRIGGER = 0x3F
REG_TEMP_SOURCE = 0x40

REG_AXIS_MAP_CONFIG = 0x41
REG_AXIS_MAP_SIGN = 0x42

AXIS_REMAP_X = 0x00
AXIS_REMAP_Y = 0x01
AXIS_REMAP_Z = 0x02
AXIS_REMAP_POSITIVE = 0x00
AXIS_REMAP_NEGATIVE = 0x01

REG_SIC_MATRIX_0_LSB = 0x43
REG_SIC_MATRIX_0_MSB = 0x44
REG_SIC_MATRIX_1_LSB = 0x45
REG_SIC_MATRIX_1_MSB = 0x46
REG_SIC_MATRIX_2_LSB = 0x47
REG_SIC_MATRIX_2_MSB = 0x48
REG_SIC_MATRIX_3_LSB = 0x49
REG_SIC_MATRIX_3_MSB = 0x4A
REG_SIC_MATRIX_4_LSB = 0x4B
REG_SIC_MATRIX_4_MSB = 0x4C
REG_SIC_MATRIX_5_LSB = 0x4D
REG_SIC_MATRIX_5_MSB = 0x4E
REG_SIC_MATRIX_6_LSB = 0x4F
REG_SIC_MATRIX_6_MSB = 0x50
REG_SIC_MATRIX_7_LSB = 0x51
REG_SIC_MATRIX_7_MSB = 0x52
REG_SIC_MATRIX_8_LSB = 0x53
REG_SIC_MATRIX_8_MSB = 0x54

REG_ACCEL_OFFSET_X_LSB = 0x55
REG_ACCEL_OFFSET_X_MSB = 0x56
REG_ACCEL_OFFSET_Y_LSB = 0x57
REG_ACCEL_OFFSET_Y_MSB = 0x58
REG_ACCEL_OFFSET_Z_LSB = 0x59
REG_ACCEL_OFFSET_Z_MSB = 0x5A

REG_MAG_OFFSET_X_LSB = 0x5B
REG_MAG_OFFSET_X_MSB = 0x5C
REG_MAG_OFFSET_Y_LSB = 0x5D
REG_MAG_OFFSET_Y_MSB = 0x5E
REG_MAG_OFFSET_Z_LSB = 0x5F
REG_MAG_OFFSET_Z_MSB = 0x60

REG_GYRO_OFFSET_X_LSB = 0x61
REG_GYRO_OFFSET_X_MSB = 0x62
REG_GYRO_OFFSET_Y_LSB = 0x63
REG_GYRO_OFFSET_Y_MSB = 0x64
REG_GYRO_OFFSET_Z_LSB = 0x65
REG_GYRO_OFFSET_Z_MSB = 0x66

REG_ACCEL_RADIUS_LSB = 0x67
REG_ACCEL_RADIUS_MSB = 0x68
REG_MAG_RADIUS_LSB = 0x69
REG_MAG_RADIUS_MSB = 0x6A

POWER_MODE_NORMAL = 0x00
POWER_MODE_LOWPOWER = 0x01
POWER_MODE_SUSPEND = 0x02

OPERATION_MODE_CONFIG = 0x00
OPERATION_MODE_ACCONLY = 0x01
OPERATION_MODE_MAGONLY = 0x02
OPERATION_MODE_GYRONLY = 0x03
OPERATION_MODE_ACCMAG = 0x04
OPERATION_MODE_ACCGYRO = 0x05
OPERATION_MODE_MAGGYRO = 0x06
OPERATION_MODE_AMG = 0x07
OPERATION_MODE_IMUPLUS = 0x08
OPERATION_MODE_COMPASS = 0x09
OPERATION_MODE_M4G = 0x0A
OPERATION_MODE_NDOF_FMC_OFF = 0x0B
OPERATION_MODE_NDOF = 0x0C


class BNO055(object):
    def __init__(self, address=ADDRESS_A, mode=OPERATION_MODE_NDOF):
        self.i2c_bus = i2c.Bus(address=address)
        self.mode = mode

        try:
            self.i2c_bus.write_reg_8(REG_PAGE_ID, 0)
        except IOError:
            pass

        self._config_mode()
        self.i2c_bus.write_reg_8(REG_PAGE_ID, 0)

        if ID != self.i2c_bus.read_reg_8u(REG_CHIP_ID):
            raise RuntimeError('Incorrect chip ID.')

        self.i2c_bus.write_reg_8(REG_SYS_TRIGGER, 0x20)
        time.sleep(.65)

        self.i2c_bus.write_reg_8(REG_PWR_MODE, POWER_MODE_NORMAL)
        self.i2c_bus.write_reg_8(REG_SYS_TRIGGER, 0x00)
        self.i2c_bus.write_reg_8(REG_TEMP_SOURCE, 0x01)

        self._operation_mode()

    def _config_mode(self):
        self.set_mode(OPERATION_MODE_CONFIG)

    def _operation_mode(self):
        self.set_mode(self.mode)

    def set_mode(self, mode):
        self.i2c_bus.write_reg_8(REG_OPR_MODE, mode & 0xFF)
        time.sleep(.03)

    def get_revision(self):
        accel = self.i2c_bus.read_reg_8u(REG_ACCEL_REV_ID)
        mag = self.i2c_bus.read_reg_8u(REG_MAG_REV_ID)
        gyro = self.i2c_bus.read_reg_8u(REG_GYRO_REV_ID)
        bl = self.i2c_bus.read_reg_8u(REG_BL_REV_ID)
        sw_lsb = self.i2c_bus.read_reg_8u(REG_SW_REV_ID_LSB)
        sw_msb = self.i2c_bus.read_reg_8u(REG_SW_REV_ID_MSB)
        sw = ((sw_msb << 8) | sw_lsb) & 0xFFFF
        return sw, bl, accel, mag, gyro

    def set_external_crystal(self, external_crystal):
        self._config_mode()
        if external_crystal:
            self.i2c_bus.write_reg_8(REG_SYS_TRIGGER, 0x80)
        else:
            self.i2c_bus.write_reg_8(REG_SYS_TRIGGER, 0x00)
        self._operation_mode()

    def get_system_status(self, run_self_test=True):
        if run_self_test:
            self._config_mode()
            sys_trigger = self.i2c_bus.read_reg_8u(REG_SYS_TRIGGER)
            self.i2c_bus.write_reg_8(REG_SYS_TRIGGER, sys_trigger | 0x1)
            time.sleep(1.)
            self_test = self.i2c_bus.read_reg_8u(REG_SELFTEST_RESULT)
            self._operation_mode()
        else:
            self_test = None

        status = self.i2c_bus.read_reg_8u(REG_SYS_STAT)
        error = self.i2c_bus.read_reg_8u(REG_SYS_ERR)
        return status, self_test, error

    def get_calibration_status(self):
        cal_status = self.i2c_bus.read_reg_8u(REG_CALIB_STAT)
        sys = (cal_status >> 6) & 0x03
        gyro = (cal_status >> 4) & 0x03
        accel = (cal_status >> 2) & 0x03
        mag = cal_status & 0x03
        return sys, gyro, accel, mag

    def get_calibration(self):
        self._config_mode()
        cal_data = self.i2c_bus.read_reg_list(REG_ACCEL_OFFSET_X_LSB, 22)
        self._operation_mode()
        return cal_data

    def set_calibration(self, data):
        self._config_mode()
        self.i2c_bus.write_reg_list(REG_ACCEL_OFFSET_X_LSB, data)
        self._operation_mode()

    def get_axis_remap(self):
        map_config = self.i2c_bus.read_reg_8u(REG_AXIS_MAP_CONFIG)
        z = (map_config >> 4) & 0x03
        y = (map_config >> 2) & 0x03
        x = map_config & 0x03
        sign_config = self.i2c_bus.read_reg_8u(REG_AXIS_MAP_SIGN)
        x_sign = (sign_config >> 2) & 0x01
        y_sign = (sign_config >> 1) & 0x01
        z_sign = sign_config & 0x01
        return x, y, z, x_sign, y_sign, z_sign

    def set_axis_remap(self, x, y, z, x_sign=AXIS_REMAP_POSITIVE, y_sign=AXIS_REMAP_POSITIVE,
                       z_sign=AXIS_REMAP_POSITIVE):
        self._config_mode()
        map_config = 0x00
        map_config |= (z & 0x03) << 4
        map_config |= (y & 0x03) << 2
        map_config |= x & 0x03
        self.i2c_bus.write_reg_8(REG_AXIS_MAP_CONFIG, map_config)
        sign_config = 0x00
        sign_config |= (x_sign & 0x01) << 2
        sign_config |= (y_sign & 0x01) << 1
        sign_config |= z_sign & 0x01
        self.i2c_bus.write_reg_8(REG_AXIS_MAP_SIGN, sign_config)
        self._operation_mode()

    def _read_vector(self, reg, count=3):
        data = self.i2c_bus.read_reg_list(reg, count * 2)
        result = [0] * count
        for i in range(count):
            result[i] = (((data[(i * 2) + 1] & 0xFF) << 8) | (data[(i * 2)] & 0xFF)) & 0xFFFF
            if result[i] & 0x8000:  # > 32767:
                result[i] -= 0x10000  # 65536
        return result

    def read_euler(self):
        heading, roll, pitch = self._read_vector(REG_EULER_H_LSB)
        return heading / 16., roll / 16., pitch / 16.

    def read_magnetometer(self):
        x, y, z = self._read_vector(REG_MAG_DATA_X_LSB)
        return x / 16., y / 16., z / 16.

    def read_gyroscope(self):
        x, y, z = self._read_vector(REG_GYRO_DATA_X_LSB)
        return x / 16., y / 16., z / 16.

    def read_accelerometer(self):
        x, y, z = self._read_vector(REG_ACCEL_DATA_X_LSB)
        return x / 100., y / 100., z / 100.

    def read_linear_acceleration(self):
        x, y, z = self._read_vector(REG_LINEAR_ACCEL_DATA_X_LSB)
        return x / 100., y / 100., z / 100.

    def read_gravity(self):
        x, y, z = self._read_vector(REG_GRAVITY_DATA_X_LSB)
        return x / 100., y / 100., z / 100.

    def read_quaternion(self):
        w, x, y, z = self._read_vector(REG_QUATERNION_DATA_W_LSB, 4)
        scale = (1. / (1 << 14))
        return x * scale, y * scale, z * scale, w * scale

    def read_temperature(self):
        return self.i2c_bus.read_reg_8s(REG_TEMP)
