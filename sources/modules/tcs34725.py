# -*- coding: utf-8 -*-
#
# TCS34725 - czujnik koloru z podświetleniem LED i wbudowanym filtrem IR.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

COMMAND_BIT = 0x80

ENABLE = 0x00
ENABLE_AIEN = 0x10
ENABLE_WEN = 0x08
ENABLE_AEN = 0x02
ENABLE_PON = 0x01
ATIME = 0x01
WTIME = 0x03
AILTL = 0x04
AILTH = 0x05
AIHTL = 0x06
AIHTH = 0x07
PERS = 0x0C
PERS_NONE = 0b0000
PERS_1_CYCLE = 0b0001
PERS_2_CYCLE = 0b0010
PERS_3_CYCLE = 0b0011
PERS_5_CYCLE = 0b0100
PERS_10_CYCLE = 0b0101
PERS_15_CYCLE = 0b0110
PERS_20_CYCLE = 0b0111
PERS_25_CYCLE = 0b1000
PERS_30_CYCLE = 0b1001
PERS_35_CYCLE = 0b1010
PERS_40_CYCLE = 0b1011
PERS_45_CYCLE = 0b1100
PERS_50_CYCLE = 0b1101
PERS_55_CYCLE = 0b1110
PERS_60_CYCLE = 0b1111
CONFIG = 0x0D
CONFIG_WLONG = 0x02
CONTROL = 0x0F
ID = 0x12  # 0x44 = TCS34721/TCS34725, 0x4D = TCS34723/TCS34727
STATUS = 0x13
STATUS_AINT = 0x10
STATUS_AVALID = 0x01
CDATAL = 0x14
CDATAH = 0x15
RDATAL = 0x16
RDATAH = 0x17
GDATAL = 0x18
GDATAH = 0x19
BDATAL = 0x1A
BDATAH = 0x1B

INTEGRATIONTIME_2_4MS = 0xFF
INTEGRATIONTIME_24MS = 0xF6
INTEGRATIONTIME_50MS = 0xEB
INTEGRATIONTIME_101MS = 0xD5
INTEGRATIONTIME_154MS = 0xC0
INTEGRATIONTIME_700MS = 0x00

GAIN_1X = 0x00
GAIN_4X = 0x01
GAIN_16X = 0x02
GAIN_60X = 0x03

integration_time_delay = {
    0xFF: .0024,
    0xF6: .024,
    0xEB: .050,
    0xD5: .101,
    0xC0: .154,
    0x00: .700
}


class TCS34725(object):
    integration_time = None

    def __init__(self, address=0x29, integration_time=INTEGRATIONTIME_2_4MS, gain=GAIN_4X):
        self.i2c_bus = i2c.Bus(address, big_endian=False)

        chip_id = self.i2c_bus.read_reg_8u(COMMAND_BIT | ID)
        if chip_id != 0x44:
            raise RuntimeError('Incorrect chip ID.')

        self.set_integration_time(integration_time)
        self.set_gain(gain)
        self.enable()

    def enable(self):
        self.i2c_bus.write_reg_8(COMMAND_BIT | ENABLE, ENABLE_PON)
        time.sleep(.01)
        self.i2c_bus.write_reg_8(COMMAND_BIT | ENABLE, ENABLE_PON | ENABLE_AEN)

    def disable(self):
        reg = self.i2c_bus.read_reg_8u(COMMAND_BIT | ENABLE)
        reg &= ~(ENABLE_PON | ENABLE_AEN)
        self.i2c_bus.write_reg_8(COMMAND_BIT | ENABLE, reg)

    def set_integration_time(self, integration_time):
        self.integration_time = integration_time
        self.i2c_bus.write_reg_8(COMMAND_BIT | ATIME, integration_time)

    def set_gain(self, gain):
        self.i2c_bus.write_reg_8(COMMAND_BIT | CONTROL, gain)

    def get_raw_data(self):
        r = self.i2c_bus.read_reg_16u(COMMAND_BIT | RDATAL)
        g = self.i2c_bus.read_reg_16u(COMMAND_BIT | GDATAL)
        b = self.i2c_bus.read_reg_16u(COMMAND_BIT | BDATAL)
        c = self.i2c_bus.read_reg_16u(COMMAND_BIT | CDATAL)
        time.sleep(integration_time_delay[self.integration_time])
        return r, g, b, c

    def set_interrupt(self, enabled):
        enable = self.i2c_bus.read_reg_8u(COMMAND_BIT | ENABLE)
        if enabled:
            enable |= ENABLE_AIEN
        else:
            enable &= ~ENABLE_AIEN
        self.i2c_bus.write_reg_8(COMMAND_BIT | ENABLE, enable)

    def clear_interrupt(self):
        self.i2c_bus.write_reg_8(COMMAND_BIT | 0x66)

    def set_interrupt_limits(self, low, high):
        self.i2c_bus.write_reg_8(COMMAND_BIT | 0x04, low & 0xFF)
        self.i2c_bus.write_reg_8(COMMAND_BIT | 0x05, low >> 8)
        self.i2c_bus.write_reg_8(COMMAND_BIT | 0x06, high & 0xFF)
        self.i2c_bus.write_reg_8(COMMAND_BIT | 0x07, high >> 8)
