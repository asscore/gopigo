# -*- coding: utf-8 -*-
#
# VL53L0X - czujnik odległości.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

SYSRANGE_START = 0x00

SYSTEM_THRESH_HIGH = 0x0C
SYSTEM_THRESH_LOW = 0x0E

SYSTEM_SEQUENCE_CONFIG = 0x01
SYSTEM_RANGE_CONFIG = 0x09
SYSTEM_INTERMEASUREMENT_PERIOD = 0x04

SYSTEM_INTERRUPT_CONFIG_GPIO = 0x0A

GPIO_HV_MUX_ACTIVE_HIGH = 0x84

SYSTEM_INTERRUPT_CLEAR = 0x0B

RESULT_INTERRUPT_STATUS = 0x13
RESULT_RANGE_STATUS = 0x14

RESULT_CORE_AMBIENT_WINDOW_EVENTS_RTN = 0xBC
RESULT_CORE_RANGING_TOTAL_EVENTS_RTN = 0xC0
RESULT_CORE_AMBIENT_WINDOW_EVENTS_REF = 0xD0
RESULT_CORE_RANGING_TOTAL_EVENTS_REF = 0xD4
RESULT_PEAK_SIGNAL_RATE_REF = 0xB6

ALGO_PART_TO_PART_RANGE_OFFSET_MM = 0x28

I2C_SLAVE_DEVICE_ADDRESS = 0x8A

MSRC_CONFIG_CONTROL = 0x60

PRE_RANGE_CONFIG_MIN_SNR = 0x27
PRE_RANGE_CONFIG_VALID_PHASE_LOW = 0x56
PRE_RANGE_CONFIG_VALID_PHASE_HIGH = 0x57
PRE_RANGE_MIN_COUNT_RATE_RTN_LIMIT = 0x64

FINAL_RANGE_CONFIG_MIN_SNR = 0x67
FINAL_RANGE_CONFIG_VALID_PHASE_LOW = 0x47
FINAL_RANGE_CONFIG_VALID_PHASE_HIGH = 0x48
FINAL_RANGE_CONFIG_MIN_COUNT_RATE_RTN_LIMIT = 0x44

PRE_RANGE_CONFIG_SIGMA_THRESH_HI = 0x61
PRE_RANGE_CONFIG_SIGMA_THRESH_LO = 0x62

PRE_RANGE_CONFIG_VCSEL_PERIOD = 0x50
PRE_RANGE_CONFIG_TIMEOUT_MACROP_HI = 0x51
PRE_RANGE_CONFIG_TIMEOUT_MACROP_LO = 0x52

SYSTEM_HISTOGRAM_BIN = 0x81
HISTOGRAM_CONFIG_INITIAL_PHASE_SELECT = 0x33
HISTOGRAM_CONFIG_READOUT_CTRL = 0x55

FINAL_RANGE_CONFIG_VCSEL_PERIOD = 0x70
FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI = 0x71
FINAL_RANGE_CONFIG_TIMEOUT_MACROP_LO = 0x72
CROSSTALK_COMPENSATION_PEAK_RATE_MCPS = 0x20

MSRC_CONFIG_TIMEOUT_MACROP = 0x46

SOFT_RESET_GO2_SOFT_RESET_N = 0xBF
IDENTIFICATION_MODEL_ID = 0xC0
IDENTIFICATION_REVISION_ID = 0xC2

OSC_CALIBRATE_VAL = 0xF8

GLOBAL_CONFIG_VCSEL_WIDTH = 0x32
GLOBAL_CONFIG_SPAD_ENABLES_REF_0 = 0xB0
GLOBAL_CONFIG_SPAD_ENABLES_REF_1 = 0xB1
GLOBAL_CONFIG_SPAD_ENABLES_REF_2 = 0xB2
GLOBAL_CONFIG_SPAD_ENABLES_REF_3 = 0xB3
GLOBAL_CONFIG_SPAD_ENABLES_REF_4 = 0xB4
GLOBAL_CONFIG_SPAD_ENABLES_REF_5 = 0xB5

GLOBAL_CONFIG_REF_EN_START_SELECT = 0xB6
DYNAMIC_SPAD_NUM_REQUESTED_REF_SPAD = 0x4E
DYNAMIC_SPAD_REF_EN_START_OFFSET = 0x4F
POWER_MANAGEMENT_GO1_POWER_FORCE = 0x80

VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV = 0x89

ALGO_PHASECAL_LIM = 0x30
ALGO_PHASECAL_CONFIG_TIMEOUT = 0x30


def encode_vcsel_period(period_pclks):
    return (period_pclks >> 1) - 1


def encode_timeout(timeout_mclks):
    ms_byte = 0

    if timeout_mclks > 0:
        ls_byte = timeout_mclks - 1

        while (int(ls_byte) & 0xFFFFFF00) > 0:
            ls_byte /= 2  # >>=
            ms_byte += 1

        return (ms_byte << 8) | (int(ls_byte) & 0xFF)
    else:
        return 0


def decode_timeout(reg_val):
    return ((reg_val & 0x00FF) << ((reg_val & 0xFF00) >> 8)) + 1


def calc_macro_period(vcsel_period_pclks):
    return ((2304 * vcsel_period_pclks * 1655) + 500) / 1000


def timeout_mclks_to_microseconds(timeout_period_mclks, vcsel_period_pclks):
    macro_period_ns = calc_macro_period(vcsel_period_pclks)
    return ((timeout_period_mclks * macro_period_ns) + (macro_period_ns / 2)) / 1000


def decode_vcsel_period(reg_val):
    return (reg_val + 1) << 1


def timeout_microseconds_to_mclks(timeout_period_us, vcsel_period_pclks):
    macro_period_ns = calc_macro_period(vcsel_period_pclks)
    return ((timeout_period_us * 1000) + (macro_period_ns / 2)) / macro_period_ns


class VL53L0X(object):
    VCSEL_PERIOD_PRE_RANGE = 0
    VCSEL_PERIOD_FINAL_RANGE = 1

    stop_variable = 0
    measurement_timing_budget_us = 0
    timeout_start = 0

    io_timeout = 0

    def __init__(self, address=0x2A, timeout=.5):
        self.i2c_bus = i2c.Bus(address=address)

        try:
            self.i2c_bus.write_reg_8(SOFT_RESET_GO2_SOFT_RESET_N, 0x00)
            time.sleep(.002)
        except IOError:
            pass

        self.address = 0x29
        self.i2c_bus.set_address(self.address)
        self.i2c_bus.write_reg_8(SOFT_RESET_GO2_SOFT_RESET_N, 0x00)
        time.sleep(.005)

        self.i2c_bus.write_reg_8(SOFT_RESET_GO2_SOFT_RESET_N, 0x01)
        time.sleep(.005)

        self.set_address(address)
        self.init()
        self.set_timeout(timeout)

    def set_address(self, address):
        address &= 0x7F
        try:
            self.i2c_bus.write_reg_8(I2C_SLAVE_DEVICE_ADDRESS, address)
            self.address = address
            self.i2c_bus.set_address(self.address)
        except IOError:
            self.i2c_bus.set_address(address)
            self.i2c_bus.write_reg_8(I2C_SLAVE_DEVICE_ADDRESS, address)
            self.address = address
            self.i2c_bus.set_address(self.address)

    def init(self):
        self.i2c_bus.write_reg_8(VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV,
                                 (self.i2c_bus.read_reg_8u(VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV) | 0x01))

        self.i2c_bus.write_reg_8(0x88, 0x00)

        self.i2c_bus.write_reg_8(0x80, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x00)
        self.stop_variable = self.i2c_bus.read_reg_8u(0x91)
        self.i2c_bus.write_reg_8(0x00, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x00)

        self.i2c_bus.write_reg_8(MSRC_CONFIG_CONTROL, (self.i2c_bus.read_reg_8u(MSRC_CONFIG_CONTROL) | 0x12))

        self.set_signal_rate_limit(.25)

        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0xFF)

        spad_count, spad_type_is_aperture, success = self.get_spad_info()
        if not success:
            return False

        ref_spad_map = self.i2c_bus.read_reg_list(GLOBAL_CONFIG_SPAD_ENABLES_REF_0, 6)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(DYNAMIC_SPAD_REF_EN_START_OFFSET, 0x00)
        self.i2c_bus.write_reg_8(DYNAMIC_SPAD_NUM_REQUESTED_REF_SPAD, 0x2C)
        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(GLOBAL_CONFIG_REF_EN_START_SELECT, 0xB4)

        if spad_type_is_aperture:
            first_spad_to_enable = 12
        else:
            first_spad_to_enable = 0

        spads_enabled = 0

        for i in range(48):
            if i < first_spad_to_enable or spads_enabled == spad_count:
                ref_spad_map[int(i / 8)] &= ~(1 << (i % 8))
            elif (ref_spad_map[int(i / 8)] >> (i % 8)) & 0x1:
                spads_enabled += 1

        self.i2c_bus.write_reg_list(GLOBAL_CONFIG_SPAD_ENABLES_REF_0, ref_spad_map)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x00)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x09, 0x00)
        self.i2c_bus.write_reg_8(0x10, 0x00)
        self.i2c_bus.write_reg_8(0x11, 0x00)

        self.i2c_bus.write_reg_8(0x24, 0x01)
        self.i2c_bus.write_reg_8(0x25, 0xFF)
        self.i2c_bus.write_reg_8(0x75, 0x00)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x4E, 0x2C)
        self.i2c_bus.write_reg_8(0x48, 0x00)
        self.i2c_bus.write_reg_8(0x30, 0x20)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x30, 0x09)
        self.i2c_bus.write_reg_8(0x54, 0x00)
        self.i2c_bus.write_reg_8(0x31, 0x04)
        self.i2c_bus.write_reg_8(0x32, 0x03)
        self.i2c_bus.write_reg_8(0x40, 0x83)
        self.i2c_bus.write_reg_8(0x46, 0x25)
        self.i2c_bus.write_reg_8(0x60, 0x00)
        self.i2c_bus.write_reg_8(0x27, 0x00)
        self.i2c_bus.write_reg_8(0x50, 0x06)
        self.i2c_bus.write_reg_8(0x51, 0x00)
        self.i2c_bus.write_reg_8(0x52, 0x96)
        self.i2c_bus.write_reg_8(0x56, 0x08)
        self.i2c_bus.write_reg_8(0x57, 0x30)
        self.i2c_bus.write_reg_8(0x61, 0x00)
        self.i2c_bus.write_reg_8(0x62, 0x00)
        self.i2c_bus.write_reg_8(0x64, 0x00)
        self.i2c_bus.write_reg_8(0x65, 0x00)
        self.i2c_bus.write_reg_8(0x66, 0xA0)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x22, 0x32)
        self.i2c_bus.write_reg_8(0x47, 0x14)
        self.i2c_bus.write_reg_8(0x49, 0xFF)
        self.i2c_bus.write_reg_8(0x4A, 0x00)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x7A, 0x0A)
        self.i2c_bus.write_reg_8(0x7B, 0x00)
        self.i2c_bus.write_reg_8(0x78, 0x21)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x23, 0x34)
        self.i2c_bus.write_reg_8(0x42, 0x00)
        self.i2c_bus.write_reg_8(0x44, 0xFF)
        self.i2c_bus.write_reg_8(0x45, 0x26)
        self.i2c_bus.write_reg_8(0x46, 0x05)
        self.i2c_bus.write_reg_8(0x40, 0x40)
        self.i2c_bus.write_reg_8(0x0E, 0x06)
        self.i2c_bus.write_reg_8(0x20, 0x1A)
        self.i2c_bus.write_reg_8(0x43, 0x40)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x34, 0x03)
        self.i2c_bus.write_reg_8(0x35, 0x44)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x31, 0x04)
        self.i2c_bus.write_reg_8(0x4B, 0x09)
        self.i2c_bus.write_reg_8(0x4C, 0x05)
        self.i2c_bus.write_reg_8(0x4D, 0x04)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x44, 0x00)
        self.i2c_bus.write_reg_8(0x45, 0x20)
        self.i2c_bus.write_reg_8(0x47, 0x08)
        self.i2c_bus.write_reg_8(0x48, 0x28)
        self.i2c_bus.write_reg_8(0x67, 0x00)
        self.i2c_bus.write_reg_8(0x70, 0x04)
        self.i2c_bus.write_reg_8(0x71, 0x01)
        self.i2c_bus.write_reg_8(0x72, 0xFE)
        self.i2c_bus.write_reg_8(0x76, 0x00)
        self.i2c_bus.write_reg_8(0x77, 0x00)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x0D, 0x01)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x01)
        self.i2c_bus.write_reg_8(0x01, 0xF8)

        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x8E, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x00)

        self.i2c_bus.write_reg_8(SYSTEM_INTERRUPT_CONFIG_GPIO, 0x04)
        self.i2c_bus.write_reg_8(GPIO_HV_MUX_ACTIVE_HIGH,
                                 self.i2c_bus.read_reg_8u(GPIO_HV_MUX_ACTIVE_HIGH) & ~0x10)
        self.i2c_bus.write_reg_8(SYSTEM_INTERRUPT_CLEAR, 0x01)

        self.measurement_timing_budget_us = self.get_measurement_timing_budget()

        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0xE8)

        self.set_measurement_timing_budget(self.measurement_timing_budget_us)

        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0x01)
        if not self.perform_single_ref_calibration(0x40):
            return False

        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0x02)
        if not self.perform_single_ref_calibration(0x00):
            return False

        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0xE8)

        return True

    def set_signal_rate_limit(self, limit_mcps):
        if limit_mcps < 0 or limit_mcps > 511.99:
            return False

        self.i2c_bus.write_reg_16(FINAL_RANGE_CONFIG_MIN_COUNT_RATE_RTN_LIMIT, int(limit_mcps * (1 << 7)))
        return True

    def get_spad_info(self):
        self.i2c_bus.write_reg_8(0x80, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x00)

        self.i2c_bus.write_reg_8(0xFF, 0x06)
        self.i2c_bus.write_reg_8(0x83, self.i2c_bus.read_reg_8u(0x83) | 0x04)
        self.i2c_bus.write_reg_8(0xFF, 0x07)
        self.i2c_bus.write_reg_8(0x81, 0x01)

        self.i2c_bus.write_reg_8(0x80, 0x01)

        self.i2c_bus.write_reg_8(0x94, 0x6b)
        self.i2c_bus.write_reg_8(0x83, 0x00)
        self.start_timeout()
        while self.i2c_bus.read_reg_8u(0x83) == 0x00:
            if self.check_timeout_expired():
                return 0, 0, False

        self.i2c_bus.write_reg_8(0x83, 0x01)
        tmp = self.i2c_bus.read_reg_8u(0x92)

        count = tmp & 0x7F
        type_is_aperture = (tmp >> 7) & 0x01

        self.i2c_bus.write_reg_8(0x81, 0x00)
        self.i2c_bus.write_reg_8(0xFF, 0x06)
        self.i2c_bus.write_reg_8(0x83, self.i2c_bus.read_reg_8u(0x83 & ~0x04))
        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x01)

        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x00)

        return count, type_is_aperture, True

    def check_timeout_expired(self):
        if 0 < self.io_timeout < (time.time() - self.timeout_start):
            return True
        return False

    def start_timeout(self):
        self.timeout_start = time.time()

    def get_measurement_timing_budget(self):
        START_OVERHEAD = 1910
        END_OVERHEAD = 960
        MSRC_OVERHEAD = 660
        TCC_OVERHEAD = 590
        DSS_OVERHEAD = 690
        PRE_RANGE_OVERHEAD = 660
        FINAL_RANGE_OVERHEAD = 550

        budget_us = START_OVERHEAD + END_OVERHEAD

        enables = self.get_sequence_step_enables()
        timeouts = self.get_sequence_step_timeouts(enables['pre_range'])

        if enables['tcc']:
            budget_us += (timeouts['msrc_dss_tcc_us'] + TCC_OVERHEAD)

        if enables['dss']:
            budget_us += 2 * (timeouts['msrc_dss_tcc_us'] + DSS_OVERHEAD)
        elif enables['msrc']:
            budget_us += (timeouts['msrc_dss_tcc_us'] + MSRC_OVERHEAD)

        if enables['pre_range']:
            budget_us += (timeouts['pre_range_us'] + PRE_RANGE_OVERHEAD)

        if enables['final_range']:
            budget_us += (timeouts['final_range_us'] + FINAL_RANGE_OVERHEAD)

        self.measurement_timing_budget_us = budget_us  # store for internal reuse
        return budget_us

    def get_sequence_step_enables(self):
        sequence_config = self.i2c_bus.read_reg_8u(SYSTEM_SEQUENCE_CONFIG)
        sequence_step_enables = dict(tcc=0, msrc=0, dss=0, pre_range=0, final_range=0)
        sequence_step_enables['tcc'] = (sequence_config >> 4) & 0x1
        sequence_step_enables['dss'] = (sequence_config >> 3) & 0x1
        sequence_step_enables['msrc'] = (sequence_config >> 2) & 0x1
        sequence_step_enables['pre_range'] = (sequence_config >> 6) & 0x1
        sequence_step_enables['final_range'] = (sequence_config >> 7) & 0x1
        return sequence_step_enables

    def get_sequence_step_timeouts(self, pre_range):
        sequence_step_timeouts = dict(pre_range_vcsel_period_pclks=0, final_range_vcsel_period_pclks=0,
                                      msrc_dss_tcc_mclks=0, pre_range_mclks=0, final_range_mclks=0, msrc_dss_tcc_us=0,
                                      pre_range_us=0, final_range_us=0)
        sequence_step_timeouts['pre_range_vcsel_period_pclks'] = self.get_vcsel_pulse_period(
            self.VCSEL_PERIOD_PRE_RANGE)

        sequence_step_timeouts['msrc_dss_tcc_mclks'] = self.i2c_bus.read_reg_8u(MSRC_CONFIG_TIMEOUT_MACROP) + 1
        sequence_step_timeouts['msrc_dss_tcc_us'] = timeout_mclks_to_microseconds(
            sequence_step_timeouts['msrc_dss_tcc_mclks'], sequence_step_timeouts['pre_range_vcsel_period_pclks'])

        sequence_step_timeouts['pre_range_mclks'] = decode_timeout(
            self.i2c_bus.read_reg_16u(PRE_RANGE_CONFIG_TIMEOUT_MACROP_HI))
        sequence_step_timeouts['pre_range_us'] = timeout_mclks_to_microseconds(
            sequence_step_timeouts['pre_range_mclks'], sequence_step_timeouts['pre_range_vcsel_period_pclks'])

        sequence_step_timeouts['final_range_vcsel_period_pclks'] = self.get_vcsel_pulse_period(
            self.VCSEL_PERIOD_FINAL_RANGE)

        sequence_step_timeouts['final_range_mclks'] = decode_timeout(
            self.i2c_bus.read_reg_16u(FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI))

        if pre_range:
            sequence_step_timeouts['final_range_mclks'] -= sequence_step_timeouts['pre_range_mclks']

        sequence_step_timeouts['final_range_us'] = timeout_mclks_to_microseconds(
            sequence_step_timeouts['final_range_mclks'], sequence_step_timeouts['final_range_vcsel_period_pclks'])

        return sequence_step_timeouts

    def get_vcsel_pulse_period(self, type):
        if type == self.VCSEL_PERIOD_PRE_RANGE:
            return decode_vcsel_period(self.i2c_bus.read_reg_8u(PRE_RANGE_CONFIG_VCSEL_PERIOD))
        elif type == self.VCSEL_PERIOD_FINAL_RANGE:
            return decode_vcsel_period(self.i2c_bus.read_reg_8u(FINAL_RANGE_CONFIG_VCSEL_PERIOD))
        else:
            return 255

    def set_measurement_timing_budget(self, budget_us):
        START_OVERHEAD = 1320
        END_OVERHEAD = 960
        MSRC_OVERHEAD = 660
        TCC_OVERHEAD = 590
        DSS_OVERHEAD = 690
        PRE_RANGE_OVERHEAD = 660
        FINAL_RANGE_OVERHEAD = 550

        MIN_TIMING_BUDGET = 20000

        if budget_us < MIN_TIMING_BUDGET:
            return False

        used_budget_us = START_OVERHEAD + END_OVERHEAD

        enables = self.get_sequence_step_enables()
        timeouts = self.get_sequence_step_timeouts(enables['pre_range'])

        if enables['tcc']:
            used_budget_us += (timeouts['msrc_dss_tcc_us'] + TCC_OVERHEAD)

        if enables['dss']:
            used_budget_us += 2 * (timeouts['msrc_dss_tcc_us'] + DSS_OVERHEAD)
        elif enables['msrc']:
            used_budget_us += (timeouts['msrc_dss_tcc_us'] + MSRC_OVERHEAD)

        if enables['pre_range']:
            used_budget_us += (timeouts['pre_range_us'] + PRE_RANGE_OVERHEAD)

        if enables['final_range']:
            used_budget_us += FINAL_RANGE_OVERHEAD

            if used_budget_us > budget_us:
                return False

            final_range_timeout_us = budget_us - used_budget_us

            final_range_timeout_mclks = timeout_microseconds_to_mclks(final_range_timeout_us,
                                                                           timeouts['final_range_vcsel_period_pclks'])

            if enables['pre_range']:
                final_range_timeout_mclks += timeouts['pre_range_mclks']

            self.i2c_bus.write_reg_16(FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI,
                                      encode_timeout(final_range_timeout_mclks))

            self.measurement_timing_budget_us = budget_us
        return True

    def perform_single_ref_calibration(self, vhv_init_byte):
        self.i2c_bus.write_reg_8(SYSRANGE_START, 0x01 | vhv_init_byte)

        self.start_timeout()
        while (self.i2c_bus.read_reg_8u(RESULT_INTERRUPT_STATUS) & 0x07) == 0:
            if self.check_timeout_expired():
                return False

        self.i2c_bus.write_reg_8(SYSTEM_INTERRUPT_CLEAR, 0x01)

        self.i2c_bus.write_reg_8(SYSRANGE_START, 0x00)

        return True

    def set_timeout(self, timeout):
        self.io_timeout = timeout

    def start_continuous(self, period_ms=0):
        self.i2c_bus.write_reg_8(0x80, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x00)
        self.i2c_bus.write_reg_8(0x91, self.stop_variable)
        self.i2c_bus.write_reg_8(0x00, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x00)

        if period_ms != 0:
            osc_calibrate_val = self.i2c_bus.read_reg_16u(OSC_CALIBRATE_VAL)

            if osc_calibrate_val != 0:
                period_ms *= osc_calibrate_val

            self.i2c_bus.write_reg_32(SYSTEM_INTERMEASUREMENT_PERIOD, period_ms)

            self.i2c_bus.write_reg_8(SYSRANGE_START, 0x04)  # VL53L0X_REG_SYSRANGE_MODE_TIMED
        else:
            self.i2c_bus.write_reg_8(SYSRANGE_START, 0x02)  # VL53L0X_REG_SYSRANGE_MODE_BACKTOBACK

    def read_range_continuous_millimeters(self):
        self.start_timeout()
        while (self.i2c_bus.read_reg_8u(RESULT_INTERRUPT_STATUS) & 0x07) == 0:
            if self.check_timeout_expired():
                raise IOError('Timeout occurred.')

        range = self.i2c_bus.read_reg_16u(RESULT_RANGE_STATUS + 10)

        self.i2c_bus.write_reg_8(SYSTEM_INTERRUPT_CLEAR, 0x01)

        return range

    def set_vcsel_pulse_period(self, type, period_pclks):
        vcsel_period_reg = encode_vcsel_period(period_pclks)

        enables = self.get_sequence_step_enables()
        timeouts = self.get_sequence_step_timeouts(enables['pre_range'])

        if type == self.VCSEL_PERIOD_PRE_RANGE:
            if period_pclks == 12:
                self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VALID_PHASE_HIGH, 0x18)
            elif period_pclks == 14:
                self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VALID_PHASE_HIGH, 0x30)
            elif period_pclks == 16:
                self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VALID_PHASE_HIGH, 0x40)
            elif period_pclks == 18:
                self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VALID_PHASE_HIGH, 0x50)
            else:
                return False

            self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VALID_PHASE_LOW, 0x08)

            self.i2c_bus.write_reg_8(PRE_RANGE_CONFIG_VCSEL_PERIOD, vcsel_period_reg)

            new_pre_range_timeout_mclks = timeout_microseconds_to_mclks(timeouts['pre_range_us'], period_pclks)

            self.i2c_bus.write_reg_16(PRE_RANGE_CONFIG_TIMEOUT_MACROP_HI,
                                      encode_timeout(new_pre_range_timeout_mclks))

            new_msrc_timeout_mclks = timeout_microseconds_to_mclks(timeouts['msrc_dss_tcc_us'], period_pclks)

            if new_msrc_timeout_mclks > 256:
                self.i2c_bus.write_reg_8(MSRC_CONFIG_TIMEOUT_MACROP, 255)
            else:
                self.i2c_bus.write_reg_8(MSRC_CONFIG_TIMEOUT_MACROP, (new_msrc_timeout_mclks - 1))

        elif type == self.VCSEL_PERIOD_FINAL_RANGE:
            if period_pclks == 8:
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_HIGH, 0x10)
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_LOW, 0x08)
                self.i2c_bus.write_reg_8(GLOBAL_CONFIG_VCSEL_WIDTH, 0x02)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x0C)
                self.i2c_bus.write_reg_8(0xFF, 0x01)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_LIM, 0x30)
                self.i2c_bus.write_reg_8(0xFF, 0x00)
            elif period_pclks == 10:
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_HIGH, 0x28)
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_LOW, 0x08)
                self.i2c_bus.write_reg_8(GLOBAL_CONFIG_VCSEL_WIDTH, 0x03)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x09)
                self.i2c_bus.write_reg_8(0xFF, 0x01)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_LIM, 0x20)
                self.i2c_bus.write_reg_8(0xFF, 0x00)
            elif period_pclks == 12:
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_HIGH, 0x38)
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_LOW, 0x08)
                self.i2c_bus.write_reg_8(GLOBAL_CONFIG_VCSEL_WIDTH, 0x03)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x08)
                self.i2c_bus.write_reg_8(0xFF, 0x01)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_LIM, 0x20)
                self.i2c_bus.write_reg_8(0xFF, 0x00)
            elif period_pclks == 14:
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_HIGH, 0x48)
                self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VALID_PHASE_LOW, 0x08)
                self.i2c_bus.write_reg_8(GLOBAL_CONFIG_VCSEL_WIDTH, 0x03)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x07)
                self.i2c_bus.write_reg_8(0xFF, 0x01)
                self.i2c_bus.write_reg_8(ALGO_PHASECAL_LIM, 0x20)
                self.i2c_bus.write_reg_8(0xFF, 0x00)
            else:
                return False

            self.i2c_bus.write_reg_8(FINAL_RANGE_CONFIG_VCSEL_PERIOD, vcsel_period_reg)

            new_final_range_timeout_mclks = timeout_microseconds_to_mclks(timeouts['final_range_us'],
                                                                               period_pclks)

            if enables['pre_range']:
                new_final_range_timeout_mclks += timeouts['pre_range_mclks']

            self.i2c_bus.write_reg_16(FINAL_RANGE_CONFIG_TIMEOUT_MACROP_HI,
                                      encode_timeout(new_final_range_timeout_mclks))
        else:
            return False

        self.set_measurement_timing_budget(self.measurement_timing_budget_us)

        sequence_config = self.i2c_bus.read_reg_8u(SYSTEM_SEQUENCE_CONFIG)
        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, 0x02)
        self.perform_single_ref_calibration(0x0)
        self.i2c_bus.write_reg_8(SYSTEM_SEQUENCE_CONFIG, sequence_config)

        return True

    def read_range_single_millimeters(self):
        self.i2c_bus.write_reg_8(0x80, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x01)
        self.i2c_bus.write_reg_8(0x00, 0x00)
        self.i2c_bus.write_reg_8(0x91, self.stop_variable)
        self.i2c_bus.write_reg_8(0x00, 0x01)
        self.i2c_bus.write_reg_8(0xFF, 0x00)
        self.i2c_bus.write_reg_8(0x80, 0x00)

        self.i2c_bus.write_reg_8(SYSRANGE_START, 0x01)

        self.start_timeout()
        while self.i2c_bus.read_reg_8u(SYSRANGE_START) & 0x01:
            if self.check_timeout_expired():
                raise IOError('Timeout occurred.')
        return self.read_range_continuous_millimeters()
