# -*- coding: utf-8 -*-
#
# P9813 - sterownik LED RGB.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO


class P9813(object):
    def __init__(self, ci_pin, di_pin, number_of_leds):
        self.ci_pin = ci_pin
        self.di_pin = di_pin
        self.number_of_leds = number_of_leds

        GPIO.setup(self.ci_pin, GPIO.OUT)
        GPIO.setup(self.di_pin, GPIO.OUT)

        for i in range(self.number_of_leds):
            self.set_color(i, 0, 0, 0)

    def _clk(self):
        GPIO.output(self.ci_pin, GPIO.LOW)
        time.sleep(.00002)
        GPIO.output(self.ci_pin, GPIO.HIGH)
        time.sleep(.00002)

    def _send_byte(self, b):
        for i in range(8):
            if b & 0x80 != 0:
                GPIO.output(self.di_pin, GPIO.HIGH)
            else:
                GPIO.output(self.di_pin, GPIO.LOW)
            self._clk()

            b = b << 1

    def _send_color(self, r, g, b):
        prefix = 0xC0  # B11000000
        if b & 0x80 == 0:
            prefix |= 0x20  # B00100000
        if b & 0x40 == 0:
            prefix |= 0x10  # B00010000
        if g & 0x80 == 0:
            prefix |= 0x08  # B00001000
        if g & 0x40 == 0:
            prefix |= 0x04  # B00000100
        if r & 0x80 == 0:
            prefix |= 0x02  # B00000010
        if r & 0x40 == 0:
            prefix |= 0x01  # B00000001
        self._send_byte(prefix)  # 1 1 B7 B6 G7 G6 R7 R6

        self._send_byte(b)
        self._send_byte(g)
        self._send_byte(r)

    def set_color(self, led, r, g, b):
        self._send_byte(0x00)
        self._send_byte(0x00)
        self._send_byte(0x00)
        self._send_byte(0x00)

        for i in range(self.number_of_leds):
            self._send_color(r, g, b)

        self._send_byte(0x00)
        self._send_byte(0x00)
        self._send_byte(0x00)
        self._send_byte(0x00)
