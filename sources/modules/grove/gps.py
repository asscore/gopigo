# -*- coding: utf-8 -*-
#
# Moduł GPS UART z anteną (Grove - GPS).
#  http://wiki.seeedstudio.com/Grove-GPS/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import datetime
import re
import serial
import time


def to_miliseconds(dt):
    return int((dt - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)


def to_timestamp(d_s, t_s):
    try:
        d = datetime.datetime.strptime(d_s, '%d%m%y').date()
        ms_s = t_s[6:]
        ms = ms_s and int(float(ms_s) * 1000000) or 0
        t = datetime.time(hour=int(t_s[0:2]), minute=int(t_s[2:4]), second=int(t_s[4:6]), microsecond=ms)
        dt = datetime.datetime.combine(d, t)
        return to_miliseconds(dt)
    except ValueError:
        now = datetime.datetime.utcnow()
        return to_miliseconds(now)


def to_decimal_degrees(dm):
    if len(dm) == 0:
        return -1.
    d, m = re.match(r'^(\d+)(\d\d\.\d+)$', dm).groups()
    return round(float(d) + float(m) / 60, 6)


class GPS(object):
    def __init__(self, port='/dev/ttyAMA0', baud=9600, timeout=0):
        self.ser = serial.Serial(port, baud, timeout=timeout)
        self.ser.flush()
        self.raw_line = ''
        self.rmc = []
        self.gga = []

    def _readline(self):
        line = ''
        while True:
            time.sleep(.01)
            ch = self.ser.read()
            line += ch
            if ch == '\r' or ch == '':
                return line

    def read(self):
        """ Czas namiaru, szerokość i długość geograficzna, jakość namiaru, liczba satelit, wysokość """
        while True:
            self.raw_line = self._readline().strip()
            # $GPRMC - Recommended Minimum Specific GPS/Transit Data (czas, data, pozycja, kierunek, prędkość)
            if self.raw_line[:6] == '$GPRMC':
                break
        self.rmc = self.raw_line.split(',')

        while True:
            self.raw_line = self._readline().strip()
            # $GPGGA - Global Positioning System Fix Data (czas i pozycja)
            if self.raw_line[:6] == '$GPGGA':
                break
        try:
            # Czasami zdarzają się takie przypadki: 
            # ['$GPGGA', '102707.000', '5034.2945', '$GPGGA', '102709.000', '5034.2945', 'N', '02204.0417',
            # 'E', '1', '9', '0.93', '165.0', 'M', '40.3', 'M', '', '*59']
            index = self.raw_line.index('$GPGGA', 5, len(self.raw_line))
            self.raw_line = self.raw_line[index:]
        except ValueError:
            pass
        self.gga = self.raw_line.split(',')

        timestamp = to_timestamp(self.rmc[9], self.gga[1])  # Data i czas pobrania namiaru (UTC)
        latitude = self.gga[2]  # Szerokość geograficzna (ddmm.mmmm)
        latitude_ns = self.gga[3]  # N - północ, S - południe
        longitude = self.gga[4]  # Długość geograficzna (dddmm.mmmm)
        longitude_ew = self.gga[5]  # E - wschód, W - zachód
        quality = int(self.gga[6])  # Jakość namiaru (0 - błędna, 1 - GPS fix, 2 - DGPS fix, ...)
        satellites = int(self.gga[7])  # Liczba widocznych satelitów
        if self.gga[9] == '':  # Wysokość nad poziomem morza
            altitude = -1.
        else:
            altitude = float(self.gga[9])

        # Konwersja współrzędnych z formatu 'stopnie/minuty' na format stosowany w Mapy Google
        latitude = to_decimal_degrees(latitude)
        longitude = to_decimal_degrees(longitude)

        if latitude_ns == 'S':
            latitude = -latitude
        if longitude_ew == 'W':
            longitude = -longitude

        return timestamp, latitude, longitude, quality, satellites, altitude
