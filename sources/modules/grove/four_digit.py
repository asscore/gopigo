# -*- coding: utf-8 -*-
#
# Moduł z 4 cyfrowym wyświetlaczem 7-segmentowym (Grove - 4 Digit Display).
#  http://wiki.seeedstudio.com/Grove-4-Digit_Display/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import tm1637


class FourDigit(object):
    def __init__(self, clk_pin, dio_pin, brightness=2):
        self.tm1637 = tm1637.TM1637(clk_pin, dio_pin, brightness)

    def set_brightness(self, brightness):
        """ Jasność

        brightness: 0 - 7
        """
        self.tm1637.set_brightness(brightness)

    def set_doublepoint(self, point):
        """ Dwukropek

        point: True/False
        """
        self.tm1637.set_doublepoint(point)

    def set_number(self, value, leading_zero=False):
        """ Wartość numeryczna

        value: 0 - 9999
        leading_zero: True/False
        """
        digits = []
        divisors = [1, 10, 100, 1000]
        leading = True

        for i in range(4):
            divisor = divisors[4 - 1 - i]
            d = value // divisor

            if d == 0:
                if leading_zero or not leading or i == 3:
                    digits.append(d)
                else:
                    digits.append(0x7F)
            else:
                digits.append(d)
                value -= d * divisor
                leading = False

        self.tm1637.set_segments(digits)

    def set_digit(self, segment, value):
        """ Cyfra

        segment: 0 - 3
        value: 0 - 15 lub 0x00 - 0xFF
        """
        self.tm1637.set_segments([value], segment)

    def set_segment(self, segment, leds):
        """ Stan segmentu

        segment: 0 - 3
        leds: 0 - 255 lub 0x00 - 0xFF
             0
           5   1
             6
           4   2
             3
         8 bit to dwukropek (tylko segment 1)
        """
        self.tm1637.set_segments([leds], segment, raw_data=True)

    def clear(self):
        """ Wyczyszczenie """
        self.tm1637.clear()
