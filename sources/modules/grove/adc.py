# -*- coding: utf-8 -*-
#
# Moduł z przetwornikiem analogowo-cyfrowym o 12-bitowej precyzji (Grove - I2C ADC).
#  http://wiki.seeedstudio.com/Grove-I2C_ADC/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import adc121c021


class ADC(object):
    def __init__(self, address=0x50):
        self.adc121c021 = adc121c021.ADC121C021(address)

    def read(self):
        """ Wartość (12-bitowa) """
        return self.adc121c021.read()
