# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem gestów (Grove - Gesture).
#  http://wiki.seeedstudio.com/Grove-Gesture_v1.0/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import paj7620


class Gesture(object):
    NOTHING = 0
    FORWARD = 1
    BACKWARD = 2
    RIGHT = 3
    LEFT = 4
    UP = 5
    DOWN = 6
    CLOCKWISE = 7
    ANTI_CLOCKWISE = 8
    WAVE = 9

    def __init__(self):
        self.paj7620 = paj7620.PAJ7620()

    def read(self):
        """ Wykryty gest """
        return self.paj7620.return_gesture()
