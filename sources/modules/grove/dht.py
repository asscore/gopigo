# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem temperatury i wilgotności (Grove - Temperature and Humidity Sensor).
#  http://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import dht11


class DHT(object):
    def __init__(self, pin):
        self.dht11 = dht11.DHT11(pin)

    def read(self):
        """ Temperatura w C """
        result = self.dht11.read()
        if result.is_valid():
            return float(result.temperature), float(result.humidity)
        return float('nan'), float('nan')
