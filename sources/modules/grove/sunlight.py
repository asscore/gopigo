# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem światła UV (Grove - Sunlight Sensor).
#  http://wiki.seeedstudio.com/Grove-Sunlight_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import si1145


class Sunlight(object):
    def __init__(self):
        self.si1145 = si1145.SI1145()

    def read_uv(self):
        """ Indeks UV """
        return self.si1145.read_uv() / 100

    def read_visible(self):
        """ Poziom światła widzialnego w lm """
        return self.si1145.read_visible()

    def read_ir(self):
        """ Poziom IR w lm """
        return self.si1145.read_ir()
