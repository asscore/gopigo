# -*- coding: utf-8 -*-
#
# Moduł z diodą LED RGB (Grove - Chainable RGB LED).
#  http://wiki.seeedstudio.com/Grove-Chainable_RGB_LED/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import p9813


class ChainableLED(object):
    def __init__(self, ci_pin, di_pin, number_of_leds):
        self.p9813 = p9813.P9813(ci_pin, di_pin, number_of_leds)

    def set_color(self, led, r, g, b):
        """ Kolor diody """
        self.p9813.set_color(led, r, g, b)
