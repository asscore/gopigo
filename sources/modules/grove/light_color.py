# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem koloru (Grove - I2C Color Sensor).
#  http://wiki.seeedstudio.com/Grove-I2C_Color_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import tcs34725


class LightColor(object):
    INTEGRATIONTIME_2_4MS = 0xFF
    INTEGRATIONTIME_24MS = 0xF6
    INTEGRATIONTIME_50MS = 0xEB
    INTEGRATIONTIME_101MS = 0xD5
    INTEGRATIONTIME_154MS = 0xC0
    INTEGRATIONTIME_700MS = 0x00

    GAIN_1X = 0x00
    GAIN_4X = 0x01
    GAIN_16X = 0x02
    GAIN_60X = 0x03

    def __init__(self, integration_time=INTEGRATIONTIME_2_4MS, gain=GAIN_4X):
        self.tcs34725 = tcs34725.TCS34725(integration_time, gain)

    def read_raw_color(self):
        """ Składowe R, G, B, C koloru """
        return self.tcs34725.get_raw_data()

    def read_rgb(self):
        """ Kolor RGB w formacie 8-bitowym """
        color = self.read_raw_color()
        r, g, b, c = list(map(lambda c: int(c * 255 / color[3]), color))
        return r, g, b

    @staticmethod
    def calculate_color_temperature(r, g, b):
        """ Barwa światła w K """
        x = (-.14282 * r) + (1.54924 * g) + (-.95641 * b)
        y = (-.32466 * r) + (1.57837 * g) + (-.73191 * b)
        z = (-.68202 * r) + (.77073 * g) + (.56332 * b)
        if (x + y + z) == 0:
            return None
        xc = x / (x + y + z)
        yc = y / (x + y + z)
        if (.1858 - yc) == 0:
            return None
        n = (xc - .3320) / (.1858 - yc)
        cct = (449. * (n ** 3.)) + (3525. * (n ** 2.)) + (6823.3 * n) + 5520.33
        return int(cct)

    @staticmethod
    def calculate_lux(r, g, b):
        """ Natężenie oświetlenia w lx """
        illuminance = (-.32466 * r) + (1.57837 * g) + (-.73191 * b)
        return int(illuminance)
