# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem wilgotności, ciśnienia i temperatury (Grove - Barometer Sensor (BME280)).
#  http://wiki.seeedstudio.com/Grove-Barometer_Sensor-BME280/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import bme280


class Barometer(object):
    def __init__(self, address=0x76):
        self.bme280 = bme280.BME280(address=address, t_mode=bme280.OSAMPLE_2, p_mode=bme280.OSAMPLE_4,
                                    h_mode=bme280.OSAMPLE_4, standby=bme280.STANDBY_10, filter=bme280.FILTER_8)

    def read(self):
        """ Temperatura w C, ciśnienie w hPa, wilgotność w % """
        return self.bme280.read()
