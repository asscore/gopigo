# -*- coding: utf-8 -*-
#
# Moduł z 3-osiowym akcelerometrem, żyroskopem i magnetometrem (Grove - IMU 9DOF).
#  http://wiki.seeedstudio.com/Grove-IMU_9DOF_v2.0/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math

from modules import mpu9250


class IMU(object):
    def __init__(self):
        self.mpu9250 = mpu9250.MPU9250()

    def read_accelerometer(self):
        """ Stan akcelerometru w g """
        axes = self.mpu9250.get_accelerometer_axes()
        return axes['x'], axes['y'], axes['z']

    def read_gyroscope(self):
        """ Stan żyroskopu w stopnie/s """
        axes = self.mpu9250.get_gyroscopic_axes()
        return axes['x'], axes['y'], axes['z']

    def read_temperature(self):
        """ Temperatura w C """
        return self.mpu9250.get_temperature()

    def read_magnetometer(self):
        """ Stan kompasu w uT """
        axes = self.mpu9250.get_magnetic_axes()
        return axes['x'], axes['y'], axes['z']

    def get_north_point(self, magnetic_declination=5.83):
        """ Położenie względem północy w stopniach """
        x, y, z = self.read_magnetometer()

        # Czujnik zamontowany z przodu robota, układem na zewnątrz
        heading = -math.atan2(x, -z) * 180 / math.pi

        if heading < 0:
            heading += 360
        elif heading > 360:
            heading -= 360

        # Wartość ujemna robot skierowany na zachód, wartość dodatnia na wschód (0 stopni północ)
        if 180 < heading <= 360:
            heading -= 360

        # http://www.magnetic-declination.com/
        heading += magnetic_declination  # Domyślnie +5 50 (Stalowa Wola, Polska)
        return heading
