# -*- coding: utf-8 -*-
#
# Moduł z wyświetlaczem LED linijka (Grove - LED Bar).
#  http://wiki.seeedstudio.com/Grove-LED_Bar/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import my9221


class LEDBar(object):
    def __init__(self, di_pin, dcki_pin, reverse=False):
        self.my9221 = my9221.MY9221(di_pin, dcki_pin, reverse)

    def set_reverse(self, reverse):
        """ Zmiana kierunku

        reverse: True/False

        True - od zielonego do czerwonego
        False - od czerwonego do zielonego
        """
        self.my9221.set_reverse(reverse)

    def set_level(self, level):
        """ Liczba diód zapalonych

        level: 0 - 10
        """
        level = max(0, min(10, level))
        self.my9221.set_level(level)

    def set_led(self, led, state):
        """ Stan diody (zapalona/zgaszona)

        led: 1 - 10
        state: 0/1
        """
        led = max(1, min(10, led))
        self.my9221.set_led(led, state)

    def toggle_led(self, led):
        """ Przełączenie stanu diody

        led: 1 - 10
        """
        led = max(1, min(10, led))
        self.my9221.toggle_led(led)

    def set_bits(self, bits):
        """ Stan linijki

        bits: 0 - 1023 lub 0x00 - 0x3FF lub 0b0000000000 - 0b1111111111

        Dioda 1=1 2=2 3=4 4=8 5=16 6=32 7=64 8=128 9=256 10=512
        """
        self.my9221.set_bits(bits & 0x3FF)

    def get_bits(self):
        """ Bieżący stan linijki """
        return self.my9221.get_bits()
