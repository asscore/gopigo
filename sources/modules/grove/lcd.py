# -*- coding: utf-8 -*-
#
# Moduł wyświetlacza LCD RGB z podświetleniem (Grove - LCD RGB Backlight).
#  http://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

import time

import i2c

DISPLAY_RGB_ADDR = 0x62
DISPLAY_TEXT_ADDR = 0x3E

LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

LCD_DISPLAYON  = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5X10DOTS = 0x04
LCD_5X8DOTS = 0x00

REG_MODE1 = 0x00
REG_MODE2 = 0x01
REG_OUTPUT = 0x08
REG_RED = 0x04  # PWM2
REG_GREEN = 0x03  # PWM1
REG_BLUE = 0x02  # PWM0


class LCD(object):
    def __init__(self):
        self.i2c_bus = i2c.Bus(DISPLAY_TEXT_ADDR)

    def _command(self, reg, data):
        self.i2c_bus.set_address(DISPLAY_TEXT_ADDR)
        if type(data).__name__ == 'list':
            self.i2c_bus.write_reg_list(reg, data) 
        else:
            self.i2c_bus.write_reg_8(reg, data)

    def set_backlight(self, r, g, b):
        """ Kolor podświetlenia """
        self.i2c_bus.set_address(DISPLAY_RGB_ADDR)  # PCA9633
        self.i2c_bus.write_reg_8(REG_MODE1, 0)
        self.i2c_bus.write_reg_8(REG_MODE2, 0)
        self.i2c_bus.write_reg_8(REG_OUTPUT, 0xAA)
        self.i2c_bus.write_reg_8(REG_RED, r)
        self.i2c_bus.write_reg_8(REG_GREEN, g)
        self.i2c_bus.write_reg_8(REG_BLUE, b)

    def set_text(self, text, no_refresh=False):
        """ Tekst """
        if no_refresh:
            # Na początek linii
            self._command(0x80, LCD_RETURNHOME)
        else:
            # Wyzerowanie wyświetlacza
            self._command(0x80, LCD_CLEARDISPLAY)
        time.sleep(.05)
        # Ekran włączony, brak kursora
        self._command(0x80, LCD_DISPLAYCONTROL | LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF)
        # 2 linie
        self._command(0x80, LCD_FUNCTIONSET | LCD_2LINE)
        time.sleep(.05)
        # Domyślny kierunek tekstu (od lewej do prawej)
        self._command(0x80, LCD_ENTRYMODESET | LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT)
        count = 0
        row = 0
        if no_refresh:
            while len(text) < 32:
                text += ' '
        for c in text:
            if c == '\n' or count == 16:
                count = 0
                row += 1
                if row == 2:
                    break
                self._command(0x80, 0xC0)
                if c == '\n':
                    continue
            count += 1
            self._command(0x40, ord(c))

    def create_char(self, location, pattern):
        """ Definicja znaku

        location: 0 - 7
        pattern: 5x8
        """
        location &= 0x07
        self._command(0x80, LCD_SETCGRAMADDR | (location << 3))
        self._command(0x40, pattern)
