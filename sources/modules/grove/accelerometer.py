# -*- coding: utf-8 -*-
#
# Moduł z 3-osiowym akcelerometrem cyfrowym (Grove - 3 Axis Digital Accelerometer(+/-16g)).
#  http://wiki.seeedstudio.com/Grove-3-Axis_Digital_Accelerometer-16g/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import adxl345


class Accelerometer(object):
    def __init__(self):
        self.adxl345 = adxl345.ADXL345()

    def read(self, gforce=False):
        """ Stan akcelerometru w m/s^2 lub g """
        axes = self.adxl345.get_axes(gforce)
        return axes['x'], axes['y'], axes['z']
