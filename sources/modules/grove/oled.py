# -*- coding: utf-8 -*-
#
# Moduł wyświetlacza OLED (Grove - OLED Display 1.12", 0.96").
#  http://wiki.seeedstudio.com/Grove-OLED_Display_1.12inch/
#  http://wiki.seeedstudio.com/Grove-OLED_Display_0.96inch/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division


class OLED(object):
    DISPLAY_128X64 = 0
    DISPLAY_96X96 = 1

    def __init__(self, display):
        if display == self.DISPLAY_128X64:
            import oled_128x64
            self.oled = oled_128x64.OLED_128x64()
        elif display == self.DISPLAY_96X96:
            import oled_96x96
            self.oled = oled_96x96.OLED_96x96()
        else:
            raise ValueError('Unexpected display %d.' % display)

    def clear(self):
        """ Wyczyszczenie """
        self.oled.clear()

    def set_contrast(self, level):
        """ Kontrast

        level: 0 - 255
        """
        self.oled.set_contrast(level)

    def set_normal_display(self):
        """ Białe znaki ma czarnym tle """
        self.oled.set_normal_display()

    def set_inverse_display(self):
        """ Czarne znaki ma białym tle """
        self.oled.set_inverse_display()

    def set_position(self, row, column):
        """ Pozycja tekstu

        row: 0 - 7 (0.96"), 0 - 11 (1.12")
        column: 0 - 15 (0.96"), 0 - 11 (1.12")
        """
        self.oled.set_position(row, column)

    def set_text(self, text):
        """ Tekst """
        self.oled.set_text(text)
