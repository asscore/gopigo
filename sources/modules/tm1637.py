# -*- coding: utf-8 -*-
#
# TM1637 - sterownik LED.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import RPi.GPIO as GPIO

hex_digits = [0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71]

ADDR_AUTO = 0x40
ADDR_FIXED = 0x44
START_ADDR = 0xC0


class TM1637(object):
    doublepoint = False
    current_data = [0x00, 0x00, 0x00, 0x00]

    def __init__(self, clk_pin, dio_pin, brightness=2):
        self.clk_pin = clk_pin
        self.dio_pin = dio_pin
        self.brightness = brightness

        GPIO.setup(self.clk_pin, GPIO.OUT)
        GPIO.setup(self.dio_pin, GPIO.OUT)

    def _write_byte(self, data):
        for i in range(0, 8):
            GPIO.output(self.clk_pin, GPIO.LOW)
            if data & 0x01:
                GPIO.output(self.dio_pin, GPIO.HIGH)
            else:
                GPIO.output(self.dio_pin, GPIO.LOW)

            data = data >> 1
            GPIO.output(self.clk_pin, GPIO.HIGH)

        GPIO.output(self.clk_pin, GPIO.LOW)
        GPIO.output(self.dio_pin, GPIO.HIGH)
        GPIO.output(self.clk_pin, GPIO.HIGH)
        GPIO.setup(self.dio_pin, GPIO.IN)

        while GPIO.input(self.dio_pin):
            time.sleep(.001);
            if GPIO.input(self.dio_pin):
                GPIO.setup(self.dio_pin, GPIO.OUT)
                GPIO.output(self.dio_pin, GPIO.LOW)
                GPIO.setup(self.dio_pin, GPIO.IN)

        GPIO.setup(self.dio_pin, GPIO.OUT)

    def _start(self):
        GPIO.output(self.clk_pin, GPIO.HIGH)
        GPIO.output(self.dio_pin, GPIO.HIGH)
        GPIO.output(self.dio_pin, GPIO.LOW)
        GPIO.output(self.clk_pin, GPIO.LOW)

    def _stop(self):
        GPIO.output(self.clk_pin, GPIO.LOW)
        GPIO.output(self.dio_pin, GPIO.LOW)
        GPIO.output(self.clk_pin, GPIO.HIGH)
        GPIO.output(self.dio_pin, GPIO.HIGH)

    def _encode(self, data):
        point = 0x80 if self.doublepoint else 0x00

        if data == 0x7F:
            data = 0x00
        else:
            data = hex_digits[data] + point
        return data

    def set_segments(self, data, position=0, raw_data=False):
        # position: 0 - 3

        # Jeśli raw_data = True to wartość dla segmentu: 0 - 255 lub 0x00 - 0xFF
        #     0
        #   5   1
        #     6
        #   4   2
        #     3
        # 8 bit to dwukropek (tylko segment 1)

        for i in range(0, len(data)):
            if raw_data:
                self.current_data[i + position] = data[i]
            else:
                self.current_data[i + position] = self._encode(data[i])

        self._start()
        self._write_byte(ADDR_AUTO)
        self._stop()
        self._start()
        self._write_byte(START_ADDR + (position & 0x03))
        for i in range(len(data)):
            if raw_data:
                self._write_byte(data[i])
            else:
                self._write_byte(self._encode(data[i]))
        self._stop()
        self._start()
        self._write_byte(0x88 + self.brightness)
        self._stop()

    def set_brightness(self, brightness):
        # brightness: 0 - 7
        if brightness not in range(8):
            return

        self.brightness = brightness
        self.set_segments(self.current_data, raw_data=True)

    def set_doublepoint(self, point):
        # point: True lub False
        self.doublepoint = point
        self.set_segments(self.current_data, raw_data=True)

    def clear(self):
        b = self.brightness
        point = self.doublepoint
        self.brightness = 0
        self.doublepoint = False
        data = [0x00, 0x00, 0x00, 0x00]
        self.set_segments(data, raw_data=True)
        self.brightness = b
        self.doublepoint = point
