# -*- coding: utf-8 -*-
#
# Interfejs I2C.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import RPi.GPIO as GPIO


class Bus(object):
    def __init__(self, address, big_endian=True, module='smbus'):
        rev = GPIO.RPI_REVISION
        bus_number = 1 if rev == 2 or rev == 3 else 0
        self.module = module
        if self.module == 'smbus':
            import smbus
            self.bus = smbus.SMBus(bus_number)
        elif self.module == 'periphery':
            import periphery
            self.bus = periphery.I2C('/dev/i2c-' + str(bus_number))
        else:
            raise IOError('I2C module not supported.')

        self.address = address
        self.big_endian = big_endian

    def set_address(self, address):
        self.address = address

    def transfer(self, out_arr, in_bytes=0):
        if self.module == 'smbus':
            if len(out_arr) >= 2 and in_bytes == 0:
                self.bus.write_i2c_block_data(self.address, out_arr[0], out_arr[1:])
            elif len(out_arr) == 1 and in_bytes == 0:
                self.bus.write_byte(self.address, out_arr[0])
            elif len(out_arr) == 1 and in_bytes >= 1:
                return self.bus.read_i2c_block_data(self.address, out_arr[0], in_bytes)
            elif len(out_arr) == 1 and in_bytes is None:
                return self.bus.read_i2c_block_data(self.address, out_arr[0])
            elif len(out_arr) == 0 and in_bytes >= 1:
                return self.bus.read_byte(self.address)
            else:
                raise IOError('I2C operation not supported.')
        elif self.module == 'periphery':
            msgs = []
            offset = 0
            if in_bytes is None:
                in_bytes = 32
            if len(out_arr) > 0:
                msgs.append(self.bus.Message(out_arr))
                offset = 1
            if in_bytes:
                r = [0 for b in range(in_bytes)]
                msgs.append(self.bus.Message(r, read=True))
            if len(msgs) >= 1:
                self.bus.transfer(self.address, msgs)
            if in_bytes:
                return msgs[offset].data
            return

    def write_8(self, val):
        """ Zapis wartości 8-bitowej """
        val = int(val)
        self.transfer([val])

    def write_reg_8(self, reg, val):
        """ Zapis wartości 8-bitowej do rejestru """
        val = int(val)
        self.transfer([reg, val])

    def write_reg_16(self, reg, val, big_endian=None):
        """ Zapis wartości 16-bitowej do rejestru """
        val = int(val)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            self.transfer([reg, ((val >> 8) & 0xFF), (val & 0xFF)])
        else:
            self.transfer([reg, (val & 0xFF), ((val >> 8) & 0xFF)])

    def write_reg_32(self, reg, val, big_endian=None):
        """ Zapis wartości 32-bitowej do rejestru """
        val = int(val)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            self.transfer([reg, ((val >> 24) & 0xFF), ((val >> 16) & 0xFF), ((val >> 8) & 0xFF), (val & 0xFF)])
        else:
            self.transfer([reg, (val & 0xFF), ((val >> 8) & 0xFF), ((val >> 16) & 0xFF), ((val >> 24) & 0xFF)])

    def write_reg_list(self, reg, list):
        """ Zapis ciągu bajtów do rejestru """
        arr = [reg]
        arr.extend(list)
        self.transfer(arr)

    def read_8u(self):
        """ Odczyt wartości 8-bitowej """
        val = self.transfer([], 1)
        return val

    def read_8s(self):
        """ Odczyt wartości 8-bitowej ze znakiem """
        val = self.transfer([], 1)
        if val & 0x80:
            return val - 0x100
        return val

    def read_16u(self, big_endian=None):
        """ Odczyt wartości 16-bitowej """
        val = self.transfer([], 2)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 8) | val[1]
        else:
            return (val[1] << 8) | val[0]

    def read_16s(self, big_endian=None):
        """ Odczyt wartości 16-bitowej ze znakiem """
        val = self.read_16u(big_endian)
        if val & 0x8000:
            return val - 0x10000
        return val

    def read_32u(self, big_endian=None):
        """ Odczyt wartości 32-bitowej """
        val = self.transfer([], 4)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 24) | (val[1] << 16) | (val[2] << 8) | val[3]
        else:
            return (val[3] << 24) | (val[2] << 16) | (val[1] << 8) | val[0]

    def read_32s(self, big_endian=None):
        """ Odczyt wartości 32-bitowej ze znakiem """
        val = self.read_32u(big_endian)
        if val & 0x80000000:
            val = val - 0x100000000
        return val

    def read_list(self, len):
        """ Odczyt ciągu bajtów """
        return self.transfer([], len)

    def read_reg_8u(self, reg):
        """ Odczyt wartości 8-bitowej z rejestru """
        val = self.transfer([reg], 1)
        return val[0]

    def read_reg_8s(self, reg):
        """ Odczyt wartości 8-bitowej ze znakiem z rejestru """
        val = self.read_reg_8u(reg)
        if val & 0x80:
            return val - 0x100
        return val

    def read_reg_16u(self, reg, big_endian=None):
        """ Odczyt wartości 16-bitowej z rejestru """
        val = self.transfer([reg], 2)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 8) | val[1]
        else:
            return (val[1] << 8) | val[0]

    def read_reg_16s(self, reg, big_endian=None):
        """ Odczyt wartości 16-bitowej ze znakiem z rejestru """
        val = self.read_reg_16u(reg, big_endian)
        if val & 0x8000:
            return val - 0x10000
        return val

    def read_reg_32u(self, reg, big_endian=None):
        """ Odczyt wartości 32-bitowej z rejestru """
        val = self.transfer([reg], 4)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 24) | (val[1] << 16) | (val[2] << 8) | val[3]
        else:
            return (val[3] << 24) | (val[2] << 16) | (val[1] << 8) | val[0]

    def read_reg_32s(self, reg, big_endian=None):
        """ Odczyt wartości 32-bitowej ze znakiem z rejestru """
        val = self.read_reg_32u(reg, big_endian)
        if val & 0x80000000:
            val = val - 0x100000000
        return val

    def read_reg_list(self, reg, len=None):
        """ Odczyt ciągu bajtów z rejestru """
        return self.transfer([reg], len)
